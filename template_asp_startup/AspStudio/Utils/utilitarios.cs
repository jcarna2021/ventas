﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Claims;

namespace AspStudio.Utils
{
    public static class utilitarios
    {
        private struct _IMAGE_FILE_HEADER
        {
            public ushort Machine;

            public ushort NumberOfSections;

            public uint TimeDateStamp;

            public uint PointerToSymbolTable;

            public uint NumberOfSymbols;

            public ushort SizeOfOptionalHeader;

            public ushort Characteristics;
        }

        public static DateTime GetBuildDateTime(Assembly assembly)
        {
            string codeBase = assembly.GetName().CodeBase;
            if (File.Exists(codeBase))
            {
                byte[] array = new byte[Math.Max(Marshal.SizeOf(typeof(_IMAGE_FILE_HEADER)), 4)];
                using (FileStream fileStream = new FileStream(codeBase, FileMode.Open, FileAccess.Read))
                {
                    fileStream.Position = 60L;
                    fileStream.Read(array, 0, 4);
                    fileStream.Position = BitConverter.ToUInt32(array, 0);
                    fileStream.Read(array, 0, 4);
                    fileStream.Read(array, 0, array.Length);
                }
                GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
                try
                {
                    _IMAGE_FILE_HEADER iMAGE_FILE_HEADER = (_IMAGE_FILE_HEADER)Marshal.PtrToStructure(gCHandle.AddrOfPinnedObject(), typeof(_IMAGE_FILE_HEADER));
                    return TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1) + new TimeSpan((long)iMAGE_FILE_HEADER.TimeDateStamp * 10000000L));
                }
                finally
                {
                    gCHandle.Free();
                }
            }
            return default(DateTime);
        }

        public static byte[] streamToByteArray(Stream input)
        {
            MemoryStream memoryStream = new MemoryStream();
            input.CopyTo(memoryStream);
            return memoryStream.ToArray();
        }

        public static List<Dictionary<string, object>> ToJson(this DataTable dt)
        {
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                foreach (DataColumn column in dt.Columns)
                {
                    if (row[column].ToString().StartsWith('{') || row[column].ToString().StartsWith('['))
                    {
                        dictionary[column.ColumnName] = JsonConvert.DeserializeObject(row[column].ToString());
                    }
                    else
                    {
                        dictionary[column.ColumnName] = row[column];
                    }
                }
                list.Add(dictionary);
            }
            return list;
        }

        public static string GetClaim(string token, string claimType)
        {
            //IL_000d: Unknown result type (might be due to invalid IL or missing references)
            //IL_0013: Expected O, but got Unknown
            JwtSecurityTokenHandler val = new JwtSecurityTokenHandler();
            SecurityToken obj = ((SecurityTokenHandler)val).ReadToken(token);
            JwtSecurityToken val2 = (JwtSecurityToken)(object)((obj is JwtSecurityToken) ? obj : null);
            return val2.Claims.First((Claim claim) => claim.Type == claimType).Value;
        }

    }
}
