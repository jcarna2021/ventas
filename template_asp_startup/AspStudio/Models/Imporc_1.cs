﻿namespace AspStudio.Models
{
    public class Imporc_1
    {
        public string? CNUMERO { get; set; }
        public string? CAGENCIA { get; set; }
        public string? CVESSEL { get; set; }
        public string? CCNAVIERA { get; set; }
        public string? CNUMEROBL { get; set; }
        public string? CNROCONTENEDOR { get; set; }
        public DateTime FEMISION { get; set; }
        public DateTime CETD { get; set; }
        public DateTime CETA { get; set; }
        public DateTime CPAGODERECHOS { get; set; }
        public DateTime CLLEGAALMACEN { get; set; }
        public DateTime CLLEGALIQUIDACION { get; set; }
        public string? INCOTERM { get; set; }
        //public string? CMARCA { get; set; }
        public string? PROVEEDOR { get; set; }
        public string? PROVEEDOR_DESC { get; set; }
        public string? CPAGOPROVEEDOR { get; set; }
        public string? DOCUMENTOS { get; set; }
        public string? ORDEN_DE_COMPRA { get; set; }
        public string? CNROFACTURA { get; set; }
        public string? CNRODAM { get; set; }
        public string? CDOCSIWTFSUYON { get; set; }
        public string? CEXTRAINFO { get; set; }
        public string? CPZI { get; set; }
        public string? COC_CHEMO { get; set; }
        public DateTime CFECHA_EFACTURA { get;set; }
        public string? CNRO_LIQUIDACION { get; set; }
        public bool CFLAG_CIERRE { get; set; }
    }

    public class ImporcAgencia_1
    {
        public string? CNUMERO { get; set; }
        public string? CAGENCIA { get; set; }
        public DateTime CLLEGALIQUIDACION { get; set; }
        public string? CDOCSIWTFSUYON { get; set; }
        public string? CEXTRAINFO { get; set; }
        public string? CNRO_LIQUIDACION { get; set; }
    }

    public class ImporcProv_1
    {
        public string? CNUMERO { get; set; }
        public string? CCODPROVE { get; set; }
        public string? CDESPROVE { get; set; }
        public string? CNROFACTURA { get; set; }
        public string? CEXTRAINFO { get; set; }
        public DateTime CFECHA_EFACTURA { get; set; }
    }

    public class ImporcProvFlete_1
    {
        public string? CNUMERO { get; set; }
        public string? CCODPROVE_FLETE { get; set; }
        public string? CDESPROVE_FLETE { get; set; }
        public string? CNROFACTURA_FLETE { get; set; }
        public DateTime CFECHA_EFACTURA_FLETE { get; set; }
    }

    public class PagoProveedor
    {
        public int PAGOPROVEEDOR_ID { get; set; }
        public string? CNUMERO { get; set; }
        public string? CCODPROVE { get; set; }
        public string? FORMA_PAGO { get; set; }
        public decimal IMPORTE { get; set; }
        public DateTime FECHA_PAGO { get; set; }
        public string? BANCO { get; set; }
        public string? NUMERO_OPERACION { get; set; }
        public string? OBSERVACION { get; set; }
    }

    public class PagoAgente
    {
        public int PAGOAGENTE_ID { get; set; }
        public string? CNUMERO { get; set; }
        public string? CCODPROVE { get; set; }
        public string? FORMA_PAGO { get; set; }
        public decimal IMPORTE { get; set; }
        public DateTime FECHA_PAGO { get; set; }
        public string? BANCO { get; set; }
        public string? NUMERO_OPERACION { get; set; }
        public string? OBSERVACION { get; set; }
    }

    public class PagoFlete
    {
        public int PAGOFLETE_ID { get; set; }
        public string? CNUMERO { get; set; }
        public string? CCODPROVE { get; set; }
        public string? FORMA_PAGO { get; set; }
        public decimal IMPORTE { get; set; }
        public DateTime FECHA_PAGO { get; set; }
        public string? BANCO { get; set; }
        public string? NUMERO_OPERACION { get; set; }
        public string? OBSERVACION { get; set; }
    }

    public class PagoVarios
    {
        public int PAGOVARIOS_ID { get; set; }
        public string? CNUMERO { get; set; }
        public string? CCODPROVE { get; set; }
        public string? FORMA_PAGO { get; set; }
        public decimal IMPORTE { get; set; }
        public DateTime FECHA_PAGO { get; set; }
        public string? BANCO { get; set; }
        public string? NUMERO_OPERACION { get; set; }
        public string? OBSERVACION { get; set; }
    }
    public class Imporc_FS
    {
        public string? CNUMERO { get; set; }
        public decimal CFLETE { get; set; }
        public decimal CSEGURO { get; set; }
    }

    public class Imporc_Cierre
    {
        public string? CNUMERO { get; set; }
        public int CFLAG { get; set; }
    }
}
