﻿namespace AspStudio.Models
{
    public class PLAN_COB_CAB
    {
        public string? COCNROPLA { get; set; }
        public DateTime? COCFECPLA { get; set; }
        public decimal? COCTCOBMN { get; set; }
        public decimal? COCTCOBUS { get; set; }
        public decimal? COCTCHDIFMN { get; set; }
        public decimal? COCTCHDIFUS { get; set; }
        public string? COCCOMPCONTMN { get; set; }
        public string? COCSITUAC { get; set; }
        public DateTime? COCFECCRE { get; set; }
        public string? COCUSUARI { get; set; }
        public string? COCVENDE {  get; set; }
        public string? COCNROPLA_AUTO {  get; set; }
        public string? COCCOMPCONTME { get; set; }
        public DateTime? COCFECCONTAB {  get; set; }
        public int? COCNROCORRELATIVO { get; set; }
        public string? COCTIPCOBRANZA { get; set; }
        public List<PLAN_COB_DET>? Detalle { get; set; }
    }

    public class PLAN_COB_DET
    {
        public string? RUC { get; set; }
        public string? AVAL { get; set; }
        public decimal MONTOCOBRADO { get; set; }
        public int Cobranza_ExcesoId { get; set; }
        public string? DEPNROPLA {  get; set; }
        public string? DEPSECUENC { get; set; }
        public string? DEPTIPDOC { get; set; }
        public string? DEPNRODOC { get; set; }
        public string? DEPTIPOPER { get; set; }
        public string? DEPCONCEP { get; set; }
        public string? DEPFECCOB { get; set; }
        public decimal? DEPIMPORTE { get; set; }
        public string? DEPTIPMON { get; set; }
        public decimal? DEPTIPCAM { get; set; }
        public DateTime? DEPFECCRE { get; set; }
        public string? DEPUSUARI { get; set; }
        public string? DEPGLOSA { get; set; }
        public string? DEPCOBRA { get; set; }
        public string? DEPCODBAN { get; set; }
        public string? DEPDESBAN { get; set; }
        public string? DEPRFTIPDOC { get; set; }
        public string? DEPRFNUMDOC { get; set; }
        public decimal? DEPIMPORTEPERC { get; set; }
        public bool F_CJABCO { get; set; }
        public string? DEPBCOGIR { get; set; }
        public string? DEPCTABANCH { get; set; }
        public string? DEPPERAUTO { get; set; }
        public bool DEPCOBRDIFCLIE { get; set; }
        public string? CTA_BANCARIA { get; set; }
        public int? CODDETPLA { get; set; }
        public int IDBCODET { get; set; }
        public string? DEPNUMRECIBO { get; set; }
        public bool IDCAJADET { get; set; }
        public bool AUTODETRACCION { get; set; }
        public bool AUTODETRACCION_GENERADO { get; set; }
        public bool AUTODETRACCION_MANUAL { get; set; }
        public string? COD_AUDITORIA { get; set; }
        public string? COD_CLIENTE1 { get; set; }
        public string? COD_CLIENTE2 { get; set; }
        public bool FLG_DIETARIO { get; set; }
        public decimal? INTERES_DIETARIO { get; set; }
        public decimal? PORTES_DIETARIO { get; set; }
        public decimal? COMISION_DIETARIO { get; set; }
        public string? FEC_RES { get; set; }
        public decimal? DEMTOCRED { get; set; }
        public string? DENROCOMP { get; set; }
        public DateTime? DEFECHCOMP { get; set; }
        public DateTime? DEPFECCONTAB { get; set; }
        public decimal? DEPAJUSTE { get; set; }
        public string? DEPCOB_BANCO { get; set; }
    }

    public class PLAN_COB
    {
        public string? COCTIPCOBRANZA { get; set; }
        public List<PLAN_COB_DET>? Detalle { get; set; }
    }
}
