﻿namespace AspStudio.Models
{
    public class MotivosNoContab
    {
        public int MotivoId { get; set; }
        public string? descripcion { get; set; }
        public string? flag_i_s { get; set; }
    }

    public class CajaNoContab
    {
        public int CajaId { get; set;}
        public DateTime Fecha { get; set;}
        public int MotivoId { get; set; }
        public decimal Monto { get; set;}
        public string? Responsable { get; set; }
        public string? Comentario { get; set; }
        public bool Flag_Cierre { get; set; }
        public string? validate { get; set; }
    }

    public class CajaNoContabAlt
    {
        public int CajaId { get; set; }
        public string? Fecha { get; set; }
        public int MotivoId { get; set; }
        public decimal Monto { get; set; }
        public string? Responsable { get; set; }
        public string? Comentario { get; set; }
        public bool Flag_Cierre { get; set; }
        public string? validate { get; set; }
    }
}
