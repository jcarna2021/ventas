﻿namespace AspStudio.Models
{
    public class Filtro
    {
        public string term { get; set; }
    }

    public class FiltroCobrExc
    {
        public string? Cliente { get; set; }
        public string? Aval { get; set; }
    }
    public class FiltroFechas
    {
        public DateTime? FInicio { get; set; }
        public DateTime? FTermino { get; set; }
    }

    public class FiltroCaja
    {
        public string? FInicio { get; set; }
        public string? FTermino { get; set; }
        public string? term { get; set; }
    }

    public class FiltroCBR
    {
        public int value { get; set; }
        public string term { get; set; }
    }

    public class FiltroDocumento
    {
        public string RUC { get; set; }
        public string TIPDDOCVTA { get; set; }
        public string SERDDOCVTA { get; set; }
        public string NUMDDOCVTA { get; set; }
    }

    public class FiltroProducto
    {
        public string? almacen { get; set; }
        public string? listap { get; set; }
        public string? term { get; set; }
    }

    public class FiltroProductoLista
    {
        public string TIPDOCVTA { get; set; }
        public string CLIENTE { get; set; }
        public string NRODOCU { get; set; }
        public string FECHAINI { get; set; }
        public string FECHAFIN { get; set; }
        public string VENDEDOR { get; set; }
        public string FORMA { get; set; }
        public string METODO { get; set; }
        public string MONEDA { get; set; }
    }

    public class FiltroCartera
    {
        public string Cliente { get; set; }
        public string Aval { get; set; }
        public DateTime FInicio { get; set; }
        public DateTime FTermino { get; set; }
        public string Pedido { get; set; }
        public string Documento { get; set; }
    }

    public class FiltroCobrando
    {
        public decimal MontoCobrar {  get; set; }
        public decimal MontoFila {  get; set; }
        public decimal MontoRestante { get; set; }
        public decimal MontoCobrado { get; set; }
        public bool ckActivo {  get; set; }
    }

    public class ParamDelete
    {
        public string? DEPNROPLA { get; set; }
        public string? DEPSECUENC { get; set; }
        public string? DEPNRODOC { get; set; }
        public string? DEPFECCOB { get; set; }
    }

    public class FiltroImporCab
    {
        public DateTime FInicio { get; set; }
        public DateTime FTermino { get; set; }
        public string? Proveedor { get; set; }
        public string? Marca { get; set; }        
        public string? Producto { get; set; }
        public DateTime Eta { get; set; }
        public string? nro_bl { get; set; }
        public string? nro_contenedor { get; set; }
        public string? nro_orden_chemo { get; set; }
        public string? nro_prof_proveedor { get; set; }
        public string? nro_dam { get; set; }
        public string? nro_pzi { get; set; }

    }
}

public class ResultJson
{
    public string? title { get; set; }
    public string? mensaje { get; set; }
    public string? icon { get; set; }
    public string? idReturn { get; set; }
}

/*
 @CLIENTE as varchar(100),
@NRODOCU as varchar(100),
@FECHAINI as varchar(10),
@FECHAFIN as varchar(10),
@VENDEDOR as varchar(2),
@FORMA as varchar(2),
@METODO as varchar(2),
@MONEDA as varchar(2)
 */