﻿using System.Security.Policy;

namespace AspStudio.Models
{
    public class Impord_1
    {
        public string? CNUMERO { get; set; }
        public string? CITEM { get; set; }
        public string? CCODARTIC { get; set; }
        public string? CDESARTIC { get; set; }
        public decimal NCANTIDAD { get; set; }
        public decimal NPREUNITA { get; set; }
        public decimal DBULTOS { get; set; }
        public string? CCOMENT1 { get; set; }
        public string? CMAR_CODIGO { get; set; }
    }
}
