﻿namespace AspStudio.Models
{
    public class LineaNaviera
    {
        public int LINEA_NAVIERAID { get; set; }
        public string? DESCRIPCION { get; set; }
        public string? COMENTARIO { get; set; }
    }

    public class FormaPago
    {
        public int FORMAPAGO_ID { get; set; }
        public string? DESCRIPCION { get; set; }
        public string? OBSERVACION { get; set; }

    }
}
