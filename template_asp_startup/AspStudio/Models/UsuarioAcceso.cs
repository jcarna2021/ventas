﻿namespace AspStudio.Models
{
    public class UsuarioAcceso
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public string Clave { get; set; }
        public string RolUsuario { get; set; }
        public string[] Roles { get; set; }
        public int soloPerito { get; set; }
        public string v_UsuarioNombre { get; set; }
        public string flag_caja_no_contable { get; set; }
        public string flag_caja_re_apertura { get; set; }
        public string flag_reapertura_importacion { get; set; }
    }

    public class UsuarioCustomAcceso
    {
        public int n_IdUsuario { get; set; }
        public string v_UsuarioNombre { get; set; }
        public string v_Password { get; set; }
        public string v_Nombre { get; set; }
        public string v_ApellidoPaterno { get; set; }
        public string v_ApellidoMaterno { get; set; }
        public string v_Email { get; set; }
        public int i_IdRol { get; set; }
        public int i_Estado { get; set; }
        public string v_CIP { get; set; }


    }
}
