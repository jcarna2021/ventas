﻿namespace AspStudio.Models
{
    public class PEDCAB
    {
        public string TIPDOCVTA { get; set; }
        public string SERDOCVTA { get; set; }
        public string CFNUMPED { get; set; }
        public string CFFECDOC { get; set; }
        public string CFFECVEN { get; set; }
        public string CFVENDE { get; set; }
        public string CFPUNVEN { get; set; }
        public string CFCODCLI { get; set; }
        public string CFNOMBRE { get; set; }
        public string CFDIRECC { get; set; }
        public string CFRUC { get; set; }
        public decimal CFIMPORTE { get; set; }
        public decimal CFPORDESCL { get; set; }
        public decimal CFPORDESES { get; set; }
        public string CFFORVEN { get; set; }
        public decimal CFTIPCAM { get; set; }
        public string CFCODMON { get; set; }
        public string CFRFTD { get; set; }
        public string CFRFNUMSER { get; set; }
        public string CFRFNUMDOC { get; set; }
        public string CFFECCRE { get; set; }
        public string CFESTADO { get; set; }
        public string CFUSER { get; set; }
        public string CFGLOSA { get; set; }
        public string CFNUMGUI { get; set; }
        public string CFNUMFAC { get; set; }
        public string CFORDCOM { get; set; }
        public string CFGLOSA1 { get; set; }
        public decimal CFIGV { get; set; }
        public decimal CFDESCTO { get; set; }
        public decimal CFDESIMP { get; set; }
        public string CFTIPFAC { get; set; }
        public decimal CFDESVAL { get; set; }
        public string CFCOTIZA { get; set; }
        public string CFLINEA { get; set; }
        public string CFORDENFAB { get; set; }
        public string CFDIRECCA { get; set; }
        public string CFESTADO_PED { get; set; }
        public int CFEXISTECOTIZA { get; set; }
        public decimal COD_DIRECCION { get; set; }
        public string COD_AUDITORIA { get; set; }
        public string RESPUESTA { get; set; }
        public string TIPO { get; set; }
        public string ETIQUETA1 { get; set; }
        public string ETIQUETA2 { get; set; }
        public string ETIQUETA3 { get; set; }
        public string ETIQUETA4 { get; set; }
        public string ETIQUETA5 { get; set; }
        public string ETIQUETA6 { get; set; }
        public string ETIQUETA7 { get; set; }
        public string ETIQUETA8 { get; set; }
        public string CFMPAG { get; set; }
        public string CFALMA { get; set; }
        public decimal CFSALDO { get; set; }
        public string CFNROPED { get; set; }
        public string MOTIVO_NC_ND { get; set; }
        public string CFRFFECDOC { get; set; }
        public List<PEDDET> DETALLE { get; set; }

    }
    public class PEDDET
    {
        public string TIPDOCVTA { get; set; }
        public string SERDOCVTA { get; set; }
        public string DFNUMPED { get; set; }
        public string DFSECUEN { get; set; }
        public string DFCODIGO { get; set; }
        public string DFDESCRI { get; set; }
        public decimal DFCANTID { get; set; }
        public decimal DFPREC_VEN { get; set; }
        public decimal DFPREC_ORI { get; set; }
        public decimal DFDESCTO { get; set; }
        public decimal DFIGV { get; set; }
        public decimal DFDESCLI { get; set; }
        public decimal DFDESESP { get; set; }
        public decimal DFIGVPOR { get; set; }
        public decimal DFPORDES { get; set; }
        public decimal DFIMPUS { get; set; }
        public decimal DFIMPMN { get; set; }
        public string DFESTADO { get; set; }
        public string DFSERIE { get; set; }
        public string DFALMA { get; set; }
        public string DFTEXTO { get; set; }
        public decimal DFCANREF { get; set; }
        public string DFLOTE { get; set; }
        public decimal DFSALDO { get; set; }
        public int DFARTIGV { get; set; }
        public string DFCODLIS { get; set; }
        public string DFUNIDAD { get; set; }
        public decimal DFPRECOM { get; set; }
        public decimal DFCOMPROMETIDO { get; set; }
        public decimal DFCOMPRA { get; set; }
        public string DFORIGEN { get; set; }
        public string REFERENCIA_GLOSA { get; set; }
        public decimal DFSALDOBULTOS { get; set; }
        public decimal PESO_BRUTO { get; set; }
        public string ETIQUETADET1 { get; set; }
        public string CFCODMON { get; set; }
        public decimal CFTIPCAM { get; set; }
    }
}
