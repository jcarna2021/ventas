﻿namespace AspStudio.Models
{
    public class Cobranza_Exceso
    {
        public int Cobranza_ExcesoId { get; set; }
        public string? Ruc { get; set; }
        public string? Aval { get; set; }
        public decimal Monto_Exceso { get; set; }
        public DateTime Fecha_Registro { get; set; }
        public decimal Monto_utilizado { get; set; }
        public DateTime Fecha_Utilizado { get; set; }
        public decimal Monto_Final { get; set; }
    }
}
