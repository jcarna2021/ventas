﻿using System;
using System.Collections.Generic;

namespace AspStudio.Models
{
    public partial class Usuario
    {
        public Usuario()
        {
           // Seguimientos = new HashSet<Seguimiento>();
        }

        public string CIdUsuario { get; set; } = null!;
        public string? VUsuarioNombre { get; set; }
        public string? VPassword { get; set; }
        public string? VNombre { get; set; }
        public string? VApellidoPaterno { get; set; }
        public string? VApellidoMaterno { get; set; }
        public string? VEmail { get; set; }
        public int? IIdRol { get; set; }
        public sbyte? BEstado { get; set; }
        public string? VUsuarioCreacion { get; set; }
        public DateTime? DFechaCreacion { get; set; }
        public string? VUsuarioModficacion { get; set; }
        public DateTime? DFechaModificacion { get; set; }
        public string? VCip { get; set; }


    }
}
