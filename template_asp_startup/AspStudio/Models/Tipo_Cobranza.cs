﻿namespace AspStudio.Models
{
    public class Tipo_Cobranza
    {
        public string TIPO { get; set; }
        public string COD_COBRANZA { get; set; }
        public string DESCRIPCION { get; set; }
        public string MONEDA { get; set; }
        public string CUENTA { get; set; }
        public bool ANEX_PROV { get; set; }
        public bool BANCOS { get; set; }
        public string USUARIO { get; set; }
        public DateTime? FECHA { get; set; }
        public DateTime? FECACT { get; set; }
        public bool CHEQ_DIFER { get; set; }
        public bool APLIC_DOC { get; set; }
        public int TIP { get; set; }
        public bool? TARJCREDITO { get; set; }
        public string? CTA_TERCEROS { get; set; }
        public string? CTA_RELACIONADAS { get; set; }
        public string? CTA_MATRIZ { get; set; }
        public string? CTA_SUBSIDIARIAS { get; set; }
        public string? CTA_ASOCIADAS { get; set; }
        public string? CTA_SUCURSAL { get; set; }
        public string? CTA_TERCEROS_NG { get; set; }
        public string? CTA_OTROS { get; set; }
    }
}
