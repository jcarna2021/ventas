﻿
var id_vend = '00';
var id_mone = 'MN';
var id_fpga = '00';
var id_lpre = '01';
var id_mpga = '00';
var id_tdoc = '1246';
var id_almc = '01';
var ruc = '20601636086';
var id_tdv = 'PD';
var table;

var productos = [];

var subtotal = 0.00;
var igv = 0.00;
var descto = 0.00;
var total = 0.00;

const formatter = new Intl.NumberFormat('es-PE', {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

function getDate() {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}

$(document).ready(function () {
    $('#txtFecha').val(getDate());
    $('#txtFechaEntrega').val(getDate());

    $('#txtFechaDesdeFil').val(getDate());
    $('#txtFechaHastaFil').val(getDate());

    getObtieneCatalogo_01($('#selVendedor'), 1, id_vend);

    $("#txtApeRaz").autocomplete({

        source: function (request, response) {

            var json = {
                term: request.term
            };
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-cliente-term",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        // console.log(item);
                        return {
                            label: item.CNOMCLI,
                            val: item.CCODCLI,
                            direccion: item.CDIRCLI,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $("[id$=txtNroDoc]").val(i.item.val);
            $('#txtApeRaz').val(i.item.label);
            $('#txtDireccion').val(i.item.direccion);
            if ((i.item.val).length > 8) {
                $('#selTipoDoc').val('1245');
            } else {
                $('#selTipoDoc').val('1246');
            }
        }
    });
    $('#btnConsultar').click(function (e) {
        // new location
        obtenerProductosListado();
    });
    $('#btnSearchDoc').click(function (e) {
        // new location
        var nroDoc = $('#txtNroDoc').val();
        console.log(nroDoc);
        if (nroDoc.length == 8) {

            $.ajax({
                type: "GET",
                url: root + "api/app/obtiene-info-dni/" + nroDoc,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({ message: '<div><img src="../img/loading.gif" /> Cargando Consulta</div>' });
                },
                error: function (data) {
                    //console.log(data);
                    if (code == 400) {
                        alert('400 status code! user error');
                    }
                    if (code == 500) {
                        alert('500 status code! server error');
                    }
                },
            }).done(function (d) {
                $('#selTipoDoc').val('1246');
                $('#txtApeRaz').val(d.apellidoPaterno + ' ' + d.apellidoMaterno + ' ' + d.nombres);
                $('#txtDireccion').val('');
                setTimeout($.unblockUI, 1000);
            })
        }
        if (nroDoc.length == 11) {
            $.ajax({
                type: "GET",
                url: root + "api/app/obtiene-info-ruc/" + nroDoc,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({ message: '<div><img src="../img/loading.gif" /> Cargando Consulta</div>' });
                },
                error: function (data) {
                    //console.log(data);
                    if (code == 400) {
                        alert('400 status code! user error');
                    }
                    if (code == 500) {
                        alert('500 status code! server error');
                    }
                },
            }).done(function (d) {
                console.log(d);
                $('#selTipoDoc').val('1245');
                $('#txtApeRaz').val(d.razonSocial);
                $('#txtDireccion').val(d.direccion);
                setTimeout($.unblockUI, 1000);
            })
        }
    });

    $('button[data-bs-toggle="tab"]').on('shown.bs.tab', function (e) {
        var tabname = e.target.id; // newly activated tab
        if (tabname === 'segundo') {

            getObtieneCatalogo_Fil_01($('#selVendedorFil'), 1, id_vend);
        }
    })

    $('#txtBusProd').on('input', function (e) {
        //console.log($(this).val().length);
        if ($(this).val().length > 2) {
            obtenerProductos();
        } else {
            obtenerProductos();
        }

    });
    $('#txtCantidad').on('input', function (e) {

        calcularDetalle();

    });
    $('#txtPrecio').on('input', function (e) {
        calcularDetalle();

    });
    $('#selLAlmacen').on('change', function () {
        obtenerProductos();
    });
    $('#selLPrecio').on('change', function () {
        obtenerProductos();
    });
    $('#selTipoDocVenta').on('change', function () {
        valor = $(this).val();
        getObtieneCatalogo_11_Filter($('#selSerieDoc'), valor, '0');
    });

    $('#btnGrabarDetalle').click(function (e) {
        var id = $('#txtCodArt').val();
        //console.log(id);
        const index = productos.findIndex((item) => item.ACODIGO === id);
        //console.log(index);
        const updatedData = {
            PRE_CAN: Number($('#txtCantidad').val().replace(/[^0-9\.]+/g, "")),
            PRE_ACT: Number($('#txtPrecio').val().replace(/[^0-9\.]+/g, "")),
            DFSALDOBULTOS: Number($('#txtBultos').val().replace(/[^0-9\.]+/g, "")),
            SUBTOTAL: Number($('#txtSubTotalDet').val().replace(/[^0-9\.]+/g, "")),
            IGV: Number($('#txtIgvDet').val().replace(/[^0-9\.]+/g, "")),
        }
        //console.log(updatedData);
        productos[index].PRE_CAN = Number(updatedData.PRE_CAN);
        productos[index].PRE_ACT = Number(updatedData.PRE_ACT);
        productos[index].DFSALDOBULTOS = Number(updatedData.DFSALDOBULTOS);
        productos[index].SUBTOTAL = Number(updatedData.SUBTOTAL);
        productos[index].IGV = Number(updatedData.IGV);
        productos[index].TIPDOCVTA = $('#selTipoDocVenta').val();
        productos[index].SERDOCVTA = $('#selSerieDoc').val();
        

        //console.log(productos);
        subtotal = 0.00;
        igv = 0.00;
        descto = 0.00;
        total = 0.00;

        for (let i in productos) {
            subtotal += Number(productos[i]["SUBTOTAL"]);
            igv += Number(productos[i]["IGV"]);
            //console.log(igv);
            //console.log(subtotal);

        }
        total = Number(subtotal + igv)
        $('#backDropModal').modal('hide');
        construyePedido();
    });

    $('#btnGuardar').click(function (e) {
        // new location
        registraPedido();
    });
    $('#btnNuevo').click(function (e) {
        // new location
        nuevoPedido();
    });
    $('#btnclearDoc').click(function (e) {
        $('#selTipoDoc').val('1246');
        $("[id$=txtNroDoc]").val('');
        $('#txtApeRaz').val('');
        $('#txtDireccion').val('');
    });

    $('#btnOpenDocs').click(function (e) {
        $('#backDropModalView').modal('show');
        var TD = $('#selTipoDocVentaRef').val();
        construyeDocumentosVenta(TD);
    });
    $('#btnClearDocs').click(function (e) {
        $('#selTipoDocVentaRef').val('PD');
        $('#txtSerieRef').val('');
        $('#txtNumDocRef').val('');
        $('#txtFecDocRef').val('');
    });
});

function pad(num, largo, char) {
    char = char || '0';
    num = num + '';
    return num.length >= largo ? num : new Array(largo - num.length + 1).join(char) + num;
}

function addItem(codigo, descripcion, precio, um, exonerado) {

    var item = {};
    item["ITEM"] = productos.length + 1;
    item["ACODIGO"] = codigo;
    item["ADESCRI"] = descripcion;
    item["PRE_ACT"] = precio;
    item["PRE_CAN"] = 1;
    item["FLG_EXONERADO_IGV"] = exonerado;
    item["SUBTOTAL"] = precio;
    item["IGV"] = (exonerado == false ? 0 : (precio) * 0.18);
    item["AUNIDAD"] = um;
    item["DFSALDOBULTOS"] = 0.00;
    item["TIPDOCVTA"] = $('#selTipoDocVenta').val();
    item["SERDOCVTA"] = $('#selSerieDoc').val();

    var duplicado = false;

    duplicado = validateUnique(codigo)
    if (duplicado == false) {
        productos.push(item);
        subtotal = 0.00;
        igv = 0.00;
        descto = 0.00;
        total = 0.00;

        for (let i in productos) {
            subtotal += Number(productos[i]["SUBTOTAL"]);
            igv += Number(productos[i]["IGV"]);
        }
        total += Number(subtotal + igv)
    }


    construyePedido();
}

function removeItem(codigo) {
    const index = productos.findIndex((item) => item.ACODIGO === codigo);
    productos.splice(index, 1);

    let indice = 0;
    for (let i in productos) {
        indice++;
        productos[i]["ITEM"] = indice;
    }

    subtotal = 0.00;
    igv = 0.00;
    descto = 0.00;
    total = 0.00;

    for (let i in productos) {
        subtotal += Number(productos[i]["SUBTOTAL"]);
        igv += Number(productos[i]["IGV"]);
        total += Number(subtotal + igv)
    }
    construyePedido();
}

function validateUnique(valor) {
    return !!productos.find(i => i.ACODIGO === valor)
}

function ReplaceNumberWithCommas(yourNumber) {
    //Seperates the components of the number
    var n = yourNumber.toString().split(".");
    //Comma-fies the first part
    n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //Combines the two sections
    return n.join(".");
}

function construyePedido() {

    //console.log(productos);

    table = $('#tabPedido').DataTable({
        "bDestroy": true,
        "aaData": productos,
        "bPaginate": false,
        "bFilter": false,
        scrollCollapse: true,
        scrollY: '180px',
        "bInfo": false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [
            {
                "mData": null,
                "className": "dt-center",
                "sWidth": "20px",
                "mRender": function (o) {
                    return o.ITEM;
                }
            },
            { "data": "ADESCRI" },
            {
                "mData": null,
                "className": "dt-center",
                "sWidth": "50px",
                "mRender": function (o) {
                    return formatter.format(o.PRE_CAN);
                }
            },
            { "data": "AUNIDAD" },
            {
                "mData": null,
                "className": "dt-right",
                "sWidth": "50px",
                "mRender": function (o) {
                    return formatter.format(o.PRE_ACT);
                }
            },
            {
                "mData": null,
                "className": "dt-right",
                "sWidth": "50px",
                "mRender": function (o) {
                    return o.DFSALDOBULTOS;
                }
            },
            {
                "mData": null,
                "className": "dt-right",
                "sWidth": "50px",
                "mRender": function (o) {
                    return formatter.format(o.SUBTOTAL);

                }
            },
            {
                "mData": null,
                "className": "dt-right",
                "sWidth": "20px",
                "mRender": function (o) {
                    return ' <div class="btn-group"><button class="btn btn-sm btn-default" onclick="updateItem(\'' + o.ACODIGO + '\',\'' + o.ADESCRI + '\',\'' + o.PRE_ACT + '\',\'' + o.AUNIDAD + '\',\'' + o.FLG_EXONERADO_IGV + '\')" ><i class="fa fa-edit"></i></button><button class="btn btn-sm btn-default" onclick="removeItem(\'' + o.ACODIGO + '\')" ><i class="fa fa-trash"></i></button></div>';
                }
            },
        ]
    });

    $('#txtSubTotal').val(formatter.format(subtotal));
    $('#txtIgv').val(formatter.format(igv));
    $('#txtTotal').val(formatter.format(total));
}

function calcularDetalle() {
    var cantDet = $('#txtCantidad').val();
    var precDet = $('#txtPrecio').val();
    var figv = $('#txtFIgv').val();
    var subCDet = cantDet * precDet;
    //console.log(subCDet);
    $('#txtSubTotalDet').val(formatter.format(subCDet));
    var igvCdet = (figv == false ? 0.00 : (subCDet) * 0.18);
    $('#txtIgvDet').val(formatter.format(igvCdet));
    var totCDet = Number(subCDet) + Number(igvCdet);
    $('#txtTotalDet').val(formatter.format(totCDet));
}

function updateItem(codigo, descripcion, precio, um, exonerado) {


    //console.log(id);
    const index = productos.findIndex((item) => item.ACODIGO === codigo);

    $('#backDropModal').modal('show');
    $('#txtCodArt').val(codigo);
    $('#txtDesArt').val(descripcion);
    $('#txtCantidad').val(productos[index].PRE_CAN)
    /* 
     = updatedData.PRE_CAN;
    productos[index].PRE_ACT = updatedData.PRE_ACT;
    productos[index].DFSALDOBULTOS = updatedData.DFSALDOBULTOS;
    productos[index].SUBTOTAL = updatedData.SUBTOTAL;
    productos[index].IGV = updatedData.IGV;
*/
    $('#lblDesripcion').html(descripcion);
    preDet = Number(productos[index].PRE_ACT);
    $('#txtPrecio').val(formatter.format(preDet));
    igvDet = (exonerado == false ? 0 : (productos[index].PRE_ACT) * 0.18);  //Number(precio) * 0.18;
    totDet = Number(precio) + Number(igvDet);
    //console.log(igvDet);
    $('#txtFIgv').val(exonerado);
    $('#txtSubTotalDet').val(formatter.format(productos[index].SUBTOTAL));
    $('#txtIgvDet').val(formatter.format(productos[index].IGV));
    $('#txtTotalDet').val(formatter.format(Number(productos[index].SUBTOTAL) + Number(productos[index].IGV)));
    $('#txtBultos').val(productos[index].DFSALDOBULTOS);
}

function registraPedido() {

    var detalle = [];

    $.each(productos, function (index, val) {
        item = {};
        item["TIPDOCVTA"] = $('#selTipoDocVenta').val();
        item["SERDOCVTA"] = $('#selSerieDoc').val();
        item["DFNUMPED"] = '';
        item["DFSECUEN"] = pad((index + 1), 3, '0');
        item["DFCODIGO"] = val.ACODIGO;
        item["DFDESCRI"] = val.ADESCRI;
        item["DFCANTID"] = val.PRE_CAN;
        item["DFPREC_VEN"] = val.PRE_ACT;
        item["DFPREC_ORI"] = val.PRE_ACT;
        item["DFDESCTO"] = 0;
        item["DFIGV"] = val.IGV;
        item["DFDESCLI"] = 0;
        item["DFDESESP"] = 0;
        item["DFIGVPOR"] = 18.00;
        item["DFPORDESDFIMPUS"] = 0;
        item["DFIMPUS"] = 0;
        item["DFIMPMN"] = (val.PRE_ACT * val.PRE_CAN) + val.IGV;
        item["DFESTADO"] = '';
        item["DFSERIE"] = '';
        item["DFALMA"] = $('#selLAlmacen').val();
        item["DFTEXTO"] = '';
        item["DFCANREF"] = 0;
        item["DFLOTE"] = '';
        item["DFSALDO"] = 0;
        item["DFARTIGV"] = val.FLG_EXONERADO_IGV == false ? 0 : 1;
        item["DFCODLIS"] = '';
        item["DFUNIDAD"] = val.AUNIDAD;
        item["DFPRECOM"] = 0.00;
        item["DFCOMPROMETIDO"] = 0.00;
        item["DFCOMPRA"] = 0.00;
        item["DFORIGEN"] = '0';
        item["REFERENCIA_GLOSA"] = '';
        item["DFSALDOBULTOS"] = val.DFSALDOBULTOS;
        item["PESO_BRUTO"] = 0.00;
        item["ETIQUETADET1"] = val.DFSALDOBULTOS.toString();
        item["CFCODMON"] = '';
        item["CFTIPCAM"] = $('#txtTC').val(),
        detalle.push(item);
    });
    //console.log(detalle);
    if (detalle.length == 0) {
        swal("informacion", "Para completar el registro del pedido se debe agregar al menos un item en el detalle del documento!", "info");
        return;
    }
    var json = {
        TIPDOCVTA: $('#selTipoDocVenta').val(),
        SERDOCVTA: $('#selSerieDoc').val(),
        CFFECDOC: $('#txtFecha').val(),
        CFFECVEN: $('#txtFechaEntrega').val(),
        CFVENDE: $('#selVendedor').val(),
        CFPUNVEN: '01',
        CFCODCLI: $('#txtNroDoc').val(),
        CFNOMBRE: $('#txtApeRaz').val(),
        CFDIRECC: $('#txtDireccion').val(),
        CFRUC: $('#txtNroDoc').val(),
        CFIMPORTE: Number($('#txtTotal').val().replace(/[^0-9\.]+/g, "")),
        CFPORDESCL: 0.00,
        CFPORDESES: 0.00,
        CFFORVEN: $('#selFormaPag').val(),
        CFTIPCAM: $('#txtTC').val(),
        CFCODMON: $('#selMoneda').val(),
        CFRFTD: $('#selTipoDocVentaRef').val(),
        CFRFNUMSER: $('#txtSerieRef').val(),
        CFRFNUMDOC: $('#txtNumDocRef').val(),
        CFESTADO: 'V',
        CFUSER: $('#hfUsuario').val(),
        CFGLOSA: $('#txtObservaciones').val(),
        CFNUMGUI: '',
        CFNUMFAC: '',
        CFORDCOM: '',
        CFGLOSA1: '',
        CFIGV: Number($('#txtIgv').val().replace(/[^0-9\.]+/g, "")),
        CFDESCTO: 0.00,
        CFDESIMP: 0.00,
        CFTIPFAC: '',
        CFDESVAL: 0.00,
        CFCOTIZA: 'ATENDIDO',
        CFLINEA: '',
        CFORDENFAB: '',
        CFDIRECCA: '',
        CFESTADO_PED: '',
        CFEXISTECOTIZA: 0,
        COD_DIRECCION: 0,
        COD_AUDITORIA: '0000',
        RESPUESTA: '',
        TIPO: 'PD',
        ETIQUETA1: '',
        ETIQUETA2: '',
        ETIQUETA3: '',
        ETIQUETA4: '',
        ETIQUETA5: '',
        ETIQUETA6: '',
        ETIQUETA7: '',
        ETIQUETA8: '',
        CFMPAG: $('#selMetPag').val(),
        CFFECCRE: '1900-01-01',
        CFALMA: $('#selLAlmacen').val(),
        CFNUMPED: '0',
        CFRFFECDOC: $('#txtFecDocRef').val(),
        MOTIVO_NC_ND: $('#selMNcNd').text(),
        CFNROPED: $('#txtNumDocRef').val(),
        DETALLE: detalle
    };
    $.ajax({
        type: "POST",
        url: root + "api/app/registra-pedido-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        beforeSend: function () {
            $.blockUI({ message: '<div><img src="../img/loading.gif" />Registrando Pedido</div>' });
        },
        error: function (code) {
            //console.log(data);
            if (code == 400) {
                alert('400 status code! user error');
            }
            if (code == 500) {
                alert('500 status code! server error');
            }
        }
       ,
    }).done(function (d) {

        swal(d.title, d.mensaje, d.icon);
        setTimeout($.unblockUI, 2000);
        if (d.idReturn.length > 1) {
            redirect_blank((root + "api/app/genera-pdf-pedido/" + d.idReturn));
            nuevoPedido();
        }
    })
}

function nuevoPedido() {

    $('#txtFecha').val(getDate());
    $('#txtFechaEntrega').val(getDate());

    getObtieneCatalogo_01($('#selVendedor'), 1, id_vend);

    productos = [];
    construyePedido();
    $('#selTipoDoc').val('1246');
    $("[id$=txtNroDoc]").val('');
    $('#txtApeRaz').val('');
    $('#txtDireccion').val('');
    $('#selTipoDocVentaRef').val('PD');
    $('#txtSerieRef').val('');
    $('#txtNumDocRef').val('');
    $('#txtFecDocRef').val('');
}

function obtenerProductos() {
    var json = {
        almacen: $('#selLAlmacen').val(),
        listap: $('#selLPrecio').val(),
        term: $('#txtBusProd').val()
    };
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-producto-term",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            //console.log(r);
            table = $('#tabProductos').DataTable({
                "bDestroy": true,
                "aaData": r,
                "bPaginate": false,
                "bFilter": false,
                scrollCollapse: true,
                scrollY: '180px',
                "bInfo": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                columns: [
                    { "data": "ACODIGO" },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.ADESCRI
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.AUNIDAD
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            return formatter.format(o.PRE_ACT)
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            return formatter.format(o.STSKDIS)
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-center",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            return '<button class="btn btn-sm btn-default" onclick="addItem(\'' + o.ACODIGO + '\',\'' + o.ADESCRI + '\',\'' + o.PRE_ACT + '\',\'' + o.AUNIDAD + '\',\'' + o.FLG_EXONERADO_IGV + '\')"><i class="fa-solid fa-cart-shopping" ></i></button>';
                        }
                    },
                ]
            });
            construyePedido();
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function redirect_blank(url) {
    var a = document.createElement('a');
    a.target = "_blank";
    a.href = url;
    a.click();
}

function obtenerProductosListado() {
    var json = {
        CLIENTE: $('#txtcliRazFil').val(),
        NRODOCU: $('#txtNroDocFil').val(),
        FECHAINI: $('#txtFechaDesdeFil').val(),
        FECHAFIN: $('#txtFechaHastaFil').val(),
        VENDEDOR: $('#selVendedorFil').val(),
        FORMA: $('#selFormaPagFil').val(),
        METODO: $('#selMetPagFil').val(),
        MONEDA: $('#selMonedaFil').val(),
    };
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-pedido-term",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        beforeSend: function () {
            $.blockUI({ message: '<div><img src="../img/loading.gif" />Cargando Consulta</div>' });
        },
        error: function (code) {
            //console.log(data);
            if (code == 400) {
                alert('400 status code! user error');
            }
            if (code == 500) {
                alert('500 status code! server error');
            }
        },
    }).done(function (r) {

        table = $('#tabPedidosList').DataTable({
            "bDestroy": true,
            "aaData": r,
            "bPaginate": true,
            "bFilter": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            columns: [
                {
                    "mData": null,
                    "bSortable": false,
                    "sWidth": "100px",
                    "mRender": function (o) {
                        var icono = '';
                        icono += '' + o.CFNUMPED
                        return icono;
                    }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        var icono = '';
                        icono += '' + o.CFFECDOC
                        return icono;
                    }
                },
                {
                    "mData": null,
                    "className": "dt-left",
                    "sWidth": "200px",
                    "mRender": function (o) {
                        return o.CFNOMBRE
                    }
                },
                {
                    "mData": null,
                    "className": "dt-left",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return o.FORMAPAGO
                    }
                },
                {
                    "mData": null,
                    "className": "dt-left",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return o.METPAGO
                    }
                },
                {
                    "mData": null,
                    "className": "dt-left",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return o.MONEDA
                    }
                },
                {
                    "mData": null,
                    "className": "dt-right",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return formatter.format(o.CFIMPORTE)
                    }
                },
                {
                    "mData": null,
                    "className": "dt-right",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return formatter.format(o.CFIGV)
                    }
                },
                {
                    "mData": null,
                    "className": "dt-right",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return formatter.format((o.CFIMPORTE + o.CFIGV))
                    }
                },
                {
                    "mData": null,
                    "className": "dt-center",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return '<a href="' + (root + "api/app/genera-pdf-pedido/" + o.CFNUMPED) + '" class="btn btn-sm btn-default" target="_blank"><i class="fa-solid fa-eye" ></i></a>';
                    }
                },
            ]
        });
        setTimeout($.unblockUI, 2000);
    })

}

function getObtieneCatalogo_01(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_02($('#selMoneda'), 2, id_mone);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_02(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_03($('#selFormaPag'), 3, id_fpga);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_03(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_04($('#selLPrecio'), 4, id_lpre);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_04(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_05($('#selMetPag'), 5, id_mpga);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_05(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_06($('#selTipoDoc'), 6, id_tdoc);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_06(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneTC($('#txtTC'), 7, id_tdoc);
        })
        .catch(err => console.error(err))

}

function getObtieneTC(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {

            $.each(res.data, function (i, item) {
                _control.val(item.VENTA);
            });

            getObtieneCatalogo_08($('#selLAlmacen'), 8, id_almc);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_08(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_09($('#selEmpresa'), 9, ruc);
            //obtenerProductos();
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_09(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_10($('#selTipoDocVenta'), 10, id_tdv);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_10(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_11($('#selSerieDoc'), valor, '0000');
        })
        .catch(err => console.error(err))

}
function getObtieneCatalogo_11_Filter(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-serie-documento/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_13_Filtro($('#selMNcNd'), id, 0);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_11(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-serie-documento/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_12($('#selTipoDocVentaRef'), 10, id_tdv);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_12(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_13($('#selMNcNd'), 'PD', 0);

        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_13_Filtro(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-motivo-ncnd/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            //getObtieneCatalogo_13($('#selMNcNd'), 'PD', 0);

        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_13(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-motivo-ncnd/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            obtenerProductos();

        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_Fil_01(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_Fil_02($('#selMonedaFil'), 2, id_mone);
        })
        .catch(err => console.error(err))

}
function getObtieneCatalogo_Fil_02(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_Fil_03($('#selFormaPagFil'), 3, id_fpga);
        })
        .catch(err => console.error(err))

}
function getObtieneCatalogo_Fil_03(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_Fil_04($('#selMetPagFil'), 5, id_mpga);
        })
        .catch(err => console.error(err))

}
function getObtieneCatalogo_Fil_04(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            obtenerProductosListado();
        })
        .catch(err => console.error(err))

}
function construyeDocumentosVenta(TD) {

    var filtro = {
        RUC: '00000000000',
        TIPDDOCVTA: TD,
        SERDDOCVTA: $('#txtBusSer').val(),
        NUMDDOCVTA: $('#txtBusNum').val(),
    }

    var titulo = 'RELACION DE ';
    switch (TD) {
        case "PD": titulo += 'PEDIDOS'; break;
        case "FT": titulo += 'FACTURAS DE VENTA'; break;
        case "BV": titulo += 'BOLETAS DE VENTA'; break;
        case "NC": titulo += 'NOTAS DE CRÉDITO'; break;
        case "ND": titulo += 'NOTAS DE DÉBITO'; break;

    }
    $('#lblDesripcionTD').html(titulo);
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-documentos-venta",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(filtro),
        beforeSend: function () {
            $.blockUI({ message: '<div><img src="../img/loading.gif" /> Cargando Consulta</div>' });
        },
        error: function (data) {
            setTimeout($.unblockUI, 1000);
            $('#backDropModalView').modal('show');
        },
    }).done(function (d) {
        table = $('#tabDocumento').DataTable({
            "bDestroy": true,
            "aaData": d,
            "bPaginate": true,
            "bFilter": true,
            "bInfo": true,
            autoWidth: false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            columns: [
                {
                    "mData": null,
                    "className": "dt-center",
                    "sWidth": "20px",
                    "mRender": function (o) {
                        return o.CFSERPED;
                    }
                },
                {
                    "mData": null,
                    "className": "dt-center",
                    "sWidth": "20px",
                    "mRender": function (o) {
                        return o.CFNUMPED;
                    }
                },
                {
                    "mData": null,
                    "className": "dt-center",
                    "sWidth": "20px",
                    "mRender": function (o) {
                        return o.CFCODCLI;
                    }
                },
                {
                    "mData": null,
                    "className": "dt-left",
                    "sWidth": "250px",
                    "mRender": function (o) {
                        return o.CFNOMBRE;
                    }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "className": "dt-center",
                    "sWidth": "80px",
                    "mRender": function (o) {
                        var icono = '';
                        icono += '' + moment(o.CFFECDOC).format('DD/MM/YYYY')
                        return icono;
                    }
                },
                {
                    "mData": null,
                    "className": "dt-right",
                    "sWidth": "25px",
                    "mRender": function (o) {
                        return formatter.format(o.CFIMPORTE);
                    }
                },
                {
                    "mData": null,
                    "className": "dt-center",
                    "sWidth": "10px",
                    "mRender": function (o) {
                        return o.CFCODMON;
                    }
                },
                {
                    "mData": null,
                    "className": "dt-center",
                    "sWidth": "20px",
                    "mRender": function (o) {
                        return ' <div class="btn-group"><button class="btn btn-sm btn-default" onclick="setDocumentoCabecera(\'' + TD + '\',\'' + o.CFSERPED + '\',\'' + o.CFNUMPED + '\',\'' + moment(o.CFFECDOC).format('DD/MM/YYYY') + '\')" ><i class="fa-solid fa-check"></i></div>';
                    }
                },
            ]
        });
        $('#backDropModalView').modal('hide');
        setTimeout($.unblockUI, 1000);

    })





}
function setDocumentoCabecera(TD, CFSERPED, CFNUMPED, CFFEC) {
    var filtro = {
        RUC: '00000000000',
        TIPDDOCVTA: TD,
        SERDDOCVTA: CFSERPED,
        NUMDDOCVTA: CFNUMPED,
    }
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-documentos-venta-ById",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(filtro),
        beforeSend: function () {
            $.blockUI({ message: '<div><img src="../img/loading.gif" /> Cargando Consulta</div>' });
        },
        error: function (data) {
            setTimeout($.unblockUI, 1000);

        },
    }).done(function (d) {
       
        var r = d[0];

        $('#txtNroDoc').val(r.CFCODCLI);
        if ($('#txtNroDoc').val().length > 8) {
            $('#selTipoDoc').val('1245');
        } else {
            $('#selTipoDoc').val('1246');
        }
        $('#txtApeRaz').val(r.CFNOMBRE);
        $('#txtDireccion').val(r.CFDIRECC);
        $('#selFormaPag').val(r.CFFORVEN);
        $('#selMoneda').val(r.CFCODMON);

        setDocumentoDetalle(TD, CFSERPED, CFNUMPED, CFFEC);

    })
    $('#backDropModalView').modal('hide');
}

function setDocumentoDetalle(TD, CFSERPED, CFNUMPED, CFFEC) {
    $('#selTipoDocVentaRef').val(TD);
    $('#txtSerieRef').val(CFSERPED);
    $('#txtNumDocRef').val(CFNUMPED);
    $('#txtFecDocRef').val(CFFEC);

    var filtro = {
        RUC: '00000000000',
        TIPDDOCVTA: TD,
        SERDDOCVTA: CFSERPED,
        NUMDDOCVTA: CFNUMPED,
    }
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-documentos-venta-detalle-ById",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(filtro),
        beforeSend: function () {
            $.blockUI({ message: '<div><img src="../img/loading.gif" /> Cargando Consulta</div>' });
        },
        error: function (data) {
            setTimeout($.unblockUI, 1000);
            
        },
    }).done(function (d) {
        productos = d;
        //console.log(d);
        subtotal = 0.00;
        igv = 0.00;
        descto = 0.00;
        total = 0.00;

        for (let i in productos) {
            subtotal += Number(productos[i]["SUBTOTAL"]);
            igv += Number(productos[i]["IGV"]);
            //console.log(igv);
            //console.log(subtotal);

        }
        total = Number(subtotal + igv)
        construyePedido();
        setTimeout($.unblockUI, 1000);
       
    })
    $('#backDropModalView').modal('hide');
}