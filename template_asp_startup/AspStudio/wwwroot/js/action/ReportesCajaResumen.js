﻿var id_vend = '00';
var id_mone = 'MN';
var id_fpga = '00';
var id_lpre = '01';
var id_mpga = '00';
var id_tdoc = '1246';
var id_almc = '01';
var ruc = '20601636086';
var id_tdv = 'PD';
var table;

var productos = [];

const formatter = new Intl.NumberFormat('es-PE', {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

function getDate() {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}

$(document).ready(function () {

    $('#txtFinicio').val(getDate());
    $('#txtFfinal').val(getDate());

});

$('#btnGenerar').click(function () {

    var fini = $('#txtFinicio').val();
    var ffin = $('#txtFfinal').val();

    var url = "";

    url = root + "api/app/genera-pdf-CajaNoContab-Resumen/" + fini + "/" + ffin;

    //window.location = root + "api/app/genera-pdf-CajaNoContab-Range/" + fini + "/" + ffin;
    window.open(url, "_blank");
});
