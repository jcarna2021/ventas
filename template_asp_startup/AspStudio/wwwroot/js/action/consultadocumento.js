﻿var id_vend = '00';
var id_mone = 'MN';
var id_fpga = '00';
var id_lpre = '01';
var id_mpga = '00';
var id_tdoc = '1246';
var id_almc = '01';
var ruc = '20601636086';
var id_tdv = 'PD';
var table;

var productos = [];

var subtotal = 0.00;
var igv = 0.00;
var descto = 0.00;
var total = 0.00;

const formatter = new Intl.NumberFormat('es-PE', {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

function getDate() {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}

$(document).ready(function () {


    $('#txtFechaDesdeFil').val(getDate());
    $('#txtFechaHastaFil').val(getDate());

    getObtieneCatalogo_Fil_01($('#selVendedorFil'), 1, id_vend);


    $('#btnConsultar').click(function (e) {
        // new location
        obtenerProductosListado();
    });

});



function obtenerProductosListado() {
    var json = {
        TIPDOCVTA: $('#selTipDocVtaFil').val(),
        CLIENTE: $('#txtcliRazFil').val(),
        NRODOCU: $('#txtNroDocFil').val(),
        FECHAINI: $('#txtFechaDesdeFil').val(),
        FECHAFIN: $('#txtFechaHastaFil').val(),
        VENDEDOR: $('#selVendedorFil').val(),
        FORMA: $('#selFormaPagFil').val(),
        METODO: $('#selMetPagFil').val(),
        MONEDA: $('#selMonedaFil').val(),
    };
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-pedido-term",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        beforeSend: function () {
            $.blockUI({ message: '<div><img src="../img/loading.gif" />Cargando Consulta</div>' });
        },
        error: function (code) {
            //console.log(data);
            if (code == 400) {
                alert('400 status code! user error');
            }
            if (code == 500) {
                alert('500 status code! server error');
            }
        },
    }).done(function (r) {

        table = $('#tabPedidosList').DataTable({
            "bDestroy": true,
            "aaData": r,
            "bPaginate": true,
            "bFilter": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            columns: [
                {
                    "mData": null,
                    "bSortable": false,
                    "sWidth": "100px",
                    "mRender": function (o) {
                        var icono = '';
                        icono += '' + o.tdv
                        return icono;
                    }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "sWidth": "80px",
                    "mRender": function (o) {
                        var icono = '';
                        icono += '' + o.CFNUMPED
                        return icono;
                    }
                },
                {
                    "mData": null,
                    "bSortable": false,
                    "sWidth": "80px",
                    "mRender": function (o) {
                        var icono = '';
                        icono += '' + moment(o.CFFECDOC).format('DD/MM/YYYY')
                        return icono;
                    }
                },
                {
                    "mData": null,
                    "className": "dt-left",
                    "sWidth": "200px",
                    "mRender": function (o) {
                        return o.CFNOMBRE
                    }
                },
                {
                    "mData": null,
                    "className": "dt-left",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return o.FORMAPAGO
                    }
                },
                {
                    "mData": null,
                    "className": "dt-left",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return o.METPAGO
                    }
                },
                {
                    "mData": null,
                    "className": "dt-left",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return o.MONEDA
                    }
                },
                {
                    "mData": null,
                    "className": "dt-right",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return formatter.format(o.STOTAL)
                    }
                },
                {
                    "mData": null,
                    "className": "dt-right",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return formatter.format(o.CFIGV)
                    }
                },
                {
                    "mData": null,
                    "className": "dt-right",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return formatter.format((o.CFIMPORTE))
                    }
                },
                {
                    "mData": null,
                    "className": "dt-center",
                    "sWidth": "50px",
                    "mRender": function (o) {
                        return '<a href="' + (root + "api/app/genera-pdf-pedido/" + o.CFNUMPED) + '" class="btn btn-sm btn-default" target="_blank"><i class="fa-solid fa-eye" ></i></a>';
                    }
                },
            ]
        });
        setTimeout($.unblockUI, 2000);
    })

}

function getObtieneCatalogo_01(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_02($('#selMoneda'), 2, id_mone);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_02(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_03($('#selFormaPag'), 3, id_fpga);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_03(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_04($('#selLPrecio'), 4, id_lpre);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_04(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_05($('#selMetPag'), 5, id_mpga);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_05(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_06($('#selTipoDoc'), 6, id_tdoc);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_06(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneTC($('#txtTC'), 7, id_tdoc);
        })
        .catch(err => console.error(err))

}

function getObtieneTC(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {

            $.each(res.data, function (i, item) {
                _control.val(item.VENTA);
            });

            getObtieneCatalogo_08($('#selLAlmacen'), 8, id_almc);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_08(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_09($('#selEmpresa'), 9, ruc);
            //obtenerProductos();
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_09(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_10($('#selTipoDocVenta'), 10, id_tdv);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_10(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_11($('#selSerieDoc'), valor, '0000');
        })
        .catch(err => console.error(err))

}
function getObtieneCatalogo_11_Filter(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-serie-documento/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_12_Filter($('#selMNcNd'), valor, '0');
            //getObtieneCatalogo_12($('#selTipoDocVentaRef'), 10, id_tdv);
        })
        .catch(err => console.error(err))

}


function getObtieneCatalogo_11(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-serie-documento/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_12($('#selTipoDocVentaRef'), 10, id_tdv);
        })
        .catch(err => console.error(err))

}

function getObtieneCatalogo_12(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-motivo-ncnd/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            obtenerProductosListado();

        })
        .catch(err => console.error(err))

}


function getObtieneCatalogo_Fil_01(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_Fil_02($('#selMonedaFil'), 2, id_mone);
        })
        .catch(err => console.error(err))

}
function getObtieneCatalogo_Fil_02(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_Fil_03($('#selFormaPagFil'), 3, id_fpga);
        })
        .catch(err => console.error(err))

}
function getObtieneCatalogo_Fil_03(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_Fil_04($('#selMetPagFil'), 5, id_mpga);
        })
        .catch(err => console.error(err))

}
function getObtieneCatalogo_Fil_04(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_Fil_05($('#selEmpresaFil'), 9, ruc);
        })
        .catch(err => console.error(err))

}
function getObtieneCatalogo_Fil_05(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            getObtieneCatalogo_Fil_06($('#selTipDocVtaFil'), 10, id_tdv);
            //obtenerProductos();
        })
        .catch(err => console.error(err))

}
function getObtieneCatalogo_Fil_06(_control, id, valor) {
    return axios({
        url: root + 'api/app/obtiene-catalogo/' + id,
        method: 'get',
        timeout: 8000,
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(res => {
            control = _control;
            control.empty();
            control.append('<option value="0">SELECCIONE</option>');
            $.each(res.data, function (i, item) {
                control.append('<option value=' + item.Id + '>' + item.Descripcion + '</option>');
            });
            control.val(valor);
            obtenerProductosListado(); 
        })
        .catch(err => console.error(err))

}