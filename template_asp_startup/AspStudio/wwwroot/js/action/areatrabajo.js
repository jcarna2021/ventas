﻿var table;

$(document).ready(function () {
    getData();
    $("#btnGuardar").click(function () {
        SaveModelo();
    });
});

function AddRow() {

    $('#txtCodigo').val('');
    $('#txtDescripcion').val('');
    $('#dProcesar').modal('show');
}

function getData() {
    var url = root + 'api/app/GetAtrabajo';
    $.ajax({
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: url
    }).done(function (d) {
        // console.log(d);
        table = $('#tab_data').DataTable({
            dom: "<'row mb-3'<'col-md-4 mb-3 mb-md-0'l><'col-md-8 text-right'<'d-flex justify-content-end'f<'ms-2'B>>>>t<'row align-items-center'<'mr-auto col-md-6 mb-3 mb-md-0 mt-n2 'i><'mb-0 col-md-6'p>>",
            "bDestroy": true,
            "aaData": d,
            "bPaginate": true,
            "bFilter": true,
            "stateSave": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
            , autoWidth: false,
            buttons: [
                {
                    text: '<i class="fa fa-plus"></i>&nbsp;Nuevo Registro',
                    className: 'btn btn-sm btn-default',
                    action: function (e, dt, node, config) {
                        AddRow();
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel"></i>&nbsp;Descargar',
                    className: 'btn btn-sm btn-default',
                }
            ],
            columns: [
                { "data": "codigo", "sWidth": "50px" },
                { "data": "descripcion" },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        var icono = '<div class="btn-group" role="group" aria-label="Basic example">';
                        icono += '<button class="btn btn-sm btn-secondary" onclick="rowEditClick(\'' + o.codigo + '\')" ><i class="fa fa-edit"></i>&nbsp;Editar</button> ';
                        icono += '<button class="btn btn-sm btn-secondary" onclick="rowRemovelick(\'' + o.codigo + '\')" ><i class="fa fa-trash"></i>&nbsp;Anular</button> ';
                        icono += '</div>';
                        return icono;
                    }, "sWidth": "12%"
                }
            ]
        });
    });
}