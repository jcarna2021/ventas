﻿var id_vend = '00';
var id_mone = 'MN';
var id_fpga = '00';
var id_lpre = '01';
var id_mpga = '00';
var id_tdoc = '1246';
var id_almc = '01';
var ruc = '20601636086';
var id_tdv = 'PD';
var table;

var productos = [];

const formatter = new Intl.NumberFormat('es-PE', {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

function getDate() {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}

$(document).ready(function () {
    //$('#txtFinicio').val(getDate());
    //$('#txtFfinal').val(getDate());

    obtenerCartera();
});

function obtenerCartera() {

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-TipCobranza-List",
        contentType: "application/json; charset=utf-8",
        //data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            //console.log(r);
            table = $('#tabTipCobranza').DataTable({
                "bDestroy": true,
                "aaData": r,
                "bPaginate": false,
                "bFilter": false,
                scrollCollapse: true,
                scrollY: '250px',
                "bInfo": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                columns: [
                    //{ "data": "AVAL" },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.TIPO
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.COD_COBRANZA
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.DESCRIPCION
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.MONEDA
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.CUENTA
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.FECHA
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                            ck += '<button class="btn btn-sm btn-default" onclick="editarRegistro(\'' + o.COD_COBRANZA.trim() + '\');"><i class="fa-solid fa-edit"></i></button>';
                            ck += '<button class="btn btn-sm btn-danger" onclick="eliminarRegistro(\'' + o.COD_COBRANZA.trim() + '\');"><i class="fa-solid fa-trash"></i></button>';
                            return ck;
                        }
                    }
                ]
            });
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function Limpiar() {
    $('#exampleRadios1').prop('checked', true);
    $('#exampleRadios2').prop('checked', false);
    $('#txtCodigo').val("");
    $('#txtDescripcion').val("");
    $('#dllMoneda').val("MN");
    $('#txtCuentaContable').val("");
    $('#dllTipo').val("0");
}

$('#btnNuevo').click(function (e) {
    $('#lblDesripcion').html("NUEVO TIPO");
    $('#modalPrincipal').modal('show');
    Limpiar();
});

function editarRegistro(dato) {
    $('#lblDesripcion').html("ACTUALIZAR TIPO");
    $('#modalPrincipal').modal('show');
    Limpiar();

    var json = {
        term : dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-TipCobranza-One",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (x) {
            console.log(x);

            var r = x[0];
            if (r.TIPO === "F") {
                $('#exampleRadios1').prop('checked', true);
                $('#exampleRadios2').prop('checked', false);
            }
            else if (r.TIPO === "L") {
                $('#exampleRadios1').prop('checked', false);
                $('#exampleRadios2').prop('checked', true);
            }

            $('#txtCodigo').val(r.COD_COBRANZA);
            $('#txtDescripcion').val(r.DESCRIPCION);
            $('#dllMoneda').val(r.MONEDA);
            $('#txtCuentaContable').val(r.CUENTA);
            $('#dllTipo').val(r.TIP);
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

$('#btnGrabar').click(function (e) {
    SaveRegister();
});

function SaveRegister() {

    //var ck = $('#exampleRadios1').val();
    var ck2 = $('#exampleRadios2').val();
    var ddlTip = $('#dllTipo').val();

    var registro = {}
    registro["TIPO"] = (ck2.checked === true ? "L" : "F");
    registro["COD_COBRANZA"] = $('#txtCodigo').val();
    registro["DESCRIPCION"] = $('#txtDescripcion').val();
    registro["MONEDA"] = $('#dllMoneda').val();
    registro["CUENTA"] = $('#txtCuentaContable').val();
    registro["ANEX_PROV"] = false;
    registro["BANCOS"] = false;
    registro["USUARIO"] = "1";
    registro["CHEQ_DIFER"] = false;
    registro["APLIC_DOC"] = (ddlTip === "3" ? true : false);
    registro["TIP"] = ddlTip; 

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-tipcobranza-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(registro),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length > 1) {
                obtenerCartera();
                Limpiar();
                $('#modalPrincipal').modal('hide');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function eliminarRegistro(dato) {

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/elimina-tipcobranza-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length > 1) {
                obtenerCartera();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}