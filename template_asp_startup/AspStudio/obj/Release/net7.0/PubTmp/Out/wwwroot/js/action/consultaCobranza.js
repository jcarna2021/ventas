﻿var productos = [];

const formatter = new Intl.NumberFormat('es-PE', {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

function getDate() {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}


$(document).ready(function () {
    $('#txtFinicio').val(getDate());
    $('#txtFfinal').val(getDate());

    obtenerCartera();
});

$('#btnBuscar').click(function (e) {
    // new location
    obtenerCartera();
});

function obtenerCartera() {

    var json = {
        FInicio: $('#txtFinicio').val(),
        FTermino: $('#txtFfinal').val()
    };

    //console.log(json);
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-Planillas-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            //console.log(r);
            table = $('#tabPlanillas').DataTable({
                "bDestroy": true,
                "aaData": r,
                "bPaginate": false,
                "bFilter": false,
                scrollCollapse: true,
                scrollY: '250px',
                "bInfo": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                columns: [
                    //{ "data": "AVAL" },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.PLANILLA
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.FECHA_COBRANZA
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            return formatter.format(o.SOLES)
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            return formatter.format(o.DOLARES)
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                            ck += '<button class="btn btn-sm btn-default" onclick="detallePlanilla(\''+ o.PLANILLA.trim()+'\')"><i class="fa-solid fa-list"></i></button>';
                            return ck;
                            //return '<button class="btn btn-sm btn-default"><i class="fa-solid fa-cart-shopping"></i></button>';
                        }
                    }
                ]
            });
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function detallePlanilla(dato) {
    $('#lblDesripcion').html("DETALLE DE PLANILLA");
    $('#modalDetails').modal('show');
    detallePlanilla2(dato);
}

function detallePlanilla2(dato) {
    
    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-PlanillasDet-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            //console.log(r);
            table = $('#tabPlanillasDet').DataTable({
                "bDestroy": true,
                "aaData": r,
                "bPaginate": false,
                "bFilter": false,
                scrollCollapse: true,
                scrollY: '180px',
                "bInfo": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                columns: [
                    //{ "data": "AVAL" },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.COMPROBANTE
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.SECUENCIA
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.FECHACOBRO
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.CLIENTE
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.RUC
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.TIPOCOBRANZA
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            return formatter.format(o.MONTOCOBRADO)
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.MONEDA
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                            ck += '<button class="btn btn-sm btn-danger" onclick="eliminarRegistro(\'' + dato + '\', \'' + o.SECUENCIA.trim() + '\', \'' + o.COMPROBANTE.trim() + '\', \'' + o.FECHACOBRO.trim() + '\')"><i class="fa-solid fa-trash"></i></button>';
                            return ck;
                            //return '<button class="btn btn-sm btn-default"><i class="fa-solid fa-cart-shopping"></i></button>';
                        }
                    }
                ]
            });
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function eliminarRegistro(planilla, secuencia, documento, fecha) {

    var json = {
        DEPNROPLA: planilla,
        DEPSECUENC: secuencia,
        DEPNRODOC: documento,
        DEPFECCOB: fecha
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/elimina-PlanDetalle-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length > 1) {
                detallePlanilla2(planilla);
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}