﻿var id_vend = '00';
var id_mone = 'MN';
var id_fpga = '00';
var id_lpre = '01';
var id_mpga = '00';
var id_tdoc = '1246';
var id_almc = '01';
var ruc = '20601636086';
var id_tdv = 'PD';
var table;

var productos = [];

const formatter = new Intl.NumberFormat('es-PE', {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

function getDate() {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}

$(document).ready(function () {
    //$('#txtFinicio').val(getDate());
    //$('#txtFfinal').val(getDate());

    obtenerCartera();
});

function obtenerCartera() {

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-LineaNaviera-List",
        contentType: "application/json; charset=utf-8",
        //data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            //console.log(r);
            table = $('#tabLNaviera').DataTable({
                "bDestroy": true,
                "aaData": r,
                "bPaginate": false,
                "bFilter": false,
                scrollCollapse: true,
                scrollY: '250px',
                "bInfo": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                columns: [
                    //{ "data": "AVAL" },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.LINEA_NAVIERAID
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.DESCRIPCION
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.COMENTARIO
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                            ck += '<button class="btn btn-sm btn-default" onclick="editarRegistro(\'' + o.LINEA_NAVIERAID + '\');"><i class="fa-solid fa-edit"></i></button>';
                            ck += '<button class="btn btn-sm btn-danger" onclick="eliminarRegistro(\'' + o.LINEA_NAVIERAID  + '\');"><i class="fa-solid fa-trash"></i></button>';
                            return ck;
                        }
                    }
                ]
            });
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function Limpiar() {

    $('#txtCodigo').val("");
    $('#txtDescripcion').val("");
    $('#txtObs').val("");
}

$('#btnNuevo').click(function (e) {
    $('#lblDesripcion').html("NUEVA LINEA");
    $('#modalPrincipal').modal('show');
    Limpiar();
});

function editarRegistro(dato) {
    $('#lblDesripcion').html("ACTUALIZAR LINEA");
    $('#modalPrincipal').modal('show');
    Limpiar();

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-LineaNaviera-Detalle",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (x) {
            //console.log(x);
            var r = x[0];
            $('#txtCodigo').val(r.LINEA_NAVIERAID);
            $('#txtDescripcion').val(r.DESCRIPCION);
            $('#txtObs').val(r.COMENTARIO);

        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

$('#btnGrabar').click(function (e) {
    SaveRegister();
});

function SaveRegister() {

    var codigo = $('#txtCodigo').val();

    var json = {
        LINEA_NAVIERAID: (codigo === "" ? 0 : codigo),
        DESCRIPCION: $('#txtDescripcion').val(),
        COMENTARIO: $('#txtObs').val()
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-LineaNaviera-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length > 0) {
                obtenerCartera();
                Limpiar();
                $('#modalPrincipal').modal('hide');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function eliminarRegistro(dato) {

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/eliminar-LineaNaviera-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length > 0) {
                obtenerCartera();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}