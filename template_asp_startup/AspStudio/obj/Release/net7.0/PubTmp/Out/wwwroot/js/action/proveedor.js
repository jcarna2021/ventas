﻿var table;

$(document).ready(function () {
    getPais();
    $("#btnGuardar").click(function () {
        SaveModelo();
    });
});

function AddRow() {
    
    $('#txtCodigo').val('');
    $('#txtDescripcion').val('');
    $('#dProcesar').modal('show');
}

function getPais() {
    control = $('#ddlPais');
    var url = root + 'api/app/GetPaises';
    $.ajax({
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: url
    }).done(function (d) {
        //console.log(d);
        control.empty();
        control.append('<option value="0">SELECCIONE</option>');
        $.each(d, function (i, item) {
            control.append('<option value=' + item.codpais + '>' + item.descripcion + '</option>');
        });
        getTDocumento();
    });
}

function getTDocumento() {
    control = $('#ddlTdoc');
    var url = root + 'api/app/GetTdocumento';
    $.ajax({
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: url
    }).done(function (d) {
        //console.log(d);
        control.empty();
        control.append('<option value="0">SELECCIONE</option>');
        $.each(d, function (i, item) {
            control.append('<option value=' + item.coddociden + '>' + item.descripcion + '</option>');
        });
        getTDocumentoCtc();
    });
}

function getTDocumentoCtc() {
    control = $('#ddlTdocCnt');
    var url = root + 'api/app/GetTdocumento';
    $.ajax({
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: url
    }).done(function (d) {
        //console.log(d);
        control.empty();
        control.append('<option value="0">SELECCIONE</option>');
        $.each(d, function (i, item) {
            control.append('<option value=' + item.coddociden + '>' + item.descripcion + '</option>');
        });
        getData();
    });
}


function mostrarModal(id, seccion) {
    if (seccion === '1') {
        $('#titulo2').html('Administrar Contactos');
        $('#dContacto').modal('show');
    }
    if (seccion === '2') {
        $('#titulo3').html('Administrar Vendedores');
        $('#dVendedor').modal('show');
    }
    if (seccion === '4') {
        $('#titulo3').html('Administrar Broker / House');
        $('#dVendedor').modal('show');
    }
}

function getData() {
    var url = root + 'api/app/GetProveedores';
    $.ajax({
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: url
    }).done(function (d) {
        // console.log(d);
        table = $('#tab_data').DataTable({
            dom: "<'row mb-3'<'col-md-4 mb-3 mb-md-0'l><'col-md-8 text-right'<'d-flex justify-content-end'f<'ms-2'B>>>>t<'row align-items-center'<'mr-auto col-md-6 mb-3 mb-md-0 mt-n2 'i><'mb-0 col-md-6'p>>",
            "bDestroy": true,
            "aaData": d,
            "bPaginate": true,
            "bFilter": true,
            "stateSave": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
            , autoWidth: false,
            buttons: [
                {
                    text: '<i class="fa fa-plus"></i>&nbsp;Nuevo Registro',
                    className: 'btn btn-sm btn-default',
                    action: function (e, dt, node, config) {
                        AddRow();
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel"></i>&nbsp;Descargar',
                    className: 'btn btn-sm btn-default',
                }
            ],
            columns: [
                { "data": "codigopro", "sWidth": "50px" },
                { "data": "razonsocial" },
                { "data": "alias" },
                { "data": "Tdoc" },
                { "data": "nrodocumento" },
                { "data": "PaisStr" },
                { "data": "departamento" },
                { "data": "provincia" },
                { "data": "distrito" },
                { "data": "Telefonos" },
                { "data": "e_mail" },
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        var icono = '<div class="btn-group" role="group" aria-label="Basic example">';
                        icono += '<button class="btn btn-sm btn-secondary" onclick="rowEditClick(\'' + o.codigopro + '\')" ><i class="fa fa-edit"></i>&nbsp;Editar</button> ';
                        icono += '<button class="btn btn-sm btn-secondary" onclick="rowRemovelick(\'' + o.codigopro + '\')" ><i class="fa fa-trash"></i>&nbsp;Anular</button> ';
                        icono += '<select class="form-select form-select-sm" onchange="mostrarModal(' + o.codigopro + ' ,this.value)"><option value="0">Mas acciones</option><option value="1">Contactos</option><option value="2">Agentes Navieros</option><option value="3">Tarifas</option></select></div>';
                        return icono;
                    }, "sWidth": "15%"
                }
            ]
        });
      
    });
}