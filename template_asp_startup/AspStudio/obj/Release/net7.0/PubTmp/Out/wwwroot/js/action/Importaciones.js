﻿var id_vend = '00';
var id_mone = 'MN';
var id_fpga = '00';
var id_lpre = '01';
var id_mpga = '00';
var id_tdoc = '1246';
var id_almc = '01';
var ruc = '20601636086';
var id_tdv = 'PD';
var table;
var tableDet;
var tableprov;
var tableagent;
var tableflete;
var tablevarios;

var productos = [];

const formatter = new Intl.NumberFormat('es-PE', {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

function getDate() {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}

$(document).ready(function () {
    $('#txtFinicio').val(getDate());
    $('#txtFfinal').val(getDate());
    $('#txtETA').val("1900-01-01");

    table = $('#tabImporCab').DataTable({
        "bDestroy": true,
        //"aaData": r,
        "bPaginate": false,
        "bFilter": false,
        scrollCollapse: true,
        //scrollY: '250px',
        "bInfo": false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

    tableDet = $('#tabImporDet').DataTable({
        "bDestroy": true,
        //"aaData": r,
        "bPaginate": false,
        "bFilter": false,
        scrollCollapse: true,
        //scrollY: '250px',
        "bInfo": false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

    tableprov = $('#tabPagoProveedor').DataTable({
        "bDestroy": true,
        //"aaData": r,
        "bPaginate": false,
        "bFilter": false,
        scrollCollapse: true,
        //scrollY: '250px',
        "bInfo": false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

    tableagent = $('#tabPagoAgente').DataTable({
        "bDestroy": true,
        //"aaData": r,
        "bPaginate": false,
        "bFilter": false,
        scrollCollapse: true,
        //scrollY: '250px',
        "bInfo": false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

    tableflete = $('#tabPagoFlete').DataTable({
        "bDestroy": true,
        //"aaData": r,
        "bPaginate": false,
        "bFilter": false,
        scrollCollapse: true,
        //scrollY: '250px',
        "bInfo": false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

    tablevarios = $('#tabPagoVarios').DataTable({
        "bDestroy": true,
        //"aaData": r,
        "bPaginate": false,
        "bFilter": false,
        scrollCollapse: true,
        //scrollY: '250px',
        "bInfo": false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

    $("#txtProv").autocomplete({
        source: function (request, response) {

            var json = {
                value: 6,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Autocompletar_Imp",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.CCODPROVE,
                            label: item.CDESPROVE
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtProv').val(i.item.label);
            $('#txtProvVal').val(i.item.val);
        }
    });

    $("#txtProd").autocomplete({
        source: function (request, response) {

            var json = {
                value: 7,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Autocompletar_Imp",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.CCODARTIC,
                            label: item.ADESCRI
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtProd').val(i.item.label);
            $('#txtProdVal').val(i.item.val);
        }
    });

    $("#txtMarc").autocomplete({
        source: function (request, response) {

            var json = {
                value: 8,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Autocompletar_Imp",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.ID,
                            label: item.DESCRIP
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtMarc').val(i.item.label);
            $('#txtMarcVal').val(i.item.val);
        }
    });

    selAgenciaLoad();
    selProveedorLoad();
    selProveedorFleteLoad();
    obtenerLista();
});

function selAgenciaLoad() {

    var json = {
        value: 3,
        term: ""
    };
    //console.log(json);
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-Autocompletar_Imp",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (data) {

            //console.log(data.data);
            $('#selAgencia').empty();
            $('#selAgencia').append('<option value="">SELECCIONE</option>');
            $.each(data, function (i, item) {
                $('#selAgencia').append('<option value=' + item.ANEX_CODIGO + '>' + item.ANEX_DESCRIPCION + '</option>');
            });
            $('#selAgencia').val("");
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function selProveedorLoad() {
    var json = {
        value: 1,
        term: ""
    };
    //console.log(json);
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-Autocompletar_Imp",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (data) {
            console.log(data);
            $('#selProveedor').empty();
            $('#selProveedor').append('<option value="">SELECCIONE</option>');
            $.each(data, function (i, item) {
                $('#selProveedor').append('<option value=' + item.ANEX_CODIGO + '>' + item.ANEX_DESCRIPCION + '</option>');
            });
            $('#selProveedor').val("");
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function selProveedorFleteLoad() {
    var json = {
        value: 11,
        term: ""
    };
    //console.log(json);
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-Autocompletar_Imp",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (data) {

            $('#selProveedorFlete').empty();
            $('#selProveedorFlete').append('<option value="">SELECCIONE</option>');
            $.each(data, function (i, item) {
                $('#selProveedorFlete').append('<option value=' + item.ANEX_CODIGO + '>' + item.ANEX_DESCRIPCION + '</option>');
            });
            $('#selProveedorFlete').val("");
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

$('#btnBuscar').click(function (e) {
    // new location
    obtenerLista();
});

$('#btncloseX').click(function (e) {
    // new location
    obtenerLista();
});
$('#btnclose').click(function (e) {
    // new location
    obtenerLista();
});

function obtenerLista() {

    var txtmarca = $('#txtMarc').val();
    var txtproveedor = $('#txtProv').val();
    var txtproducto = $('#txtProd').val();

    var marca = $('#txtMarcVal').val();
    var proveedor = $('#txtProvVal').val();
    var producto = $('#txtProdVal').val();
    var bl = $('#txtNROBL').val();
    var contenedor = $('#txtNROCONTENEDOR').val();
    var orden_chemo = $('#txtOCHEMO').val();
    var prof_provee = $('#txtProFPROVEE').val();
    var dam = $('#txtDAM').val();
    var pzi = $('#txtPZI').val();

    var json = {
        FInicio: $('#txtFinicio').val(),
        FTermino: $('#txtFfinal').val(),
        Proveedor: (txtproveedor === "" ? "" : proveedor),
        Marca: (txtmarca === "" ? "" : marca),
        Producto: (txtproducto === "" ? "" : producto),
        Eta: $('#txtETA').val(),
        nro_bl: (bl === "" ? "" : bl),
        nro_contenedor: (contenedor === "" ? "" : contenedor),
        nro_orden_chemo: (orden_chemo === "" ? "" : orden_chemo),
        nro_prof_proveedor: (prof_provee === "" ? "" : prof_provee),
        nro_dam: (dam === "" ? "" : dam),
        nro_pzi: (pzi === "" ? "" : pzi)
    };

    var reapertura = $('#txtReapertura').val();
    console.log(reapertura);
    //console.log(@ViewBag.reapertura);

    //$("#tabImporCab > tbody:last").children().remove();
    $("#tabImporCab").DataTable().clear().draw();
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-ImportCab-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {

            console.log(r);
            $.each(r, function (i, item) {
                var tr = "<tr>";
                tr += "<td style=\"text-align:center;\"><div id=\"strN_IMP" + item.N_IMP + "\">" + item.N_IMP + "</div><div class=\"col-sm-12\"><input id=\"txtN_IMP" + item.N_IMP + "\" value=\"" + item.N_IMP + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtN_IMP2" + item.N_IMP + "\" value=\"" + item.N_IMP + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                //tr += "<td style=\"text-align:center;\"><div id=\"strAGENCIA" + item.N_IMP + "\">" + item.AGENCIA + "</div><div class=\"col-sm-12\"><input id=\"txtAGENCIA" + item.N_IMP + "\" value=\"" + item.AGENCIA + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtAGENCIA" + item.N_IMP + "Val\" value=\"" + item.AGENCIA + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtAGENCIA2" + item.N_IMP + "\" value=\"" + item.AGENCIA + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strAGENCIA" + item.N_IMP + "\">" + item.AGENCIA_DESCRI + "</div><button class=\"btn btn-sm btn-default\" onclick=\"ModalAgencia_Edit('" + item.N_IMP + "','" + item.AGENCIA_DESCRI + "','" + item.AGENCIA + "','" + item.DOC_Y_SIWTF_SUYON + "','" + item.CLLEGALIQUIDACION2 + "','" + item.CNRO_LIQUIDACION + "','" + item.EXTRA_INFORMACION + "')\"><i class=\"fa-solid fa-magnifying-glass\"></i></button></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strCOC_CHEMO" + item.N_IMP + "\">" + item.COC_CHEMO + "</div><div class=\"col-sm-12\"><input id=\"txtCOC_CHEMO" + item.N_IMP + "\" value=\"" + item.COC_CHEMO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtCOC_CHEMO2" + item.N_IMP + "\" value=\"" + item.COC_CHEMO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strORDEN_DE_COMPRA" + item.N_IMP + "\">" + item.ORDEN_DE_COMPRA + "</div><div class=\"col-sm-12\"><input id=\"txtORDEN_DE_COMPRA" + item.N_IMP + "\" value=\"" + item.ORDEN_DE_COMPRA + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtORDEN_DE_COMPRA2" + item.N_IMP + "\" value=\"" + item.ORDEN_DE_COMPRA + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strFEMISION" + item.N_IMP + "\">" + item.FEMISION + "</div><div class=\"col-sm-12\"><input id=\"txtFEMISION" + item.N_IMP + "\" value=\"" + item.FEMISION2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtFEMISION2" + item.N_IMP + "\" value=\"" + item.FEMISION + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                //tr += "<td style=\"text-align:center;\"><div id=\"strN_FACTURA" + item.N_IMP + "\">" + item.N_FACTURA + "</div><div class=\"col-sm-12\"><input id=\"txtN_FACTURA" + item.N_IMP + "\" value=\"" + item.N_FACTURA + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtN_FACTURA2" + item.N_IMP + "\" value=\"" + item.N_FACTURA + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                //tr += "<td style=\"text-align:center;\"><div id=\"strCFECHA_EFACTURA" + item.N_IMP + "\">" + item.CFECHA_EFACTURA + "</div><div class=\"col-sm-12\"><input id=\"txtCFECHA_EFACTURA" + item.N_IMP + "\" value=\"" + item.CFECHA_EFACTURA2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtCFECHA_EFACTURA2" + item.N_IMP + "\" value=\"" + item.CFECHA_EFACTURA2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strVESSEL" + item.N_IMP + "\">" + item.VESSEL + "</div><div class=\"col-sm-12\"><input id=\"txtVESSEL" + item.N_IMP + "\" value=\"" + item.VESSEL + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtVESSEL2" + item.N_IMP + "\" value=\"" + item.VESSEL + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strC_NAVIERA" + item.N_IMP + "\">" + item.C_NAVIERA_DESCRI + "</div><div class=\"col-sm-12\"><input id=\"txtC_NAVIERA" + item.N_IMP + "\" value=\"" + item.C_NAVIERA_DESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtC_NAVIERA" + item.N_IMP + "Val\" value=\"" + item.C_NAVIERA + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtC_NAVIERA2" + item.N_IMP + "\" value=\"" + item.C_NAVIERA_DESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strNUMERO_BL" + item.N_IMP + "\">" + item.NUMERO_BL + "</div><div class=\"col-sm-12\"><input id=\"txtNUMERO_BL" + item.N_IMP + "\" value=\"" + item.NUMERO_BL + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtNUMERO_BL2" + item.N_IMP + "\" value=\"" + item.NUMERO_BL + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strN_CONTENEDOR" + item.N_IMP + "\">" + item.N_CONTENEDOR + "</div><div class=\"col-sm-12\"><input id=\"txtN_CONTENEDOR" + item.N_IMP + "\" value=\"" + item.N_CONTENEDOR + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtN_CONTENEDOR2" + item.N_IMP + "\" value=\"" + item.N_CONTENEDOR + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";

                tr += "<td style=\"text-align:center;\"><div id=\"strETD" + item.N_IMP + "\">" + item.ETD + "</div><div class=\"col-sm-12\"><input id=\"txtETD" + item.N_IMP + "\" value=\"" + item.ETD2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtETD2" + item.N_IMP + "\" value=\"" + item.ETD2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strETA" + item.N_IMP + "\">" + item.ETA + "</div><div class=\"col-sm-12\"><input id=\"txtETA" + item.N_IMP + "\" value=\"" + item.ETA2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtETA2" + item.N_IMP + "\" value=\"" + item.ETA2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strINCOTERM" + item.N_IMP + "\">" + item.INCOTERM_DESCRI + "</div><div class=\"col-sm-12\"><input id=\"txtINCOTERM" + item.N_IMP + "\" value=\"" + item.INCOTERM_DESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtINCOTERM" + item.N_IMP + "Val\" value=\"" + item.INCOTERM + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtINCOTERM2" + item.N_IMP + "\" value=\"" + item.INCOTERM_DESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strPRODUCTO" + item.N_IMP + "\">" + item.PRODUCTO + "</div><button class=\"btn btn-sm btn-default\" onclick=\"ObtenerListaDet('" + item.N_IMP + "','" + item.CFLETE_MN + "','" + item.CSEGURO_MN + "','" + item.NIMPORTE + "');\"><i class=\"fa-solid fa-magnifying-glass\"></i></button></td>";
                //tr += "<td style=\"text-align:center;\"><div id=\"strMARCA" + item.N_IMP + "\">" + item.MARCA + "</div></td>";
                //tr += "<td style=\"text-align:center;\"><div id=\"strPROVEEDOR" + item.N_IMP + "\">" + item.PROVEEDOR + "</div><div class=\"col-sm-12\"><input id=\"txtPROVEEDOR" + item.N_IMP + "\" value=\"" + item.PROVEEDOR + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPROVEEDOR" + item.N_IMP + "Val\" value=\"" + item.CCODPROVE + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPROVEEDOR2" + item.N_IMP + "\" value=\"" + item.PROVEEDOR + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strPROVEEDOR" + item.N_IMP + "\">" + item.PROVEEDOR + "</div><button class=\"btn btn-sm btn-default\" onclick=\"ModalProveedor_Edit('" + item.N_IMP + "','" + item.CCODPROVE + "','" + item.N_FACTURA + "','" + item.CFECHA_EFACTURA2 + "','" + item.EXTRA_INFORMACION + "')\"><i class=\"fa-solid fa-magnifying-glass\"></i></button></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strPROVEEDORFLETE" + item.N_IMP + "\">" + item.CDESPROVE_FLETE + "</div><button class=\"btn btn-sm btn-default\" onclick=\"ModalProveedorFlete_Edit('" + item.N_IMP + "','" + item.CCODPROVE_FLETE + "','" + item.CNROFACTURA_FLETE + "','" + item.CFECHA_EFACTURA_FLETE2 + "')\"><i class=\"fa-solid fa-magnifying-glass\"></i></button></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strPAGO_A_PROVEDDOR" + item.N_IMP + "\">" + item.PAGO_A_PROVEDDOR + "</div><button class=\"btn btn-sm btn-default\" onclick=\"ObtenerPagoProvee('" + item.N_IMP + "','" + item.CCODPROVE + "','" + item.PROVEEDOR + "');\"><i class=\"fa-solid fa-magnifying-glass\"></i></button></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strDOCUMENTOS" + item.N_IMP + "\">" + item.DOCUMENTOS + "</div><div class=\"col-sm-12\"><input id=\"txtDOCUMENTOS" + item.N_IMP + "\" value=\"" + item.DOCUMENTOS + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtDOCUMENTOS2" + item.N_IMP + "\" value=\"" + item.DOCUMENTOS + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strN_DAM" + item.N_IMP + "\">" + item.N_DAM + "</div><div class=\"col-sm-12\"><input id=\"txtN_DAM" + item.N_IMP + "\" value=\"" + item.N_DAM + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtN_DAM2" + item.N_IMP + "\" value=\"" + item.N_DAM + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                //tr += "<td style=\"text-align:center;\"><div id=\"strDOC_Y_SIWTF_SUYON" + item.N_IMP + "\">" + item.DOC_Y_SIWTF_SUYON + "</div><div class=\"col-sm-12\"><input id=\"txtDOC_Y_SIWTF_SUYON" + item.N_IMP + "\" value=\"" + item.DOC_Y_SIWTF_SUYON + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtDOC_Y_SIWTF_SUYON2" + item.N_IMP + "\" value=\"" + item.DOC_Y_SIWTF_SUYON + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                //tr += "<td style=\"text-align:center;\"><div id=\"strEXTRA_INFORMACION" + item.N_IMP + "\">" + item.EXTRA_INFORMACION + "</div><div class=\"col-sm-12\"><input id=\"txtEXTRA_INFORMACION" + item.N_IMP + "\" value=\"" + item.EXTRA_INFORMACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtEXTRA_INFORMACION2" + item.N_IMP + "\" value=\"" + item.EXTRA_INFORMACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strPZI" + item.N_IMP + "\">" + item.PZI + "</div><div class=\"col-sm-12\"><input id=\"txtPZI" + item.N_IMP + "\" value=\"" + item.PZI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPZI2" + item.N_IMP + "\" value=\"" + item.PZI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                //tr += "<td style=\"text-align:center;\"><div id=\"strCPAGODERECHOS" + item.N_IMP + "\">" + item.CPAGODERECHOS + "</div><div class=\"col-sm-12\"><input id=\"txtCPAGODERECHOS" + item.N_IMP + "\" value=\"" + item.CPAGODERECHOS2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtCPAGODERECHOS2" + item.N_IMP + "\" value=\"" + item.CPAGODERECHOS + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><div id=\"strCLLEGAALMACEN" + item.N_IMP + "\">" + item.CLLEGAALMACEN + "</div><div class=\"col-sm-12\"><input id=\"txtCLLEGAALMACEN" + item.N_IMP + "\" value=\"" + item.CLLEGAALMACEN2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtCLLEGAALMACEN2" + item.N_IMP + "\" value=\"" + item.CLLEGAALMACEN + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                //tr += "<td style=\"text-align:center;\"><div id=\"strCLLEGALIQUIDACION" + item.N_IMP + "\">" + item.CLLEGALIQUIDACION + "</div><div class=\"col-sm-12\"><input id=\"txtCLLEGALIQUIDACION" + item.N_IMP + "\" value=\"" + item.CLLEGALIQUIDACION2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtCLLEGALIQUIDACION2" + item.N_IMP + "\" value=\"" + item.CLLEGALIQUIDACION + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\"><button id=\"btnEdit" + item.N_IMP + "\" type=\"button\" class=\"btn btn-outline-purple\" onclick=\"EditRowCab('" + item.N_IMP + "');\"><i class=\"fa-solid fa-edit\"></i></button><button id=\"btnSave" + item.N_IMP + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarRegistros('" + item.N_IMP + "','" + item.N_IMP + "');\"><i class=\"fa-solid fa-save\"></i></button></td>";
                tr += "<td style=\"text-align:center;\"><button id=\"btnDelete" + item.N_IMP + "\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"EliminarRegistro('" + item.N_IMP + "');\"><i class=\"fa-solid fa-trash\"></i></button><button id=\"btnCancel" + item.N_IMP + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"CancelEdition('" + item.N_IMP + "');\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";

                if (reapertura === "1") {
                    if (item.CFLAG_CIERRE === true) {

                        tr += "<td style=\"text-align:center;\"><button class=\"btn btn-sm btn-danger\" onclick=\"CierreImp_Registro('" + item.N_IMP + "','0')\"><i class=\"fa-solid fa-lock\"></i></button></td>";
                    }
                    else {
                        tr += "<td style=\"text-align:center;\"><button class=\"btn btn-sm btn-success\" onclick=\"CierreImp_Registro('" + item.N_IMP + "','1')\"><i class=\"fa-solid fa-lock-open\"></i></button></td>";
                    }
                }
                else {
                    if (item.CFLAG_CIERRE === true) {

                        tr += "<td style=\"text-align:center;\"><button class=\"btn btn-sm btn-danger\"><i class=\"fa-solid fa-lock\"></i></button></td>";
                    }
                    else {
                        tr += "<td style=\"text-align:center;\"><button class=\"btn btn-sm btn-success\" onclick=\"CierreImp_Registro('" + item.N_IMP + "','1')\"><i class=\"fa-solid fa-lock-open\"></i></button></td>";
                    }
                }

                tr += "</tr>";
                //console.log(item);
                $("#tabImporCab").DataTable().rows.add($(tr)).draw();

            });
            //$("#tabImporCabRows").html(tr);
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

$('#btnPrueba').click(function (e) {

    var tr = "<tr>";
    tr += "<td style=\"text-align:center;\"></td>";
    //tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtN_IMP\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    //tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtAGENCIA\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtAGENCIAVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtCOC_CHEMO\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtORDEN_DE_COMPRA\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtFEMISION\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    //tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtN_FACTURA\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    //tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtCFECHA_EFACTURA\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtVESSEL\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtC_NAVIERA\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtC_NAVIERAVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtNUMERO_BL\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtN_CONTENEDOR\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";

    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtETD\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtETA\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtINCOTERM\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtINCOTERMVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"></td>";
    //tr += "<td style=\"text-align:center;\"></td>";
    //tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtPROVEEDOR\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtPROVEEDORVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"></td>";
    tr += "<td style=\"text-align:center;\"></td>";
    tr += "<td style=\"text-align:center;\"></td>";
    //tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtPAGO_A_PROVEDDOR\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtDOCUMENTOS\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtN_DAM\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    //tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtDOC_Y_SIWTF_SUYON\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    //tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtEXTRA_INFORMACION\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtPZI\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    //tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtCPAGODERECHOS\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtCLLEGAALMACEN\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    //tr += "<td style=\"text-align:center;\"><div class=\"col-sm-12\"><input id=\"txtCLLEGALIQUIDACION\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\"><button type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarRegistros('','');\"><i class=\"fa-solid fa-save\"></i></button></td>";
    tr += "<td style=\"text-align:center;\"><button type=\"button\" class=\"btn btn-outline-danger\" onclick=\"obtenerLista();\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
    tr += "<td style=\"text-align:center;\"></td>";
    tr += "</tr>";

    $("#tabImporCab").DataTable().row.add($(tr)).draw(false);
    Autocompletee("");
});

function EditRowCab(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");
    //$("#txtAGENCIA" + dato).removeAttr("style");
    $("#txtVESSEL" + dato).removeAttr("style");
    $("#txtC_NAVIERA" + dato).removeAttr("style");
    $("#txtNUMERO_BL" + dato).removeAttr("style");
    $("#txtN_CONTENEDOR" + dato).removeAttr("style");
    $("#txtFEMISION" + dato).removeAttr("style");
    $("#txtETD" + dato).removeAttr("style");
    $("#txtETA" + dato).removeAttr("style");
    //$("#txtCPAGODERECHOS" + dato).removeAttr("style");
    $("#txtCLLEGAALMACEN" + dato).removeAttr("style");
    //$("#txtCLLEGALIQUIDACION" + dato).removeAttr("style");
    $("#txtINCOTERM" + dato).removeAttr("style");
    $("#txtCOC_CHEMO" + dato).removeAttr("style");
    $("#txtPROVEEDOR" + dato).removeAttr("style");
    //$("#txtPAGO_A_PROVEDDOR" + dato).removeAttr("style");
    $("#txtDOCUMENTOS" + dato).removeAttr("style");
    $("#txtORDEN_DE_COMPRA" + dato).removeAttr("style");
    //$("#txtN_FACTURA" + dato).removeAttr("style");
    $("#txtN_DAM" + dato).removeAttr("style");
    //$("#txtDOC_Y_SIWTF_SUYON" + dato).removeAttr("style");
    //$("#txtEXTRA_INFORMACION" + dato).removeAttr("style");
    //$("#txtCFECHA_EFACTURA" + dato).removeAttr("style");
    //$("#txtCNRO_LIQUIDACION" + dato).removeAttr("style");
    $("#txtPZI" + dato).removeAttr("style");
    $("#btnSave" + dato).removeAttr("style");
    $("#btnCancel" + dato).removeAttr("style");

    //$("#strN_IMP" + dato).attr("style", "display:none");
    //$("#strAGENCIA" + dato).attr("style", "display:none");
    $("#strVESSEL" + dato).attr("style", "display:none");
    $("#strC_NAVIERA" + dato).attr("style", "display:none");
    $("#strNUMERO_BL" + dato).attr("style", "display:none");
    $("#strN_CONTENEDOR" + dato).attr("style", "display:none");
    $("#strFEMISION" + dato).attr("style", "display:none");
    $("#strETD" + dato).attr("style", "display:none");
    $("#strETA" + dato).attr("style", "display:none");
    //$("#strCPAGODERECHOS" + dato).attr("style", "display:none");
    $("#strCLLEGAALMACEN" + dato).attr("style", "display:none");
    //$("#strCLLEGALIQUIDACION" + dato).attr("style", "display:none");
    $("#strINCOTERM" + dato).attr("style", "display:none");
    $("#strCOC_CHEMO" + dato).attr("style", "display:none");
    //$("#strPROVEEDOR" + dato).attr("style", "display:none");
    $("#strPAGO_A_PROVEDDOR" + dato).attr("style", "display:none");
    $("#strDOCUMENTOS" + dato).attr("style", "display:none");
    $("#strORDEN_DE_COMPRA" + dato).attr("style", "display:none");
    //$("#strN_FACTURA" + dato).attr("style", "display:none");
    $("#strN_DAM" + dato).attr("style", "display:none");
    //$("#strDOC_Y_SIWTF_SUYON" + dato).attr("style", "display:none");
    //$("#strEXTRA_INFORMACION" + dato).attr("style", "display:none");
    $("#strPZI" + dato).attr("style", "display:none");
    //$("#strCNRO_LIQUIDACION" + dato).attr("style", "display:none");
    //$("#strCFECHA_EFACTURA" + dato).attr("style", "display:none");
    $("#btnEdit" + dato).attr("style", "display:none");
    $("#btnDelete" + dato).attr("style", "display:none");

    Autocompletee(dato);
}

function CancelEdition(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");

    //$("#txtAGENCIA" + dato).attr("style", "display:none");
    $("#txtVESSEL" + dato).attr("style", "display:none");
    $("#txtC_NAVIERA" + dato).attr("style", "display:none");
    $("#txtNUMERO_BL" + dato).attr("style", "display:none");
    $("#txtN_CONTENEDOR" + dato).attr("style", "display:none");
    $("#txtFEMISION" + dato).attr("style", "display:none");
    $("#txtETD" + dato).attr("style", "display:none");
    $("#txtETA" + dato).attr("style", "display:none");
    //$("#txtCPAGODERECHOS" + dato).attr("style", "display:none");
    $("#txtCLLEGAALMACEN" + dato).attr("style", "display:none");
    //$("#txtCLLEGALIQUIDACION" + dato).attr("style", "display:none");
    $("#txtINCOTERM" + dato).attr("style", "display:none");
    $("#txtCOC_CHEMO" + dato).attr("style", "display:none");
    //$("#txtPROVEEDOR" + dato).attr("style", "display:none");
    //$("#txtPAGO_A_PROVEDDOR" + dato).attr("style", "display:none");
    $("#txtDOCUMENTOS" + dato).attr("style", "display:none");
    $("#txtORDEN_DE_COMPRA" + dato).attr("style", "display:none");
    //$("#txtN_FACTURA" + dato).attr("style", "display:none");
    $("#txtN_DAM" + dato).attr("style", "display:none");
    //$("#txtDOC_Y_SIWTF_SUYON" + dato).attr("style", "display:none");
    //$("#txtEXTRA_INFORMACION" + dato).attr("style", "display:none");
    $("#txtPZI" + dato).attr("style", "display:none");
    //$("#txtCFECHA_EFACTURA" + dato).attr("style", "display:none");
    //$("#txtCNRO_LIQUIDACION" + dato).attr("style", "display:none");

    //$("#txtAGENCIA" + dato).val($("#txtAGENCIA2" + dato).val());
    $("#txtVESSEL" + dato).val($("#txtVESSEL2" + dato).val());
    $("#txtC_NAVIERA" + dato).val($("#txtC_NAVIERA2" + dato).val());
    $("#txtNUMERO_BL" + dato).val($("#txtNUMERO_BL2" + dato).val());
    $("#txtN_CONTENEDOR" + dato).val($("#txtN_CONTENEDOR2" + dato).val());
    $("#txtFEMISION" + dato).val($("#txtFEMISION2" + dato).val());
    $("#txtETD" + dato).val($("#txtETD2" + dato).val());
    $("#txtETA" + dato).val($("#txtETA2" + dato).val());
    //$("#txtCPAGODERECHOS" + dato).val($("#txtCPAGODERECHOS2" + dato).val());
    $("#txtCLLEGAALMACEN" + dato).val($("#txtCLLEGAALMACEN2" + dato).val());
    //$("#txtCLLEGALIQUIDACION" + dato).val($("#txtCLLEGALIQUIDACION2" + dato).val());
    $("#txtINCOTERM" + dato).val($("#txtINCOTERM2" + dato).val());
    $("#txtCOC_CHEMO" + dato).val($("#txtCOC_CHEMO2" + dato).val());
    //$("#txtPROVEEDOR" + dato).val($("#txtPROVEEDOR2" + dato).val());
    //$("#txtPAGO_A_PROVEDDOR" + dato).val($("#txtPAGO_A_PROVEDDOR2" + dato).val());
    $("#txtDOCUMENTOS" + dato).val($("#txtDOCUMENTOS2" + dato).val());
    $("#txtORDEN_DE_COMPRA" + dato).val($("#txtORDEN_DE_COMPRA2" + dato).val());
    //$("#txtN_FACTURA" + dato).val($("#txtN_FACTURA2" + dato).val());
    $("#txtN_DAM" + dato).val($("#txtN_DAM2" + dato).val());
    //$("#txtDOC_Y_SIWTF_SUYON" + dato).val($("#txtDOC_Y_SIWTF_SUYON2" + dato).val());
    //$("#txtEXTRA_INFORMACION" + dato).val($("#txtEXTRA_INFORMACION2" + dato).val());
    $("#txtPZI" + dato).val($("#txtPZI2" + dato).val());
    //$("#txtCFECHA_EFACTURA" + dato).val($("#txtCFECHA_EFACTURA2" + dato).val());
    //$("#txtCNRO_LIQUIDACION" + dato).val($("#txtCNRO_LIQUIDACION2" + dato).val());

    //$("#strAGENCIA" + dato).removeAttr("style");
    $("#strVESSEL" + dato).removeAttr("style");
    $("#strC_NAVIERA" + dato).removeAttr("style");
    $("#strNUMERO_BL" + dato).removeAttr("style");
    $("#strN_CONTENEDOR" + dato).removeAttr("style");
    $("#strFEMISION" + dato).removeAttr("style");
    $("#strETD" + dato).removeAttr("style");
    $("#strETA" + dato).removeAttr("style");
    //$("#strCPAGODERECHOS" + dato).removeAttr("style");
    $("#strCLLEGAALMACEN" + dato).removeAttr("style");
    //$("#strCLLEGALIQUIDACION" + dato).removeAttr("style");
    $("#strINCOTERM" + dato).removeAttr("style");
    $("#strCOC_CHEMO" + dato).removeAttr("style");
    //$("#strPROVEEDOR" + dato).removeAttr("style");
    $("#strPAGO_A_PROVEDDOR" + dato).removeAttr("style");
    $("#strDOCUMENTOS" + dato).removeAttr("style");
    $("#strORDEN_DE_COMPRA" + dato).removeAttr("style");
    //$("#strN_FACTURA" + dato).removeAttr("style");
    $("#strN_DAM" + dato).removeAttr("style");
    //$("#strDOC_Y_SIWTF_SUYON" + dato).removeAttr("style");
    //$("#strEXTRA_INFORMACION" + dato).removeAttr("style");
    $("#strPZI" + dato).removeAttr("style");
    //$("#strCFECHA_EFACTURA" + dato).removeAttr("style");
    //$("#strCNRO_LIQUIDACION" + dato).removeAttr("style");

    $("#btnSave" + dato).attr("style", "display:none");
    $("#btnCancel" + dato).attr("style", "display:none");
    $("#btnEdit" + dato).removeAttr("style");
    $("#btnDelete" + dato).removeAttr("style");
}

function Autocompletee(dato) {

    //$("#txtPROVEEDOR" + dato).autocomplete({
    //    source: function (request, response) {

    //        var json = {
    //            value: 1,
    //            term: request.term
    //        };
    //        //console.log(json);
    //        $.ajax({
    //            type: "POST",
    //            url: root + "api/app/obtiene-Autocompletar_Imp",
    //            contentType: "application/json; charset=utf-8",
    //            data: JSON.stringify(json),
    //            dataType: "json",
    //            success: function (data) {
    //                response($.map(data, function (item) {
    //                    //console.log(item);
    //                    return {
    //                        tipo: "",
    //                        val: item.ANEX_CODIGO,
    //                        label: item.ANEX_DESCRIPCION,
    //                    }
    //                }))
    //            },
    //            error: errores
    //        });

    //        function errores(msg) {
    //            alert('Error: ' + msg.responseText);
    //        }
    //    },
    //    minLength: 2,
    //    select: function (e, i) {
    //        //console.log(i);
    //        $('#txtPROVEEDOR' + dato).val(i.item.label);
    //        $('#txtPROVEEDOR' + dato + 'Val').val(i.item.val);
    //    }
    //});

    $("#txtC_NAVIERA" + dato).autocomplete({
        source: function (request, response) {

            var json = {
                value: 4,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Autocompletar_Imp",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.LINEA_NAVIERAID,
                            label: item.DESCRIPCION,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtC_NAVIERA' + dato).val(i.item.label);
            $('#txtC_NAVIERA' + dato + 'Val').val(i.item.val);
        }
    });

    $("#txtINCOTERM" + dato).autocomplete({
        source: function (request, response) {

            var json = {
                value: 2,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Autocompletar_Imp",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.TCOD,
                            label: item.TDESCRI,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtINCOTERM' + dato).val(i.item.label);
            $('#txtINCOTERM' + dato + 'Val').val(i.item.val);
        }
    });
}

function removeRow() {
    obtenerLista();
}

function GuardarRegistros(id, name) {

    var cod = id;
    //var agen = $('#txtAGENCIA' + name).val();
    //var agenVal = $('#txtAGENCIA' + name + 'Val').val();
    var vess = $('#txtVESSEL' + name).val();
    var cnav = $('#txtC_NAVIERA' + name).val();
    var cnavVal = $('#txtC_NAVIERA' + name + 'Val').val();
    var numbl = $('#txtNUMERO_BL' + name).val();
    var cont = $('#txtN_CONTENEDOR' + name).val();
    var fems = $('#txtFEMISION' + name).val();
    var etd = $('#txtETD' + name).val();
    var eta = $('#txtETA' + name).val();
    //var pd = $('#txtCPAGODERECHOS' + name).val();
    var la = $('#txtCLLEGAALMACEN' + name).val();
    //var ll = $('#txtCLLEGALIQUIDACION' + name).val();
    var incot = $('#txtINCOTERM' + name).val();
    var incotVal = $('#txtINCOTERM' + name + 'Val').val();
    //var prov = $('#txtPROVEEDOR' + name).val();
    //var provVal = $('#txtPROVEEDOR' + name+'Val').val();
    //var pagprov = $('#txtPAGO_A_PROVEDDOR' + name).val();
    var doc = $('#txtDOCUMENTOS' + name).val();
    var orcompra = $('#txtORDEN_DE_COMPRA' + name).val();
    //var nfact = $('#txtN_FACTURA' + name).val();
    var ndam = $('#txtN_DAM' + name).val();
    //var dss = $('#txtDOC_Y_SIWTF_SUYON' + name).val();
    //var exi = $('#txtEXTRA_INFORMACION' + name).val();
    var pzi = $('#txtPZI' + name).val();
    var chemo = $('#txtCOC_CHEMO' + name).val();
    //var fechfact = $('#txtCFECHA_EFACTURA' + name).val();
    //var nroliq = $('#txtCNRO_LIQUIDACION' + name).val();


    // validaciones para registro
    //if (agenVal === "") {
    //    swal("Advertencia", "Seleccione una agencia aduanas.", "warning");
    //    return;
    //}

    //if (cnavVal === "") {
    //    swal("Advertencia", "Seleccione una linea naviera.", "warning");
    //    return;
    //}

    //if (incotVal === "") {
    //    swal("Advertencia", "Seleccione un dato para la columna INCOTERM.", "warning");
    //    return;
    //}

    //if (provVal === "" || prov === "") {
    //    swal("Advertencia", "Seleccione un proveedor.", "warning");
    //    return;
    //}

    var json = {
        CNUMERO: cod,
        CAGENCIA: "",
        CVESSEL: vess,
        CCNAVIERA: (cnav === "" ? "" : cnavVal),
        CNUMEROBL: numbl,
        CNROCONTENEDOR: cont,
        FEMISION: fems,
        CETD: etd,
        CETA: eta,
        CPAGODERECHOS: "1900-01-01",
        CLLEGAALMACEN: la,
        CLLEGALIQUIDACION: "1900-01-01",
        INCOTERM: (incot === "" ? "" : incotVal),
        //CMARCA: ,
        PROVEEDOR: "",
        PROVEEDOR_DESC: "",
        CPAGOPROVEEDOR: "",
        DOCUMENTOS: doc,
        ORDEN_DE_COMPRA: orcompra,
        CNROFACTURA: "",
        CNRODAM: ndam,
        CDOCSIWTFSUYON: "",
        CEXTRAINFO: "",
        CPZI: pzi,
        COC_CHEMO: chemo,
        CFECHA_EFACTURA: "1900-01-01",
        CNRO_LIQUIDACION: ""
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-Imporcab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerLista();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function ActualizarRegistro() {

    var cod = $('#txtCODIGOIMPOR3').val();
    //var agen = $('#txtModAgencia').val();
    var agenVal = $('#selAgencia').val();
    var ll = $('#txtLlegaLiq').val();
    var dss = $('#txtDocEnvAgencia').val();
    //var exi = $('#txtCourier').val();
    var nroliq = $('#txtNLiqui').val();

    var json = {
        CNUMERO: cod,
        CAGENCIA: agenVal,
        CLLEGALIQUIDACION: ll,
        CDOCSIWTFSUYON: dss,
        CEXTRAINFO: "",
        CNRO_LIQUIDACION: nroliq,

    };

    $.ajax({
        type: "POST",
        url: root + "api/app/Actualiza-Imporcab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerLista();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}


function ActualizarRegistro_Prov() {

    var cod = $('#txtCODIGOIMPOR4').val();
    var prov = $('#selProveedor option:selected').text();
    var provVal = $('#selProveedor').val();
    var ll = $('#txtFactura').val();
    var exi = $('#txtCourier').val();
    var nroliq = $('#txtFEFactura').val();

    var json = {
        CNUMERO: cod,
        CCODPROVE: provVal,
        CDESPROVE: prov,
        CNROFACTURA: ll,
        CEXTRAINFO: exi,
        CFECHA_EFACTURA: nroliq
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/Actualiza-ImporcabProv-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerLista();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function ActualizarRegistro_ProvFlete() {

    var cod = $('#txtCODIGOIMPOR5').val();
    var prov = $('#selProveedorFlete option:selected').text();
    var provVal = $('#selProveedorFlete').val();
    var ll = $('#txtFacturaFlete').val();
    var nroliq = $('#txtFEFacturaFlete').val();

    var json = {
        CNUMERO: cod,
        CCODPROVE_FLETE: provVal,
        CDESPROVE_FLETE: prov,
        CNROFACTURA_FLETE: ll,
        CFECHA_EFACTURA_FLETE: nroliq
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/Actualiza-ImporcabProvFlete-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerLista();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function GuardarRegistroFS() {

    var cod = $('#txtCODIGOIMPOR').val();
    var flete = $('#txtFlete').val();
    var seguro = $('#txtSeguro').val();

    var json = {
        CNUMERO: cod,
        CFLETE: flete,
        CSEGURO: seguro
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-FleteSeguro-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            //if (r.idReturn.length >= 1) {
            //    obtenerLista();
            //}
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function EliminarRegistro(dato) {

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/eliminar-Imporcab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerLista();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function ObtenerListaDet(dato, cflete, cseguro, total) {
    $('#lblDesripcion').html("DETALLE DE IMPORTACIONES == NRO " + dato);
    $('#modalDetalle').modal('show');

    var json = {
        term: dato
    };
    $('#txtCODIGOIMPOR').val(dato);
    $('#txtFlete').val(cflete);
    $('#txtSeguro').val(cseguro);
    if (total !== 0) {
        $('#txtTotalizador').val(total);
    }
    $("#tabImporDet").DataTable().clear().draw();

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-ImportDet-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {

            console.log(r);
            $.each(r, function (i, item) {
                var tr = "<tr>";
                tr += "<td style=\"text-align:left;\" width=\"50%\"><div id=\"strARTICULO" + item.CNUMERO + "\">" + item.CDESARTIC + "</div><div class=\"col-sm-12\"><input id=\"txtARTICULO" + item.CNUMERO + "\" value=\"" + item.CDESARTIC + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtARTICULO" + item.CNUMERO + "Val\" value=\"" + item.CCODARTIC + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtARTICULO2" + item.CNUMERO + "\" value=\"" + item.CCODARTIC + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strCMAR_CODIGO" + item.CNUMERO + "\">" + item.MAR_DESCRIPCION + "</div><div class=\"col-sm-12\"><input id=\"txtCMAR_CODIGO" + item.CNUMERO + "\" value=\"" + item.MAR_DESCRIPCION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\" disabled/><input id=\"txtCMAR_CODIGO" + item.CNUMERO + "Val\" value=\"" + item.CMAR_CODIGO + "\" type=\"hidden\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtCMAR_CODIGO2" + item.CNUMERO + "\" value=\"" + item.CMAR_CODIGO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><div id=\"strCANTIDAD" + item.CNUMERO + "\">" + item.NCANTIDAD + "</div><div class=\"col-sm-12\"><input id=\"txtCANTIDAD" + item.CNUMERO + "\" value=\"" + item.NCANTIDAD + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\" onchange=\"sumValues('" + item.CNUMERO + "')\" /> <input id=\"txtCANTIDAD2" + item.CNUMERO + "\" value=\"" + item.NCANTIDAD + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><div id=\"strPRECIOUNITARIO" + item.CNUMERO + "\">" + item.NPREUNITA + "</div><div class=\"col-sm-12\"><input id=\"txtPRECIOUNITARIO" + item.CNUMERO + "\" value=\"" + item.NPREUNITA + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\" onchange=\"sumValues('" + item.CNUMERO + "')\"/><input id=\"txtPRECIOUNITARIO2" + item.CNUMERO + "\" value=\"" + item.NPREUNITA + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><div id=\"strBULTOS" + item.CNUMERO + "\">" + item.DBULTOS + "</div><div class=\"col-sm-12\"><input id=\"txtBULTOS" + item.CNUMERO + "\" value=\"" + item.DBULTOS + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtBULTOS2" + item.CNUMERO + "\" value=\"" + item.DBULTOS + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><div class=\"col-sm-12\"><input id=\"txtSubTotal" + item.CNUMERO + "\" value=\"" + item.NTOTVENT + "\" type=\"text\" class=\"form-control form-control-sm\" disabled/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strOBS" + item.CNUMERO + "\">" + item.CCOMENT1 + "</div><div class=\"col-sm-12\"><input id=\"txtOBS" + item.CNUMERO + "\" value=\"" + item.CCOMENT1 + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtOBS2" + item.CNUMERO + "\" value=\"" + item.CCOMENT1 + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><button id=\"btnEditDet" + item.CNUMERO + "\" type=\"button\" class=\"btn btn-outline-purple\" onclick=\"EditRowDet('" + item.CNUMERO + "');\"><i class=\"fa-solid fa-edit\"></i></button><button id=\"btnSaveDet" + item.CNUMERO + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarRegistrosDet('" + item.CNUMERO + "','" + item.CITEM + "','" + item.CNUMERO + "');\"><i class=\"fa-solid fa-save\"></i></button></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><button id=\"btnDeleteDet" + item.CNUMERO + "\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"EliminarRegistroDet('" + item.CNUMERO + "','" + item.CITEM + "');\"><i class=\"fa-solid fa-trash\"></i></button><button id=\"btnCancelDet" + item.CNUMERO + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"CancelEditionDet('" + item.CNUMERO + "');\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
                tr += "</tr>";
                //console.log(item);
                $("#tabImporDet").DataTable().rows.add($(tr)).draw();
            });

        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

$('#btnNuevoDet').click(function (e) {

    var codDato = $('#txtCODIGOIMPOR').val();

    var tr = "<tr>";
    tr += "<td style=\"text-align:center;\" width=\"50%\"><div class=\"col-sm-12\"><input id=\"txtARTICULO\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtARTICULOVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtCMAR_CODIGO\" type=\"text\" class=\"form-control form-control-sm\" disabled/><input id=\"txtCMAR_CODIGOVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtCANTIDAD\" type=\"text\" class=\"form-control form-control-sm\" onchange=\"sumValues('')\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPRECIOUNITARIO\" type=\"text\" class=\"form-control form-control-sm\" onchange=\"sumValues('')\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtBULTOS\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtSubTotal\" type=\"text\" class=\"form-control form-control-sm\" disabled/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><textarea rows=\"2\" id=\"txtOBS\" type=\"text\" class=\"form-control form-control-sm\"></textarea></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"5%\"><button type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarRegistrosDet('" + codDato + "','','')\"><i class=\"fa-solid fa-save\"></i></button></td>";
    tr += "<td style=\"text-align:center;\" width=\"5%\"><button type=\"button\" class=\"btn btn-outline-danger\" onclick=\"ObtenerListaDet('" + codDato + "', 0, 0, 0);\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
    tr += "</tr>";

    $("#tabImporDet").DataTable().row.add($(tr)).draw(false);
    AutocompleteDet("");
});

function sumValues(dato) {
    // Capturar los valores de los campos de entrada
    var input1 = $('#txtCANTIDAD' + dato).val();
    var input2 = $('#txtPRECIOUNITARIO' + dato).val();

    // Convertir los valores a números
    var num1 = parseFloat(input1) || 0;
    var num2 = parseFloat(input2) || 0;

    // Sumar los valores
    var sum = num1 * num2;

    // Mostrar el resultado
    $('#txtSubTotal' + dato).val(sum);
}

function AutocompleteDet(dato) {

    $("#txtARTICULO" + dato).autocomplete({
        appendTo: "#modalDetalle",
        source: function (request, response) {

            var json = {
                value: 5,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Autocompletar_Imp",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            codval: item.MAR_CODIGO,
                            tipo: item.MAR_DESCRIPCION,
                            val: item.ACODIGO,
                            label: item.ADESCRI,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtCMAR_CODIGO' + dato).val(i.item.tipo);
            $('#txtARTICULO' + dato).val(i.item.label);
            $('#txtARTICULO' + dato + 'Val').val(i.item.val);
            $('#txtCMAR_CODIGO' + dato + 'Val').val(i.item.codval);
        }
    });

};

function EditRowDet(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");
    $("#txtARTICULO" + dato).removeAttr("style");
    $("#txtCMAR_CODIGO" + dato).removeAttr("style");
    $("#txtCANTIDAD" + dato).removeAttr("style");
    $("#txtPRECIOUNITARIO" + dato).removeAttr("style");
    $("#txtBULTOS" + dato).removeAttr("style");
    $("#txtOBS" + dato).removeAttr("style");
    $("#btnSaveDet" + dato).removeAttr("style");
    $("#btnCancelDet" + dato).removeAttr("style");

    //$("#strN_IMP" + dato).attr("style", "display:none");
    $("#strARTICULO" + dato).attr("style", "display:none");
    $("#strCMAR_CODIGO" + dato).attr("style", "display:none");
    $("#strCANTIDAD" + dato).attr("style", "display:none");
    $("#strPRECIOUNITARIO" + dato).attr("style", "display:none");
    $("#strBULTOS" + dato).attr("style", "display:none");
    $("#strOBS" + dato).attr("style", "display:none");
    $("#btnEditDet" + dato).attr("style", "display:none");
    $("#btnDeleteDet" + dato).attr("style", "display:none");

    AutocompleteDet(dato);
}

function CancelEditionDet(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");

    $("#txtARTICULO" + dato).attr("style", "display:none");
    $("#txtCMAR_CODIGO" + dato).attr("style", "display:none");
    $("#txtCANTIDAD" + dato).attr("style", "display:none");
    $("#txtPRECIOUNITARIO" + dato).attr("style", "display:none");
    $("#txtBULTOS" + dato).attr("style", "display:none");
    $("#txtOBS" + dato).attr("style", "display:none");

    $("#txtARTICULO" + dato).val($("#txtARTICULO2" + dato).val());
    $("#txtCMAR_CODIGO" + dato).val($("#txtCMAR_CODIGO2" + dato).val());
    $("#txtCANTIDAD" + dato).val($("#txtCANTIDAD2" + dato).val());
    $("#txtPRECIOUNITARIO" + dato).val($("#txtPRECIOUNITARIO2" + dato).val());
    $("#txtBULTOS" + dato).val($("#txtBULTOS2" + dato).val());
    $("#txtOBS" + dato).val($("#txtOBS2" + dato).val());

    $("#strARTICULO" + dato).removeAttr("style");
    $("#strCMAR_CODIGO" + dato).removeAttr("style");
    $("#strCANTIDAD" + dato).removeAttr("style");
    $("#strPRECIOUNITARIO" + dato).removeAttr("style");
    $("#strBULTOS" + dato).removeAttr("style");
    $("#strOBS" + dato).removeAttr("style");

    $("#btnSaveDet" + dato).attr("style", "display:none");
    $("#btnCancelDet" + dato).attr("style", "display:none");
    $("#btnEditDet" + dato).removeAttr("style");
    $("#btnDeleteDet" + dato).removeAttr("style");
}

function GuardarRegistrosDet(codigo, item, name) {

    var cod = codigo;
    var sec = item;
    var art = $('#txtARTICULO' + name).val();
    var artVal = $('#txtARTICULO' + name + 'Val').val();
    var artmarcaVal = $('#txtCMAR_CODIGO' + name + 'Val').val();
    var artmarca = $('#txtCMAR_CODIGO' + name).val();
    var cant = $('#txtCANTIDAD' + name).val();
    var precunit = $('#txtPRECIOUNITARIO' + name).val();
    var bultos = $('#txtBULTOS' + name).val();
    var obs = $('#txtOBS' + name).val();

    var json = {
        CNUMERO: cod,
        CITEM: sec,
        CCODARTIC: (art === "" ? "" : artVal),
        CDESARTIC: art,
        NCANTIDAD: (cant === "" ? "0.00" : cant),
        NPREUNITA: (precunit === "" ? "0.00" : precunit),
        DBULTOS: (bultos === "" ? "0.00" : bultos),
        CCOMENT1: obs,
        CMAR_CODIGO: (artmarca === "" ? "" : artmarcaVal)
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-Impordet-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                ObtenerListaDet(codigo, 0, 0, 0);
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function EliminarRegistroDet(codigo, item) {

    var json = {
        Cliente: codigo,
        Aval: item
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/eliminar-Impordet-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                ObtenerListaDet(codigo, 0, 0, 0);
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function ObtenerPagoProvee(dato, dato2, dato3) {

    $('#lblDesripcion2').html("PAGO PROVEEDORES == NRO " + dato);
    $('#modalPagoProveedor').modal('show');

    var json = {
        term: dato
    };
    $('#txtCODIGOIMPOR2').val(dato);
    $('#txtCODIGOPROVEE2').val(dato2);
    if (dato3 !== "") {
        $('#txtDESCRIPROVEE2').val(dato3);
    }

    $("#tabPagoProveedor").DataTable().clear().draw();
    $("#tabPagoAgente").DataTable().clear().draw();
    $("#tabPagoFlete").DataTable().clear().draw();
    $("#tabPagoVarios").DataTable().clear().draw();

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-PagoProveedores-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {

            $.each(r.prov, function (i, item) {
                var tr = "<tr>";
                tr += "<td style=\"text-align:left;\" width=\"30%\"><div id=\"strPP_FORMAPAGO" + item.NAMECOL + "\">" + item.FORMA_PAGODESCRI + "</div><div class=\"col-sm-12\"><select id=\"txtPP_FORMAPAGO" + item.NAMECOL + "\" style=\"display:none;\" class=\"form-select form-select-sm selectpicker\"></select><input id=\"txtPP_FORMAPAGO" + item.NAMECOL + "Val\" value=\"" + item.FORMA_PAGO + "\" type=\"hidden\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPP_IMPORTE" + item.NAMECOL + "\">" + item.IMPORTE + "</div><div class=\"col-sm-12\"><input id=\"txtPP_IMPORTE" + item.NAMECOL + "\" value=\"" + item.IMPORTE + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPP_IMPORTE2" + item.NAMECOL + "\" value=\"" + item.IMPORTE + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPP_FECHAPAGO" + item.NAMECOL + "\">" + item.FECHA_PAGO + "</div><div class=\"col-sm-12\"><input id=\"txtPP_FECHAPAGO" + item.NAMECOL + "\" value=\"" + item.FECHA_PAGO2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPP_FECHAPAGO2" + item.NAMECOL + "\" value=\"" + item.FECHA_PAGO2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"15%\"><div id=\"strPP_BANCO" + item.NAMECOL + "\">" + item.BANCODESCRI + "</div><div class=\"col-sm-12\"><input id=\"txtPP_BANCO" + item.NAMECOL + "\" value=\"" + item.BANCODESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPP_BANCO" + item.NAMECOL + "Val\" value=\"" + item.BANCO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPP_BANCO2" + item.NAMECOL + "\" value=\"" + item.BANCO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPP_OPERACION" + item.NAMECOL + "\">" + item.NUMERO_OPERACION + "</div><div class=\"col-sm-12\"><input id=\"txtPP_OPERACION" + item.NAMECOL + "\" value=\"" + item.NUMERO_OPERACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPP_OPERACION2" + item.NAMECOL + "\" value=\"" + item.NUMERO_OPERACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"25%\"><div id=\"strPP_OBS" + item.NAMECOL + "\">" + item.OBSERVACION + "</div><div class=\"col-sm-12\"><input id=\"txtPP_OBS" + item.NAMECOL + "\" value=\"" + item.OBSERVACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPP_OBS2" + item.NAMECOL + "\" value=\"" + item.OBSERVACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><button id=\"btnPP_Edit" + item.NAMECOL + "\" type=\"button\" class=\"btn btn-outline-purple\" onclick=\"EditPP_Row('" + item.NAMECOL + "');\"><i class=\"fa-solid fa-edit\"></i></button><button id=\"btnPP_Save" + item.NAMECOL + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarPP_Registros('" + item.NAMECOL + "','" + item.CNUMERO + "','" + item.CCODPROVE + "','" + item.PAGOPROVEEDOR_ID + "');\"><i class=\"fa-solid fa-save\"></i></button></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><button id=\"btnPP_Delete" + item.NAMECOL + "\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"EliminarPP_Registro('" + item.PAGOPROVEEDOR_ID + "','" + item.CNUMERO + "','" + item.CCODPROVE + "');\"><i class=\"fa-solid fa-trash\"></i></button><button id=\"btnPP_Cancel" + item.NAMECOL + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"CancelPP_Edition('" + item.NAMECOL + "');\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
                tr += "</tr>";
                //console.log(item);
                $("#tabPagoProveedor").DataTable().rows.add($(tr)).draw();
            });

            $.each(r.agent, function (i, item) {
                var tr = "<tr>";
                //tr += "<td style=\"text-align:left;\" width=\"30%\"><div id=\"strPA_FORMAPAGO" + item.NAMECOL + "\">" + item.FORMA_PAGODESCRI + "</div><div class=\"col-sm-12\"><input id=\"txtPA_FORMAPAGO" + item.NAMECOL + "\" value=\"" + item.FORMA_PAGODESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPA_FORMAPAGO" + item.NAMECOL + "Val\" value=\"" + item.FORMA_PAGO + "\" type=\"hidden\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPA_FORMAPAGO2" + item.NAMECOL + "\" value=\"" + item.ARTICULO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:left;\" width=\"30%\"><div id=\"strPA_FORMAPAGO" + item.NAMECOL + "\">" + item.FORMA_PAGODESCRI + "</div><div class=\"col-sm-12\"><select id=\"txtPA_FORMAPAGO" + item.NAMECOL + "\" style=\"display:none;\" class=\"form-select form-select-sm selectpicker\"></select><input id=\"txtPA_FORMAPAGO" + item.NAMECOL + "Val\" value=\"" + item.FORMA_PAGO + "\" type=\"hidden\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPA_IMPORTE" + item.NAMECOL + "\">" + item.IMPORTE + "</div><div class=\"col-sm-12\"><input id=\"txtPA_IMPORTE" + item.NAMECOL + "\" value=\"" + item.IMPORTE + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPA_IMPORTE2" + item.NAMECOL + "\" value=\"" + item.IMPORTE + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPA_FECHAPAGO" + item.NAMECOL + "\">" + item.FECHA_PAGO + "</div><div class=\"col-sm-12\"><input id=\"txtPA_FECHAPAGO" + item.NAMECOL + "\" value=\"" + item.FECHA_PAGO2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPA_FECHAPAGO2" + item.NAMECOL + "\" value=\"" + item.FECHA_PAGO2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"15%\"><div id=\"strPA_BANCO" + item.NAMECOL + "\">" + item.BANCODESCRI + "</div><div class=\"col-sm-12\"><input id=\"txtPA_BANCO" + item.NAMECOL + "\" value=\"" + item.BANCODESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPA_BANCO" + item.NAMECOL + "Val\" value=\"" + item.BANCO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPA_BANCO2" + item.NAMECOL + "\" value=\"" + item.BANCO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPA_OPERACION" + item.NAMECOL + "\">" + item.NUMERO_OPERACION + "</div><div class=\"col-sm-12\"><input id=\"txtPA_OPERACION" + item.NAMECOL + "\" value=\"" + item.NUMERO_OPERACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPA_OPERACION2" + item.NAMECOL + "\" value=\"" + item.NUMERO_OPERACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"25%\"><div id=\"strPA_OBS" + item.NAMECOL + "\">" + item.OBSERVACION + "</div><div class=\"col-sm-12\"><input id=\"txtPA_OBS" + item.NAMECOL + "\" value=\"" + item.OBSERVACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPA_OBS2" + item.NAMECOL + "\" value=\"" + item.OBSERVACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><button id=\"btnPA_Edit" + item.NAMECOL + "\" type=\"button\" class=\"btn btn-outline-purple\" onclick=\"EditPA_Row('" + item.NAMECOL + "');\"><i class=\"fa-solid fa-edit\"></i></button><button id=\"btnPA_Save" + item.NAMECOL + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarPA_Registros('" + item.NAMECOL + "','" + item.CNUMERO + "','" + item.CCODPROVE + "','" + item.PAGOAGENTE_ID + "');\"><i class=\"fa-solid fa-save\"></i></button></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><button id=\"btnPA_Delete" + item.NAMECOL + "\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"EliminarPA_Registro('" + item.PAGOAGENTE_ID + "','" + item.CNUMERO + "','" + item.CCODPROVE + "');\"><i class=\"fa-solid fa-trash\"></i></button><button id=\"btnPA_Cancel" + item.NAMECOL + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"CancelPA_Edition('" + item.NAMECOL + "');\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
                tr += "</tr>";
                //console.log(item);
                $("#tabPagoAgente").DataTable().rows.add($(tr)).draw();
            });

            $.each(r.flete, function (i, item) {
                var tr = "<tr>";
                //tr += "<td style=\"text-align:left;\" width=\"30%\"><div id=\"strPF_FORMAPAGO" + item.NAMECOL + "\">" + item.FORMA_PAGODESCRI + "</div><div class=\"col-sm-12\"><input id=\"txtPF_FORMAPAGO" + item.NAMECOL + "\" value=\"" + item.FORMA_PAGODESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPF_FORMAPAGO" + item.NAMECOL + "Val\" value=\"" + item.FORMA_PAGO + "\" type=\"hidden\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPF_FORMAPAGO2" + item.NAMECOL + "\" value=\"" + item.ARTICULO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:left;\" width=\"30%\"><div id=\"strPF_FORMAPAGO" + item.NAMECOL + "\">" + item.FORMA_PAGODESCRI + "</div><div class=\"col-sm-12\"><select id=\"txtPF_FORMAPAGO" + item.NAMECOL + "\" style=\"display:none;\" class=\"form-select form-select-sm selectpicker\"></select><input id=\"txtPF_FORMAPAGO" + item.NAMECOL + "Val\" value=\"" + item.FORMA_PAGO + "\" type=\"hidden\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPF_IMPORTE" + item.NAMECOL + "\">" + item.IMPORTE + "</div><div class=\"col-sm-12\"><input id=\"txtPF_IMPORTE" + item.NAMECOL + "\" value=\"" + item.IMPORTE + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPF_IMPORTE2" + item.NAMECOL + "\" value=\"" + item.IMPORTE + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPF_FECHAPAGO" + item.NAMECOL + "\">" + item.FECHA_PAGO + "</div><div class=\"col-sm-12\"><input id=\"txtPF_FECHAPAGO" + item.NAMECOL + "\" value=\"" + item.FECHA_PAGO2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPF_FECHAPAGO2" + item.NAMECOL + "\" value=\"" + item.FECHA_PAGO2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"15%\"><div id=\"strPF_BANCO" + item.NAMECOL + "\">" + item.BANCODESCRI + "</div><div class=\"col-sm-12\"><input id=\"txtPF_BANCO" + item.NAMECOL + "\" value=\"" + item.BANCODESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPF_BANCO" + item.NAMECOL + "Val\" value=\"" + item.BANCO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPF_BANCO2" + item.NAMECOL + "\" value=\"" + item.BANCO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPF_OPERACION" + item.NAMECOL + "\">" + item.NUMERO_OPERACION + "</div><div class=\"col-sm-12\"><input id=\"txtPF_OPERACION" + item.NAMECOL + "\" value=\"" + item.NUMERO_OPERACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPF_OPERACION2" + item.NAMECOL + "\" value=\"" + item.NUMERO_OPERACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"25%\"><div id=\"strPF_OBS" + item.NAMECOL + "\">" + item.OBSERVACION + "</div><div class=\"col-sm-12\"><input id=\"txtPF_OBS" + item.NAMECOL + "\" value=\"" + item.OBSERVACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPF_OBS2" + item.NAMECOL + "\" value=\"" + item.OBSERVACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><button id=\"btnPF_Edit" + item.NAMECOL + "\" type=\"button\" class=\"btn btn-outline-purple\" onclick=\"EditPF_Row('" + item.NAMECOL + "');\"><i class=\"fa-solid fa-edit\"></i></button><button id=\"btnPF_Save" + item.NAMECOL + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarPF_Registros('" + item.NAMECOL + "','" + item.CNUMERO + "','" + item.CCODPROVE + "','" + item.PAGOFLETE_ID + "');\"><i class=\"fa-solid fa-save\"></i></button></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><button id=\"btnPF_Delete" + item.NAMECOL + "\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"EliminarPF_Registro('" + item.PAGOFLETE_ID + "','" + item.CNUMERO + "','" + item.CCODPROVE + "');\"><i class=\"fa-solid fa-trash\"></i></button><button id=\"btnPF_Cancel" + item.NAMECOL + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"CancelPF_Edition('" + item.NAMECOL + "');\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
                tr += "</tr>";
                //console.log(item);
                $("#tabPagoFlete").DataTable().rows.add($(tr)).draw();
            });

            $.each(r.varios, function (i, item) {
                var tr = "<tr>";
                //tr += "<td style=\"text-align:left;\" width=\"30%\"><div id=\"strPV_FORMAPAGO" + item.NAMECOL + "\">" + item.FORMA_PAGODESCRI + "</div><div class=\"col-sm-12\"><input id=\"txtPV_FORMAPAGO" + item.NAMECOL + "\" value=\"" + item.FORMA_PAGODESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPV_FORMAPAGO" + item.NAMECOL + "Val\" value=\"" + item.FORMA_PAGO + "\" type=\"hidden\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPV_FORMAPAGO2" + item.NAMECOL + "\" value=\"" + item.ARTICULO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:left;\" width=\"30%\"><div id=\"strPV_FORMAPAGO" + item.NAMECOL + "\">" + item.FORMA_PAGODESCRI + "</div><div class=\"col-sm-12\"><select id=\"txtPV_FORMAPAGO" + item.NAMECOL + "\" style=\"display:none;\" class=\"form-select form-select-sm selectpicker\"></select><input id=\"txtPV_FORMAPAGO" + item.NAMECOL + "Val\" value=\"" + item.FORMA_PAGO + "\" type=\"hidden\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPV_IMPORTE" + item.NAMECOL + "\">" + item.IMPORTE + "</div><div class=\"col-sm-12\"><input id=\"txtPV_IMPORTE" + item.NAMECOL + "\" value=\"" + item.IMPORTE + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPV_IMPORTE2" + item.NAMECOL + "\" value=\"" + item.IMPORTE + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPV_FECHAPAGO" + item.NAMECOL + "\">" + item.FECHA_PAGO + "</div><div class=\"col-sm-12\"><input id=\"txtPV_FECHAPAGO" + item.NAMECOL + "\" value=\"" + item.FECHA_PAGO2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPV_FECHAPAGO2" + item.NAMECOL + "\" value=\"" + item.FECHA_PAGO2 + "\" type=\"date\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"15%\"><div id=\"strPV_BANCO" + item.NAMECOL + "\">" + item.BANCODESCRI + "</div><div class=\"col-sm-12\"><input id=\"txtPV_BANCO" + item.NAMECOL + "\" value=\"" + item.BANCODESCRI + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPV_BANCO" + item.NAMECOL + "Val\" value=\"" + item.BANCO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPV_BANCO2" + item.NAMECOL + "\" value=\"" + item.BANCO + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"10%\"><div id=\"strPV_OPERACION" + item.NAMECOL + "\">" + item.NUMERO_OPERACION + "</div><div class=\"col-sm-12\"><input id=\"txtPV_OPERACION" + item.NAMECOL + "\" value=\"" + item.NUMERO_OPERACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPV_OPERACION2" + item.NAMECOL + "\" value=\"" + item.NUMERO_OPERACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"25%\"><div id=\"strPV_OBS" + item.NAMECOL + "\">" + item.OBSERVACION + "</div><div class=\"col-sm-12\"><input id=\"txtPV_OBS" + item.NAMECOL + "\" value=\"" + item.OBSERVACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/><input id=\"txtPV_OBS2" + item.NAMECOL + "\" value=\"" + item.OBSERVACION + "\" type=\"text\" style=\"display:none;\" class=\"form-control form-control-sm\"/></div></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><button id=\"btnPV_Edit" + item.NAMECOL + "\" type=\"button\" class=\"btn btn-outline-purple\" onclick=\"EditPV_Row('" + item.NAMECOL + "');\"><i class=\"fa-solid fa-edit\"></i></button><button id=\"btnPV_Save" + item.NAMECOL + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarPV_Registros('" + item.NAMECOL + "','" + item.CNUMERO + "','" + item.CCODPROVE + "','" + item.PAGOVARIOS_ID + "');\"><i class=\"fa-solid fa-save\"></i></button></td>";
                tr += "<td style=\"text-align:center;\" width=\"5%\"><button id=\"btnPV_Delete" + item.NAMECOL + "\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"EliminarPV_Registro('" + item.PAGOFLETE_ID + "','" + item.CNUMERO + "','" + item.CCODPROVE + "');\"><i class=\"fa-solid fa-trash\"></i></button><button id=\"btnPV_Cancel" + item.NAMECOL + "\" style=\"display:none;\" type=\"button\" class=\"btn btn-outline-danger\" onclick=\"CancelPV_Edition('" + item.NAMECOL + "');\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
                tr += "</tr>";
                //console.log(item);
                $("#tabPagoVarios").DataTable().rows.add($(tr)).draw();
            });
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

$('#btnPP_Nuevo').click(function (e) {

    var codDato = $('#txtCODIGOIMPOR2').val();
    var codDatoProv = $('#txtCODIGOPROVEE2').val();

    var tr = "<tr>";
    tr += "<td style=\"text-align:center;\" width=\"30%\"><div class=\"col-sm-12\"><select id=\"txtPP_FORMAPAGO\" style=\"display:none;\" class=\"form-select form-select-sm selectpicker\"></select></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPP_IMPORTE\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPP_FECHAPAGO\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"15%\"><div class=\"col-sm-12\"><input id=\"txtPP_BANCO\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtPP_BANCOVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPP_OPERACION\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"25%\"><div class=\"col-sm-12\"><textarea rows=\"2\" id=\"txtPP_OBS\" type=\"text\" class=\"form-control form-control-sm\"></textarea></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"5%\"><button type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarPP_Registros('', '" + codDato + "','" + codDatoProv + "',0)\"><i class=\"fa-solid fa-save\"></i></button></td>";
    tr += "<td style=\"text-align:center;\" width=\"5%\"><button type=\"button\" class=\"btn btn-outline-danger\" onclick=\"ObtenerPagoProvee('" + codDato + "','" + codDatoProv + "','');\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
    tr += "</tr>";

    $("#tabPagoProveedor").DataTable().row.add($(tr)).draw(false);
    AutocompletePP("");
});

$('#btnPA_Nuevo').click(function (e) {

    var codDato = $('#txtCODIGOIMPOR2').val();
    var codDatoProv = $('#txtCODIGOPROVEE2').val();

    var tr = "<tr>";
    //tr += "<td style=\"text-align:center;\" width=\"30%\"><div class=\"col-sm-12\"><input id=\"txtPA_FORMAPAGO\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtPA_FORMAPAGOVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"30%\"><div class=\"col-sm-12\"><select id=\"txtPA_FORMAPAGO\" style=\"display:none;\" class=\"form-select form-select-sm selectpicker\"></select></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPA_IMPORTE\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPA_FECHAPAGO\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"15%\"><div class=\"col-sm-12\"><input id=\"txtPA_BANCO\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtPA_BANCOVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPA_OPERACION\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"25%\"><div class=\"col-sm-12\"><textarea rows=\"2\" id=\"txtPA_OBS\" type=\"text\" class=\"form-control form-control-sm\"></textarea></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"5%\"><button type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarPA_Registros('', '" + codDato + "','" + codDatoProv + "',0)\"><i class=\"fa-solid fa-save\"></i></button></td>";
    tr += "<td style=\"text-align:center;\" width=\"5%\"><button type=\"button\" class=\"btn btn-outline-danger\" onclick=\"ObtenerPagoProvee('" + codDato + "','" + codDatoProv + "','');\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
    tr += "</tr>";

    $("#tabPagoAgente").DataTable().row.add($(tr)).draw(false);
    AutocompletePA("");
});

$('#btnPF_Nuevo').click(function (e) {

    var codDato = $('#txtCODIGOIMPOR2').val();
    var codDatoProv = $('#txtCODIGOPROVEE2').val();

    var tr = "<tr>";
    //tr += "<td style=\"text-align:center;\" width=\"30%\"><div class=\"col-sm-12\"><input id=\"txtPF_FORMAPAGO\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtPF_FORMAPAGOVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"30%\"><div class=\"col-sm-12\"><select id=\"txtPF_FORMAPAGO\" style=\"display:none;\" class=\"form-select form-select-sm selectpicker\"></select></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPF_IMPORTE\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPF_FECHAPAGO\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"15%\"><div class=\"col-sm-12\"><input id=\"txtPF_BANCO\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtPF_BANCOVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPF_OPERACION\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"25%\"><div class=\"col-sm-12\"><textarea rows=\"2\" id=\"txtPF_OBS\" type=\"text\" class=\"form-control form-control-sm\"></textarea></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"5%\"><button type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarPF_Registros('', '" + codDato + "','" + codDatoProv + "',0)\"><i class=\"fa-solid fa-save\"></i></button></td>";
    tr += "<td style=\"text-align:center;\" width=\"5%\"><button type=\"button\" class=\"btn btn-outline-danger\" onclick=\"ObtenerPagoProvee('" + codDato + "','" + codDatoProv + "','');\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
    tr += "</tr>";

    $("#tabPagoFlete").DataTable().row.add($(tr)).draw(false);
    AutocompletePF("");
});

$('#btnPV_Nuevo').click(function (e) {

    var codDato = $('#txtCODIGOIMPOR2').val();
    var codDatoProv = $('#txtCODIGOPROVEE2').val();

    var tr = "<tr>";
    //tr += "<td style=\"text-align:center;\" width=\"30%\"><div class=\"col-sm-12\"><input id=\"txtPV_FORMAPAGO\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtPV_FORMAPAGOVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"30%\"><div class=\"col-sm-12\"><select id=\"txtPV_FORMAPAGO\" style=\"display:none;\" class=\"form-select form-select-sm selectpicker\"></select></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPV_IMPORTE\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPV_FECHAPAGO\" type=\"date\" value=\"1900-01-01\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"15%\"><div class=\"col-sm-12\"><input id=\"txtPV_BANCO\" type=\"text\" class=\"form-control form-control-sm\"/><input id=\"txtPV_BANCOVal\" type=\"hidden\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"10%\"><div class=\"col-sm-12\"><input id=\"txtPV_OPERACION\" type=\"text\" class=\"form-control form-control-sm\"/></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"25%\"><div class=\"col-sm-12\"><textarea rows=\"2\" id=\"txtPV_OBS\" type=\"text\" class=\"form-control form-control-sm\"></textarea></div></td>";
    tr += "<td style=\"text-align:center;\" width=\"5%\"><button type=\"button\" class=\"btn btn-outline-success\" onclick=\"GuardarPV_Registros('', '" + codDato + "','" + codDatoProv + "',0)\"><i class=\"fa-solid fa-save\"></i></button></td>";
    tr += "<td style=\"text-align:center;\" width=\"5%\"><button type=\"button\" class=\"btn btn-outline-danger\" onclick=\"ObtenerPagoProvee('" + codDato + "','" + codDatoProv + "', '');\"><i class=\"fa-solid fa-times-circle\"></i></button></td>";
    tr += "</tr>";

    $("#tabPagoVarios").DataTable().row.add($(tr)).draw(false);
    AutocompletePV("");
});

function EditPP_Row(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");
    $("#txtPP_FORMAPAGO" + dato).removeAttr("style");
    $("#txtPP_IMPORTE" + dato).removeAttr("style");
    $("#txtPP_FECHAPAGO" + dato).removeAttr("style");
    $("#txtPP_BANCO" + dato).removeAttr("style");
    $("#txtPP_OPERACION" + dato).removeAttr("style");
    $("#txtPP_OBS" + dato).removeAttr("style");
    $("#btnPP_Save" + dato).removeAttr("style");
    $("#btnPP_Cancel" + dato).removeAttr("style");

    //$("#strN_IMP" + dato).attr("style", "display:none");
    $("#strPP_FORMAPAGO" + dato).attr("style", "display:none");
    $("#strPP_IMPORTE" + dato).attr("style", "display:none");
    $("#strPP_FECHAPAGO" + dato).attr("style", "display:none");
    $("#strPP_BANCO" + dato).attr("style", "display:none");
    $("#strPP_OPERACION" + dato).attr("style", "display:none");
    $("#strPP_OBS" + dato).attr("style", "display:none");
    $("#btnPP_Edit" + dato).attr("style", "display:none");
    $("#btnPP_Delete" + dato).attr("style", "display:none");

    AutocompletePP(dato);
}

function CancelPP_Edition(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");

    $("#txtPP_FORMAPAGO" + dato).attr("style", "display:none");
    $("#txtPP_IMPORTE" + dato).attr("style", "display:none");
    $("#txtPP_FECHAPAGO" + dato).attr("style", "display:none");
    $("#txtPP_BANCO" + dato).attr("style", "display:none");
    $("#txtPP_OPERACION" + dato).attr("style", "display:none");
    $("#txtPP_OBS" + dato).attr("style", "display:none");

    $("#txtPP_FORMAPAGO" + dato).val($("#txtPP_FORMAPAGO2" + dato).val());
    $("#txtPP_IMPORTE" + dato).val($("#txtPP_IMPORTE2" + dato).val());
    $("#txtPP_FECHAPAGO" + dato).val($("#txtPP_FECHAPAGO2" + dato).val());
    $("#txtPP_BANCO" + dato).val($("#txtPP_BANCO2" + dato).val());
    $("#txtPP_OPERACION" + dato).val($("#txtPP_OPERACION2" + dato).val());
    $("#txtPP_OBS" + dato).val($("#txtPP_OBS2" + dato).val());

    $("#strPP_FORMAPAGO" + dato).removeAttr("style");
    $("#strPP_IMPORTE" + dato).removeAttr("style");
    $("#strPP_FECHAPAGO" + dato).removeAttr("style");
    $("#strPP_BANCO" + dato).removeAttr("style");
    $("#strPP_OPERACION" + dato).removeAttr("style");
    $("#strPP_OBS" + dato).removeAttr("style");

    $("#btnPP_Save" + dato).attr("style", "display:none");
    $("#btnPP_Cancel" + dato).attr("style", "display:none");
    $("#btnPP_Edit" + dato).removeAttr("style");
    $("#btnPP_Delete" + dato).removeAttr("style");
}

function EditPA_Row(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");
    $("#txtPA_FORMAPAGO" + dato).removeAttr("style");
    $("#txtPA_IMPORTE" + dato).removeAttr("style");
    $("#txtPA_FECHAPAGO" + dato).removeAttr("style");
    $("#txtPA_BANCO" + dato).removeAttr("style");
    $("#txtPA_OPERACION" + dato).removeAttr("style");
    $("#txtPA_OBS" + dato).removeAttr("style");
    $("#btnPA_Save" + dato).removeAttr("style");
    $("#btnPA_Cancel" + dato).removeAttr("style");

    //$("#strN_IMP" + dato).attr("style", "display:none");
    $("#strPA_FORMAPAGO" + dato).attr("style", "display:none");
    $("#strPA_IMPORTE" + dato).attr("style", "display:none");
    $("#strPA_FECHAPAGO" + dato).attr("style", "display:none");
    $("#strPA_BANCO" + dato).attr("style", "display:none");
    $("#strPA_OPERACION" + dato).attr("style", "display:none");
    $("#strPA_OBS" + dato).attr("style", "display:none");
    $("#btnPA_Edit" + dato).attr("style", "display:none");
    $("#btnPA_Delete" + dato).attr("style", "display:none");

    AutocompletePA(dato);
}

function CancelPA_Edition(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");

    $("#txtPA_FORMAPAGO" + dato).attr("style", "display:none");
    $("#txtPA_IMPORTE" + dato).attr("style", "display:none");
    $("#txtPA_FECHAPAGO" + dato).attr("style", "display:none");
    $("#txtPA_BANCO" + dato).attr("style", "display:none");
    $("#txtPA_OPERACION" + dato).attr("style", "display:none");
    $("#txtPA_OBS" + dato).attr("style", "display:none");

    $("#txtPA_FORMAPAGO" + dato).val($("#txtPA_FORMAPAGO2" + dato).val());
    $("#txtPA_IMPORTE" + dato).val($("#txtPA_IMPORTE2" + dato).val());
    $("#txtPA_FECHAPAGO" + dato).val($("#txtPA_FECHAPAGO2" + dato).val());
    $("#txtPA_BANCO" + dato).val($("#txtPA_BANCO2" + dato).val());
    $("#txtPA_OPERACION" + dato).val($("#txtPA_OPERACION2" + dato).val());
    $("#txtPA_OBS" + dato).val($("#txtPA_OBS2" + dato).val());

    $("#strPA_FORMAPAGO" + dato).removeAttr("style");
    $("#strPA_IMPORTE" + dato).removeAttr("style");
    $("#strPA_FECHAPAGO" + dato).removeAttr("style");
    $("#strPA_BANCO" + dato).removeAttr("style");
    $("#strPA_OPERACION" + dato).removeAttr("style");
    $("#strPA_OBS" + dato).removeAttr("style");

    $("#btnPA_Save" + dato).attr("style", "display:none");
    $("#btnPA_Cancel" + dato).attr("style", "display:none");
    $("#btnPA_Edit" + dato).removeAttr("style");
    $("#btnPA_Delete" + dato).removeAttr("style");
}

function EditPF_Row(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");
    $("#txtPF_FORMAPAGO" + dato).removeAttr("style");
    $("#txtPF_IMPORTE" + dato).removeAttr("style");
    $("#txtPF_FECHAPAGO" + dato).removeAttr("style");
    $("#txtPF_BANCO" + dato).removeAttr("style");
    $("#txtPF_OPERACION" + dato).removeAttr("style");
    $("#txtPF_OBS" + dato).removeAttr("style");
    $("#btnPF_Save" + dato).removeAttr("style");
    $("#btnPF_Cancel" + dato).removeAttr("style");

    //$("#strN_IMP" + dato).attr("style", "display:none");
    $("#strPF_FORMAPAGO" + dato).attr("style", "display:none");
    $("#strPF_IMPORTE" + dato).attr("style", "display:none");
    $("#strPF_FECHAPAGO" + dato).attr("style", "display:none");
    $("#strPF_BANCO" + dato).attr("style", "display:none");
    $("#strPF_OPERACION" + dato).attr("style", "display:none");
    $("#strPF_OBS" + dato).attr("style", "display:none");
    $("#btnPF_Edit" + dato).attr("style", "display:none");
    $("#btnPF_Delete" + dato).attr("style", "display:none");

    AutocompletePF(dato);
}

function CancelPF_Edition(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");

    $("#txtPF_FORMAPAGO" + dato).attr("style", "display:none");
    $("#txtPF_IMPORTE" + dato).attr("style", "display:none");
    $("#txtPF_FECHAPAGO" + dato).attr("style", "display:none");
    $("#txtPF_BANCO" + dato).attr("style", "display:none");
    $("#txtPF_OPERACION" + dato).attr("style", "display:none");
    $("#txtPF_OBS" + dato).attr("style", "display:none");

    $("#txtPF_FORMAPAGO" + dato).val($("#txtPF_FORMAPAGO2" + dato).val());
    $("#txtPF_IMPORTE" + dato).val($("#txtPF_IMPORTE2" + dato).val());
    $("#txtPF_FECHAPAGO" + dato).val($("#txtPF_FECHAPAGO2" + dato).val());
    $("#txtPF_BANCO" + dato).val($("#txtPF_BANCO2" + dato).val());
    $("#txtPF_OPERACION" + dato).val($("#txtPF_OPERACION2" + dato).val());
    $("#txtPF_OBS" + dato).val($("#txtPF_OBS2" + dato).val());

    $("#strPF_FORMAPAGO" + dato).removeAttr("style");
    $("#strPF_IMPORTE" + dato).removeAttr("style");
    $("#strPF_FECHAPAGO" + dato).removeAttr("style");
    $("#strPF_BANCO" + dato).removeAttr("style");
    $("#strPF_OPERACION" + dato).removeAttr("style");
    $("#strPF_OBS" + dato).removeAttr("style");

    $("#btnPF_Save" + dato).attr("style", "display:none");
    $("#btnPF_Cancel" + dato).attr("style", "display:none");
    $("#btnPF_Edit" + dato).removeAttr("style");
    $("#btnPF_Delete" + dato).removeAttr("style");
}

function EditPV_Row(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");
    $("#txtPV_FORMAPAGO" + dato).removeAttr("style");
    $("#txtPV_IMPORTE" + dato).removeAttr("style");
    $("#txtPV_FECHAPAGO" + dato).removeAttr("style");
    $("#txtPV_BANCO" + dato).removeAttr("style");
    $("#txtPV_OPERACION" + dato).removeAttr("style");
    $("#txtPV_OBS" + dato).removeAttr("style");
    $("#btnPV_Save" + dato).removeAttr("style");
    $("#btnPV_Cancel" + dato).removeAttr("style");

    //$("#strN_IMP" + dato).attr("style", "display:none");
    $("#strPV_FORMAPAGO" + dato).attr("style", "display:none");
    $("#strPV_IMPORTE" + dato).attr("style", "display:none");
    $("#strPV_FECHAPAGO" + dato).attr("style", "display:none");
    $("#strPV_BANCO" + dato).attr("style", "display:none");
    $("#strPV_OPERACION" + dato).attr("style", "display:none");
    $("#strPV_OBS" + dato).attr("style", "display:none");
    $("#btnPV_Edit" + dato).attr("style", "display:none");
    $("#btnPV_Delete" + dato).attr("style", "display:none");

    AutocompletePV(dato);
}

function CancelPV_Edition(dato) {

    //$("#txtN_IMP" + dato).removeAttr("style");

    $("#txtPV_FORMAPAGO" + dato).attr("style", "display:none");
    $("#txtPV_IMPORTE" + dato).attr("style", "display:none");
    $("#txtPV_FECHAPAGO" + dato).attr("style", "display:none");
    $("#txtPV_BANCO" + dato).attr("style", "display:none");
    $("#txtPV_OPERACION" + dato).attr("style", "display:none");
    $("#txtPV_OBS" + dato).attr("style", "display:none");

    $("#txtPV_FORMAPAGO" + dato).val($("#txtPV_FORMAPAGO2" + dato).val());
    $("#txtPV_IMPORTE" + dato).val($("#txtPV_IMPORTE2" + dato).val());
    $("#txtPV_FECHAPAGO" + dato).val($("#txtPV_FECHAPAGO2" + dato).val());
    $("#txtPV_BANCO" + dato).val($("#txtPV_BANCO2" + dato).val());
    $("#txtPV_OPERACION" + dato).val($("#txtPV_OPERACION2" + dato).val());
    $("#txtPV_OBS" + dato).val($("#txtPV_OBS2" + dato).val());

    $("#strPV_FORMAPAGO" + dato).removeAttr("style");
    $("#strPV_IMPORTE" + dato).removeAttr("style");
    $("#strPV_FECHAPAGO" + dato).removeAttr("style");
    $("#strPV_BANCO" + dato).removeAttr("style");
    $("#strPV_OPERACION" + dato).removeAttr("style");
    $("#strPV_OBS" + dato).removeAttr("style");

    $("#btnPV_Save" + dato).attr("style", "display:none");
    $("#btnPV_Cancel" + dato).attr("style", "display:none");
    $("#btnPV_Edit" + dato).removeAttr("style");
    $("#btnPV_Delete" + dato).removeAttr("style");
}

function GuardarPP_Registros(namecol, codigo, codigoprov, item) {

    var cod = codigo;
    var provee = codigoprov;
    var sec = item;
    var fp = $('#txtPP_FORMAPAGO' + namecol).val();
    //var fpVal = $('#txtPP_FORMAPAGO' + namecol + 'Val').val();
    var imp = $('#txtPP_IMPORTE' + namecol).val();
    var fech = $('#txtPP_FECHAPAGO' + namecol).val();
    var banc = $('#txtPP_BANCO' + namecol).val();
    var bancVal = $('#txtPP_BANCO' + namecol + 'Val').val();
    var oper = $('#txtPP_OPERACION' + namecol).val();
    var obs = $('#txtPP_OBS' + namecol).val();

    var json = {
        PAGOPROVEEDOR_ID: sec,
        CNUMERO: cod,
        CCODPROVE: codigoprov,
        FORMA_PAGO: fp, // (fp === "" ? "" : fpVal),
        IMPORTE: imp,
        FECHA_PAGO: fech,
        BANCO: (banc === "" ? "" : bancVal),
        NUMERO_OPERACION: oper,
        OBSERVACION: obs
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-PagoProveedor-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                ObtenerPagoProvee(codigo, codigoprov, '');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function GuardarPA_Registros(namecol, codigo, codigoprov, item) {

    var cod = codigo;
    var provee = codigoprov;
    var sec = item;
    var fp = $('#txtPA_FORMAPAGO' + namecol).val();
    //var fpVal = $('#txtPA_FORMAPAGO' + namecol + 'Val').val();
    var imp = $('#txtPA_IMPORTE' + namecol).val();
    var fech = $('#txtPA_FECHAPAGO' + namecol).val();
    var banc = $('#txtPA_BANCO' + namecol).val();
    var bancVal = $('#txtPA_BANCO' + namecol + 'Val').val();
    var oper = $('#txtPA_OPERACION' + namecol).val();
    var obs = $('#txtPA_OBS' + namecol).val();

    var json = {
        PAGOAGENTE_ID: sec,
        CNUMERO: cod,
        CCODPROVE: codigoprov,
        FORMA_PAGO: fp,//(fp === "" ? "" : fpVal),
        IMPORTE: imp,
        FECHA_PAGO: fech,
        BANCO: (banc === "" ? "" : bancVal),
        NUMERO_OPERACION: oper,
        OBSERVACION: obs
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-PagoAgente-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                ObtenerPagoProvee(codigo, codigoprov, '');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function GuardarPF_Registros(namecol, codigo, codigoprov, item) {

    var cod = codigo;
    var provee = codigoprov;
    var sec = item;
    var fp = $('#txtPF_FORMAPAGO' + namecol).val();
    //var fpVal = $('#txtPF_FORMAPAGO' + namecol + 'Val').val();
    var imp = $('#txtPF_IMPORTE' + namecol).val();
    var fech = $('#txtPF_FECHAPAGO' + namecol).val();
    var banc = $('#txtPF_BANCO' + namecol).val();
    var bancVal = $('#txtPF_BANCO' + namecol + 'Val').val();
    var oper = $('#txtPF_OPERACION' + namecol).val();
    var obs = $('#txtPF_OBS' + namecol).val();

    var json = {
        PAGOFLETE_ID: sec,
        CNUMERO: cod,
        CCODPROVE: codigoprov,
        FORMA_PAGO: fp,//(fp === "" ? "" : fpVal),
        IMPORTE: imp,
        FECHA_PAGO: fech,
        BANCO: (banc === "" ? "" : bancVal),
        NUMERO_OPERACION: oper,
        OBSERVACION: obs
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-PagoFlete-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                ObtenerPagoProvee(codigo, codigoprov, '');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function GuardarPV_Registros(namecol, codigo, codigoprov, item) {

    var cod = codigo;
    var provee = codigoprov;
    var sec = item;
    var fp = $('#txtPV_FORMAPAGO' + namecol).val();
    //var fpVal = $('#txtPV_FORMAPAGO' + namecol + 'Val').val();
    var imp = $('#txtPV_IMPORTE' + namecol).val();
    var fech = $('#txtPV_FECHAPAGO' + namecol).val();
    var banc = $('#txtPV_BANCO' + namecol).val();
    var bancVal = $('#txtPV_BANCO' + namecol + 'Val').val();
    var oper = $('#txtPV_OPERACION' + namecol).val();
    var obs = $('#txtPV_OBS' + namecol).val();

    var json = {
        PAGOVARIOS_ID: sec,
        CNUMERO: cod,
        CCODPROVE: codigoprov,
        FORMA_PAGO: fp,//(fp === "" ? "" : fpVal),
        IMPORTE: imp,
        FECHA_PAGO: fech,
        BANCO: (banc === "" ? "" : bancVal),
        NUMERO_OPERACION: oper,
        OBSERVACION: obs
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-PagoVarios-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                ObtenerPagoProvee(codigo, codigoprov, '');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function EliminarPP_Registro(codigo, cnumero, cprovee) {

    var json = {
        term: codigo
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/eliminar-PagoProveedor-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                ObtenerPagoProvee(cnumero, cprovee, '');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function EliminarPA_Registro(codigo, cnumero, cprovee) {

    var json = {
        term: codigo
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/eliminar-PagoAgente-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                ObtenerPagoProvee(cnumero, cprovee, '');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function EliminarPF_Registro(codigo, cnumero, cprovee) {

    var json = {
        term: codigo
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/eliminar-PagoFlete-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                ObtenerPagoProvee(cnumero, cprovee, '');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function EliminarPV_Registro(codigo, cnumero, cprovee) {

    var json = {
        term: codigo
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/eliminar-PagoVarios-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                ObtenerPagoProvee(cnumero, cprovee, '');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function autocompleteFP(_control, _controlVal) {

    var json = {
        value: 9,
        term: ""
    };
    //console.log(json);
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-Autocompletar_Imp",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (data) {

            control = _control;
            control.empty();
            control.append('<option value="">SELECCIONE</option>');
            $.each(data, function (i, item) {
                control.append('<option value=' + item.FORMAPAGO_ID + '>' + item.DESCRIPCION + '</option>');
            });
            control.val(_controlVal);
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function AutocompletePP(dato) {

    autocompleteFP($("#txtPP_FORMAPAGO" + dato), $("#txtPP_FORMAPAGO" + dato + "Val").val());

    $("#txtPP_BANCO" + dato).autocomplete({
        appendTo: "#modalPagoProveedor",
        source: function (request, response) {

            var json = {
                value: 10,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Autocompletar_Imp",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.ID,
                            label: item.DESCRIPCION,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtPP_BANCO' + dato).val(i.item.label);
            $('#txtPP_BANCO' + dato + 'Val').val(i.item.val);
        }
    });
};

function AutocompletePA(dato) {

    autocompleteFP($("#txtPA_FORMAPAGO" + dato), $("#txtPA_FORMAPAGO" + dato + "Val").val());

    $("#txtPA_BANCO" + dato).autocomplete({
        appendTo: "#modalPagoProveedor",
        source: function (request, response) {

            var json = {
                value: 10,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Autocompletar_Imp",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.ID,
                            label: item.DESCRIPCION,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtPA_BANCO' + dato).val(i.item.label);
            $('#txtPA_BANCO' + dato + 'Val').val(i.item.val);
        }
    });
};

function AutocompletePF(dato) {

    autocompleteFP($("#txtPF_FORMAPAGO" + dato), $("#txtPF_FORMAPAGO" + dato + "Val").val());

    $("#txtPF_BANCO" + dato).autocomplete({
        appendTo: "#modalPagoProveedor",
        source: function (request, response) {

            var json = {
                value: 10,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Autocompletar_Imp",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.ID,
                            label: item.DESCRIPCION,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtPF_BANCO' + dato).val(i.item.label);
            $('#txtPF_BANCO' + dato + 'Val').val(i.item.val);
        }
    });
};

function AutocompletePV(dato) {

    autocompleteFP($("#txtPV_FORMAPAGO" + dato), $("#txtPV_FORMAPAGO" + dato + "Val").val());

    $("#txtPV_BANCO" + dato).autocomplete({
        appendTo: "#modalPagoProveedor",
        source: function (request, response) {

            var json = {
                value: 10,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Autocompletar_Imp",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.ID,
                            label: item.DESCRIPCION,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtPV_BANCO' + dato).val(i.item.label);
            $('#txtPV_BANCO' + dato + 'Val').val(i.item.val);
        }
    });
};

// Agencias
function ModalAgencia_Edit(codigo, textagen, valagen, docenv, llegliq, nliq, cour) {

    $('#lblDesripcion3').html("REGISTO DE AGENCIA == NRO " + codigo);
    $('#modalAgencia').modal('show');

    $('#txtCODIGOIMPOR3').val(codigo);

    //$('#txtModAgencia').val(textagen);
    $('#selAgencia').val(valagen);
    $('#txtDocEnvAgencia').val(docenv);
    $('#txtLlegaLiq').val(llegliq);
    $('#txtNLiqui').val(nliq);
    //$('#txtCourier').val(cour);
}

function ModalProveedor_Edit(codigo, valprov, fact, fechafact, cour) {

    $('#lblDesripcion4').html("REGISTO DE PROVEEDOR == NRO " + codigo);
    $('#modalProveedor').modal('show');

    $('#txtCODIGOIMPOR4').val(codigo);

    $('#selProveedor').val(valprov);
    $('#txtFactura').val(fact);
    $('#txtFEFactura').val(fechafact);
    $('#txtCourier').val(cour);
}

function ModalProveedorFlete_Edit(codigo, valprov, fact, fechafact) {

    $('#lblDesripcion5').html("REGISTO DE PROVEEDOR FLETE == NRO " + codigo);
    $('#modalProveedorFlete').modal('show');

    $('#txtCODIGOIMPOR5').val(codigo);

    $('#selProveedorFlete').val(valprov);
    $('#txtFacturaFlete').val(fact);
    $('#txtFEFacturaFlete').val(fechafact);
}

function CierreImp_Registro(cnumero, cflag) {

    var json = {
        CNUMERO: cnumero,
        CFLAG: cflag
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-cierreimp-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerLista();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}
