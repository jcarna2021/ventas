﻿var id_vend = '00';
var id_mone = 'MN';
var id_fpga = '00';
var id_lpre = '01';
var id_mpga = '00';
var id_tdoc = '1246';
var id_almc = '01';
var ruc = '20601636086';
var id_tdv = 'PD';
var table;

var productos = [];

const formatter = new Intl.NumberFormat('es-PE', {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

function getDate() {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}

$(document).ready(function () {
    //$('#txtFinicio').val(getDate());
    //$('#txtFfinal').val(getDate());

    obtenerCartera();
});

function obtenerCartera() {

    var json = {
        term: ""
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-MotivosNoContab-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            console.log(r);
            table = $('#tabMotivos').DataTable({
                "bDestroy": true,
                "aaData": r,
                "bPaginate": false,
                "bFilter": false,
                scrollCollapse: true,
                scrollY: '250px',
                "bInfo": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                columns: [
                    //{ "data": "AVAL" },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.MotivoId
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.descripcion
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.flag_i_s
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                            if (o.utilizado === 0) {
                                ck += '<button class="btn btn-sm btn-default" onclick="editarRegistro(\'' + o.MotivoId + '\');"><i class="fa-solid fa-edit"></i></button>';
                            }
                            else {
                                ck += '<button class="btn btn-sm btn-default" onclick="usado();"><i class="fa-solid fa-edit"></i></button>';
                            }
                            ck += '<button class="btn btn-sm btn-danger" onclick="eliminarRegistro(\'' + o.MotivoId + '\');"><i class="fa-solid fa-trash"></i></button>';
                            return ck;
                        }
                    }
                ]
            });
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function Limpiar() {
    $('#txtCodigo').val("");
    $('#txtDescripcion').val("");
    $('#dllTipo').val("I");
}

$('#btnNuevo').click(function (e) {
    $('#lblDesripcion').html("NUEVO TIPO");
    $('#modalPrincipal').modal('show');
    Limpiar();
});

function usado() {
    swal("ADVERTENCIA!!!", "El motivo no puede ser editado, cuanta con asignaciones realizadas.", "warning");
}

function editarRegistro(dato) {
    $('#lblDesripcion').html("ACTUALIZAR TIPO");
    $('#modalPrincipal').modal('show');
    Limpiar();

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-MotivosNoContab-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (x) {

            var r = x[0];

            $('#txtCodigo').val(r.MotivoId);
            $('#txtDescripcion').val(r.descripcion);
            $('#dllTipo').val(r.flag_i_s);
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

$('#btnGrabar').click(function (e) {
    SaveRegister();
});

function SaveRegister() {

    var ddlTip = $('#dllTipo').val();
    var cod = $('#txtCodigo').val();

    var registro = {}
    registro["MotivoId"] = (cod == "" ? 0 : cod);
    registro["descripcion"] = $('#txtDescripcion').val();
    registro["flag_i_s"] = ddlTip;

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-MotivosNoContab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(registro),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerCartera();
                Limpiar();
                $('#modalPrincipal').modal('hide');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function eliminarRegistro(dato) {

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/elimina-MotivosNoContab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerCartera();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}