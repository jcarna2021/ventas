﻿var id_vend = '00';
var id_mone = 'MN';
var id_fpga = '00';
var id_lpre = '01';
var id_mpga = '00';
var id_tdoc = '1246';
var id_almc = '01';
var ruc = '20601636086';
var id_tdv = 'PD';
var table;

var productos = [];

const formatter = new Intl.NumberFormat('es-PE', {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

function getDate() {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}

$(document).ready(function () {
    $('#txtFinicio').val(getDate());
    $('#txtFfinal').val(getDate());
    $('#FRegistro').val(getDate());

    obtenerCartera();
    obtenerMotivos();
    obteneraNEXOS();
});

$('#btnBuscar').click(function (e) {
    // new location
    obtenerCartera();
});

$('#btnclose').click(function (e) {
    // new location
    obtenerCartera();
});

$('#btncloseX').click(function (e) {
    // new location
    obtenerCartera();
});

function obtenerCartera() {

    var json = {
        FInicio: $('#txtFinicio').val(),
        FTermino: $('#txtFfinal').val()
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-CajaNoContabCab-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            //console.log("aquiiiiiiiiiiiiiiiiiii");
            //console.log(r);
            table = $('#tabCajaCab').DataTable({
                "bDestroy": true,
                "aaData": r,
                "bPaginate": false,
                "bFilter": false,
                scrollCollapse: true,
                scrollY: '250px',
                "bInfo": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                ordering: false,
                columns: [
                    //{ "data": "AVAL" },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.Fecha
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.INGRESO
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.SALIDA
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.SALDO
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                            ck += '<button class="btn btn-sm btn-default" onclick="obtenerCarteraDet(\'' + o.Fecha + " - " + (o.CIERRE === true ? 'CERRADO' : 'ABIERTO') + '\',\'' + o.FechaFiltro + '\',\'' + o.CIERRE +'\');"><i class="fa-solid fa-magnifying-glass"></i></button>';
                            
                            return ck;
                        }
                    }
                    ,
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                          if (o.CIERRE === true) {
                                ck += '<button class="btn btn-sm btn-danger"><i class="fa-solid fa-lock"></i></button>';
                            }
                            else {
                                ck += '<button class="btn btn-sm btn-success"><i class="fa-solid fa-lock-open"></i></button>';
                            }
                        return ck;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';

                            if (o.CIERRE === true) {
                                ck += '<a href="' + (root + "api/app/genera-pdf-CajaNoContab/" + o.FechaReporte) + '" class="btn btn-sm btn-default" target="_blank"><i class="fa-solid fa-eye" ></i></a>';
                            }
                            else {
                                ck += '<button class="btn btn-sm btn-default" onclick=\"cajacerrada();\"><i class="fa-solid fa-eye"></i></button>';
                            }
                         
                            return ck;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';

                            if (o.CIERRE === true) {
                                ck += '<button class="btn btn-sm btn-danger" onclick=\"caja2cerrada();\"><i class="fa-solid fa-trash"></i></button>';
                            }
                            else {
                                ck += '<button class="btn btn-sm btn-danger" onclick=\"eliminarTodoRegistro(\'' + o.FechaFiltro + '\')\"><i class="fa-solid fa-trash"></i></button>';
                            }

                            return ck;
                        }
                    }
                ]
            });
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function cajacerrada() {
    swal("ADVERTENCIA!!!", "El reporte no puede ser visualizado, verifique si la caja esta cerrada.", "warning");
}

function caja2cerrada() {
    swal("ADVERTENCIA!!!", "No es posible eliminar una caja cerrada, verifique si la caja esta cerrada.", "warning");
}

function obtenerCarteraDet(texto, fecha, cierre) {

    $('#txtSaldoInicial').val("0.00");
    $("#tabCaja > tbody:last").children().remove();
    $('#txtIngreso').val("0.00");
    $('#txtSalida').val("0.00");
    $('#txtSaldo').val("0.00");

    if (texto !== "") {
        $('#lblDesripcion').html(texto);
    }

    $('#modalPrincipal').modal('show');
    $('#txtCierre').val(fecha);
    $('#txtReapertura').val(fecha);
    $('#txtFecha').val(fecha);

    if (cierre === "true") {
        $('#btnNuevo').prop('disabled', true);
        $('#btnReapertura').prop('disabled', false);
        $('#btnCierre').prop('disabled', true);
    }
    else {
        $('#btnNuevo').prop('disabled', false);
        $('#btnReapertura').prop('disabled', true);
        $('#btnCierre').prop('disabled', false);
    }

    var json = {
        FInicio: fecha,
        FTermino: fecha,
        term: ""
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-CajaNoContab-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {

            $('#txtSaldoInicial').val(r[0].MONTO_INICIAL);

            table = $('#tabCaja').DataTable({
                "bDestroy": true,
                "aaData": r,
                "bPaginate": false,
                "bFilter": false,
                scrollCollapse: true,
                scrollY: '250px',
                "bInfo": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                //"aaSorting": [[1, "asc"]],
                ordering: false,
                columns: [
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.descripcion
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.flag_i_s
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.Monto
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.Responsable
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.Comentario
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                            if (cierre === "true" || o.INICIAL === "1") {
                                ck += '<button class="btn btn-sm btn-default"><i class="fa-solid fa-edit"></i></button>';
                            }
                            else {
                                ck += '<button class="btn btn-sm btn-default" onclick="editarRegistro(\'' + o.CajaId + '\');"><i class="fa-solid fa-edit"></i></button>';
                            }
                            return ck;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                            if (cierre === "true" || o.INICIAL === "1") {
                                ck += '<button class="btn btn-sm btn-danger"><i class="fa-solid fa-trash"></i></button>';
                            }
                            else {
                                ck += '<button class="btn btn-sm btn-danger" onclick="eliminarRegistro(\'' + o.CajaId + '\',\'' + o.Fecha + '\');"><i class="fa-solid fa-trash"></i></button>';
                            }
                            return ck;
                        }
                    }
                ]
            });

            detalleREsumen(fecha);
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function detalleREsumen(dato) {

    //obtenerMotivos();
    //obteneraNEXOS();

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-ResumenCajaNoContab-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (x) {

            var r = x[0];

            $('#txtIngreso').val(r.INGRESO);
            $('#txtSalida').val(r.SALIDA);
            $('#txtSaldo').val(r.SALDO);
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function obtenerMotivos() {

    var dllfiltro = $('#dlli_s').val();

    var json = {
        term: dllfiltro
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-MotivosNoContab-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            //console.log(r);

            $('#dllMotivo').empty();
            $('#dllMotivo').append('<option value="0">SELECCIONE</option>');
            //console.log(r);
            $.each(r, function (i, item) {
                //console.log(item);
                $('#dllMotivo').append('<option value=' + item.MotivoId + '>'+ item.descripcion + '</option>');
            });
            $('#dllMotivo').val("0");
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function obteneraNEXOS() {

    var json = {
        term: ""
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-AnexosNoContab-List",
        contentType: "application/json; charset=utf-8",
        //data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            //console.log(r);

            $('#txtResponsable').empty();
            $('#txtResponsable').append('<option value="0">SELECCIONE</option>');
            //console.log(r);
            $.each(r, function (i, item) {
                //console.log(item);
                $('#txtResponsable').append('<option value=\'' + item.descripcion.trim() + '\'>' + item.descripcion + '</option>');
            });
            $('#txtResponsable').val("0");
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function Limpiar() {
    $('#txtCodigo').val("");
    $('#dllMotivo').val("0");
    $('#txtMonto').val("0");
    $('#txtResponsable').val("0");
    $('#txtComentario').val("");
}

$('#btnNuevo').click(function (e) {
    $('#lblDesripciondet').html("NUEVO REGISTRO");
    $('#modalPrincipalDet').modal('show');
    Limpiar();
    //obtenerMotivos();
    //obteneraNEXOS();
});

function editarRegistro(dato) {
    $('#lblDesripciondet').html("ACTUALIZAR REGISTRO");
    $('#modalPrincipalDet').modal('show');
    Limpiar();

    //obtenerMotivos();
    //obteneraNEXOS();

    var json = {
        //FInicio: "01/01/1900",
        //FTermino: "01/01/1900",
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-CajaNoContab-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (x) {

            var r = x[0];

            $('#txtCodigo').val(r.CajaId);            
            $('#dllMotivo').val(r.MotivoId);
            $('#txtMonto').val(r.Monto);
            $('#txtResponsable').val(r.Responsable);
            $('#txtComentario').val(r.Comentario);
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}




$('#btnGrabar').click(function (e) {

    SaveRegister();
    //var fecha = $('#FRegistro').val();
    //var sSal = $("#dllMotivo option:selected").text().substring(0,1);

    //if (sSal === "s" || sSal === "S") {
    //    var flag = ValidarRegistro(fecha);
    //    if (flag === true) {
    //        SaveRegister();
    //    }
    //    else {
    //        swal("Advertencia", "El total de salida supera al valor de ingresos.", "warning");
    //    }
    //} else {
    //    SaveRegister();
    //}
});

function ValidarRegistro(fecha) {

    let result = true;

    var xMonto = $('#txtMonto').val();

    var json = {
        term: fecha
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-ResumenCajaNoContab-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            //console.log(r);
            var x = r[0];
            //console.log(x);
            //console.log(x.SALDO);
            //console.log(Number(xMonto));

            //console.log((x.SALIDA + Number(xMonto)))
            //console.log(Number(x.SALIDA + Number(xMonto)))
            if (Number(x.SALIDA + Number(xMonto)) < x.INGRESO) {

                result = false;
            }
        },
        error: errores
    });

    return result;

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function SaveRegister() {

    var ddlTip = $('#dllMotivo').val();
    var cod = $('#txtCodigo').val();
    var fecha = $('#txtFecha').val();

    var registro = {}
    registro["CajaId"] = (cod == "" ? 0 : cod);
    registro["Fecha"] = fecha;
    registro["MotivoId"] = ddlTip;
    registro["Monto"] = $('#txtMonto').val();
    registro["Responsable"] = $('#txtResponsable').val();
    registro["Comentario"] = $('#txtComentario').val();
    registro["Flag_Cierre"] = false;
    registro["validate"] = $("#dllMotivo option:selected").text().substring(0, 1);

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-CajaNoContab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(registro),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerCarteraDet("", fecha);
                Limpiar();
                $('#modalPrincipalDet').modal('hide');
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function eliminarRegistro(dato, fecha) {

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/elimina-CajaNoContab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerCarteraDet("", fecha);
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function eliminarTodoRegistro(dato) {

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/eliminaTodo-CajaNoContab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerCartera();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}
$('#btnCierre').click(function (e) {

    var dato = $('#txtCierre').val();

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/cerrar-CajaNoContab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerCarteraDet("", dato);
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
});

function aperturCajaRegistro() {

    var fecha = $('#FRegistro').val();

    var json = {
        FInicio: fecha,
        FTermino: fecha
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/aperturar-CajaNoContab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerCartera();
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

$('#btnReapertura').click(function (e) {

    var dato = $('#txtReapertura').val();

    var json = {
        term: dato
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/reaperturar-CajaNoContab-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            swal(r.title, r.mensaje, r.icon);
            if (r.idReturn.length >= 1) {
                obtenerCarteraDet("", dato);
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
});