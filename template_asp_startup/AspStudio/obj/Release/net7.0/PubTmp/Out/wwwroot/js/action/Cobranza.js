﻿var id_vend = '00';
var id_mone = 'MN';
var id_fpga = '00';
var id_lpre = '01';
var id_mpga = '00';
var id_tdoc = '1246';
var id_almc = '01';
var ruc = '20601636086';
var id_tdv = 'PD';
var table;

var productos = [];

var subtotalMontoCobrar = 0.00;

const formatter = new Intl.NumberFormat('es-PE', {
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

function getDate() {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}


$(document).ready(function () {
    $('#txtFinicio').val(getDate());
    $('#txtFfinal').val(getDate());

    $("#txtTipCobranza").autocomplete({
        source: function (request, response) {

            var json = {
                value: 1,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Combobox",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                         //console.log(item);
                        return {
                            tipo: item.TIPO,
                            val: item.COD_COBRANZA,
                            label: item.DESCRIPCION,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtTipCobranza').val(i.item.label);
            $('#txtTipCobranzaVal').val(i.item.val);
            $('#txtTipCobranzaTip').val(i.item.tipo);
        }
    });

    $("#txtCliente").autocomplete({
        source: function (request, response) {

            var json = {
                value: 2,
                term: request.term
            };

            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Combobox",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                         //console.log(item);
                        return {
                            tipo: "",
                            val: item.CFCODCLI,
                            label: item.CFNOMBRE,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtCliente').val(i.item.label);
            $('#txtClienteVal').val(i.item.val);
            //$('#txtTipCobranzaTip').val(i.item.tipo);
        }
    });

    $("#txtAval").autocomplete({
        source: function (request, response) {

            var json = {
                value: 3,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Combobox",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.AVAL_CODIGO,
                            label: item.AVAL_DESCRIPCION,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtAval').val(i.item.label);
            $('#txtAvalVal').val(i.item.val);
        }
    });

    $("#txtDocumento").autocomplete({
        source: function (request, response) {

            var json = {
                value: 4,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Combobox",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.CDONRODOC,
                            label: item.CDONRODOC,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtDocumento').val(i.item.label);
        }
    });

    $("#txtPedido").autocomplete({
        source: function (request, response) {

            var json = {
                value: 5,
                term: request.term
            };
            //console.log(json);
            $.ajax({
                type: "POST",
                url: root + "api/app/obtiene-Combobox",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        return {
                            tipo: "",
                            val: item.CFNUMPED,
                            label: item.CFNUMPED,
                        }
                    }))
                },
                error: errores
            });

            function errores(msg) {
                alert('Error: ' + msg.responseText);
            }
        },
        minLength: 2,
        select: function (e, i) {
            //console.log(i);
            $('#txtPedido').val(i.item.label);
        }
    });

    obtenerCartera();
});

$('#btnBuscar').click(function (e) {
    // new location
    obtenerMontoExceso();
    obtenerCartera();
});

$('#btnUsarCobroExc').click(function (e) {
    var monto = $('#txtMontoExceso').val();

    $('#txtMontoCobrar').val(monto);

    $('#modalPrincipal').modal('hide');
});

function obtenerMontoExceso() {

    var txtaval = $('#txtAval').val();
    var txtcliente = $('#txtCliente').val();

    var cliente = $('#txtClienteVal').val();
    var aval = $('#txtAvalVal').val();

    var json = {
        Cliente: (txtcliente === "" ? "" : cliente),
        Aval: (txtaval === "" ? "" : aval)
    };

    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-Monto-cobranzaexceso",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (x) {

            if (x.idReturn === "1") {
                $('#lblDesripcion').html("INFORMACIÓN DE EXCEDENTE");
                $('#modalPrincipal').modal('show');
                $('#messageExceso').html(x.mensaje);
                $('#txtMontoExceso').val(x.title);
                $('#txtidcobranzaexceso').val(x.icon);
            }
            else {
                $('#messageExceso').html("");
                $('#txtMontoExceso').val("");
                $('#txtidcobranzaexceso').val("");
            }
        },
        error: errores
    });
    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}



function obtenerCartera() {

    var aval = $('#txtAval').val();
    var avalVal = $('#txtAvalVal').val();
    var cliente = $('#txtCliente').val();
    var clienteVal = $('#txtClienteVal').val();
    var pedido = $('#txtPedido').val();
    var documento = $('#txtDocumento').val();

    var json = {
        Cliente: (cliente !== "" ? clienteVal : ""),
        Aval: (aval !== "" ? avalVal : ""),
        FInicio: $('#txtFinicio').val(),
        FTermino: $('#txtFfinal').val(),
        Pedido: pedido,
        Documento: documento
    };

    //console.log(json);
    $.ajax({
        type: "POST",
        url: root + "api/app/obtiene-Cartera-List",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        dataType: "json",
        success: function (r) {
            //console.log(r);
            table = $('#tabCartera').DataTable({
                "bDestroy": true,
                "aaData": r,
                "bPaginate": false,
                "bFilter": false,
                scrollCollapse: true,
                scrollY: '180px',
                "bInfo": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                columns: [
                    //{ "data": "AVAL" },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.AVAL
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.FEC_PEDIDO
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.PEDIDO
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            return formatter.format(o.TOTAL_PEDIDO)
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.DOC
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.COMPROBANTE
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "bSortable": false,
                        "mRender": function (o) {
                            var icono = '';
                            icono += '' + o.FEC_DOC
                            return icono;
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            return formatter.format(o.TOTAL_DOC)
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            return formatter.format(o.SALDO_DOC)
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                            ck += '<div class="form-check-inline"><input class="form-check-input" type="checkbox" onclick="EjecutarCobranzaCK(\'txt_' + o.COMPROBANTE.trim() + '\',' + o.SALDO_DOC + ', this,\'' + o.DOC.trim() + '\',\'' + o.COMPROBANTE.trim() +'\')" value="" id="flexCheckDefault"></div>';
                            return ck;
                            //return '<button class="btn btn-sm btn-default"><i class="fa-solid fa-cart-shopping"></i></button>';
                        }
                    },
                    {
                        "mData": null,
                        "className": "dt-right",
                        "sWidth": "50px",
                        "mRender": function (o) {
                            var ck = '';
                            ck += '<input id="txt_' + o.COMPROBANTE.trim() +'" type="text" style="background-color:#EEE" value="0.00" class="form-control form-control-sm text-end" disabled/>';
                            return ck;
                            //return '<button class="btn btn-sm btn-default"><i class="fa-solid fa-cart-shopping"></i></button>';
                        }
                    }
                ]
            });
        },
        error: errores
    });

    function errores(msg) {
        alert('Error: ' + msg.responseText);
    }
}

function EjecutarCobranzaCK(nameInput, valueBalance, ck, tipdoc, nrodoc) {

    var ckAction = ck.checked;
    var valorCobrado = 0.00;

    //console.log(ck);
    //console.log(ckAction);
    var valorMontoCobrar = $('#txtMontoCobrar').val();
    var txtMonto = Number(valorMontoCobrar);
    var valorSaldo = valueBalance;
    var saldoCobrado = Number($('#txtMontoCobrarAux').val());
    var montoCobrado = Number($('#' + nameInput.trim()).val());
    
    var json = {
        MontoCobrar: txtMonto,
        MontoFila: valorSaldo,
        MontoRestante: saldoCobrado,
        MontoCobrado: montoCobrado,
        ckActivo: ckAction
    };

    //console.log(json);


    if (txtMonto > 0) {

        $.ajax({
            type: "POST",
            url: root + "api/app/obtiene-MontoCobrar",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            dataType: "json",
            success: function (r) {
                //console.log(r);
                if (r.statuscode === 1) {
                    $('#txtMontoCobrarAux').val(r.resultante);
                    if (r.resultado === 0) {
                        //valorCobrado = valorSaldo;
                        $('#' + nameInput.trim()).val(valorSaldo);

                        if (ckAction === true) {
                            addItem(tipdoc, nrodoc, valorSaldo);
                        }
                        //else {
                        //    removeItem(nrodoc);
                        //}
                    }
                    else {
                        //valorCobrado = r.resultado;
                        $('#' + nameInput.trim()).val(r.resultado);

                        if (ckAction === true) {
                            addItem(tipdoc, nrodoc, r.resultado);
                        }
                        //else {
                        //    removeItem(nrodoc);
                        //}
                    }

                    $('#' + nameInput.trim()).prop('disabled', false);
                }
                else if (r.statuscode === 0) {
                    $('#txtMontoCobrarAux').val(r.resultante);
                    $('#' + nameInput.trim()).val('0.00');
                    $('#' + nameInput.trim()).prop('disabled', true);
                }
                else {
                    $('#' + nameInput.trim()).prop('disabled', true);
                    $('#' + nameInput.trim()).val('0.00');
                }
            },
            error: errores
        });
        function errores(msg) {
            alert('Error: ' + msg.responseText);
        }
    }
    else {
        valorCobrado = valorSaldo;

        if (ckAction === true) {
            $('#' + nameInput.trim()).val(valorSaldo);
            addItem(tipdoc, nrodoc, nameInput);
        }
        else {
            $('#' + nameInput.trim()).val("0.00");
            //removeItem(nrodoc);
        }
    }  

    if (ckAction !== true) {
    //    addItem(tipdoc, nrodoc, nameInput);
    //}
    //else {
        removeItem(nrodoc);
    }
}

function addItem(tipdoc, nrodoc, importe) {

    //var ddl_Concepto = $('#txtTipCobranzaVal').val();
    //var ddl_ConcepDesc = $('#txtTipCobranza').val();

    //if (ddl_ConcepDesc === "") {
    //    swal("Advertencia", "Seleccione un tipo de cobranza.", "warning");
    //    return;
    //}

    var item = {};
    item["RUC"] = $('#txtClienteVal').val();
    item["AVAL"] = $('#txtAvalVal').val();
    item["MONTOCOBRADO"] = $('#txtMontoCobrar').val();
    item["Cobranza_ExcesoId"] = ($('#txtidcobranzaexceso').val() === "" ? 0 : $('#txtidcobranzaexceso').val());
    item["DEPTIPDOC"] = tipdoc;
    item["DEPNRODOC"] = nrodoc;
    //item["DEPTIPOPER"] = "";
    item["DEPCONCEP"] = "";
    //item["DEPFECCOB"] = "";
    item["DEPIMPORTE"] = importe;
    //item["DEPTIPMON"] = "";
    item["DEPTIPCAM"] = 0;
    //item["DEPFECCRE"] = "";
    //item["DEPUSUARI"] = "";
    item["DEPGLOSA"] = $('#txtGlosa').val();
    //item["DEPCOBRA"] = "";
    //item["DEPCODBAN"] = "";
    //item["DEPDESBAN"] = "";
    //item["DEPRFTIPDOC"] = "";
    item["DEPRFNUMDOC"] = $('#txtReferencia').val();
    item["DEPIMPORTEPERC"] = 0;
    item["F_CJABCO"] = false;
    item["DEPBCOGIR"] = "";
    item["DEPCTABANCH"] = "";
    item["DEPPERAUTO"] = "";
    item["DEPCOBRDIFCLIE"] = false;
    item["CTA_BANCARIA"] = "";
    //item["CODDETPLA"] = "";
    item["IDBCODET"] = 0;
    item["DEPNUMRECIBO"] = "";
    item["IDCAJADET"] = false;
    item["AUTODETRACCION"] = false;
    item["AUTODETRACCION_GENERADO"] = false;
    item["AUTODETRACCION_MANUAL"] = false;
    item["COD_AUDITORIA"] = "";
    //item["COD_CLIENTE1"] = "";
    //item["COD_CLIENTE2"] = "";
    item["FLG_DIETARIO"] = false;
    item["INTERES_DIETARIO"] = "";
    item["PORTES_DIETARIO"] = "";
    item["COMISION_DIETARIO"] = "";
    item["FEC_RES"] = "";
    item["DEMTOCRED"] = "";
    item["DENROCOMP"] = "";
    item["DEFECHCOMP"] = "";
    item["DEPFECCONTAB"] = "";
    item["DEPAJUSTE"] = "";
    item["DEPCOB_BANCO"] = "";
    

    var duplicado = false;

    duplicado = validateUnique(nrodoc)
    if (duplicado == false) {
        productos.push(item);
        //subtotal = 0.00;
        //igv = 0.00;
        //descto = 0.00;
        //total = 0.00;

        //for (let i in productos) {
        //    subtotal += Number(productos[i]["SUBTOTAL"]);
        //    igv += Number(productos[i]["IGV"]);
        //}
        //total += Number(subtotal + igv)
    }
    //construyePedido();
}

function validateUnique(valor) {
    return !!productos.find(i => i.DEPNRODOC === valor)
}

function removeItem(codigo) {
    const index = productos.findIndex((item) => item.DEPNRODOC === codigo);
    productos.splice(index, 1);

    //let indice = 0;
    //for (let i in productos) {
    //    indice++;
    //    productos[i]["DEPNRODOC"] = indice;
    //}

    //subtotal = 0.00;
    //igv = 0.00;
    //descto = 0.00;
    //total = 0.00;

    //for (let i in productos) {
    //    subtotal += Number(productos[i]["SUBTOTAL"]);
    //    igv += Number(productos[i]["IGV"]);
    //    total += Number(subtotal + igv)
    //}
    //construyePedido();
}

$('#btnGuardar').click(function (e) {
    // new location
    registrar();
});
$('#btnNuevo').click(function (e) {
    // new location
    nuevoPedido();
});

function registrar() {

    var ddl_Concepto = $('#txtTipCobranzaVal').val();
    var ddl_ConcepDesc = $('#txtTipCobranza').val();

    if (ddl_ConcepDesc === "") {
        swal("Advertencia", "Seleccione un tipo de cobranza.", "warning");
        return;
    }

    //console.log(detalle);
    if (productos.length == 0) {
        swal("Advertencia", "Para completar el registro de cobranza se debe elegir al menos un item!", "warning");
        return;
    }

    var totalDesc = 0;
    for (let i in productos) {
        productos[i]["DEPCONCEP"] = ddl_Concepto;
        totalDesc += Number(productos[i]["DEPIMPORTE"]);
    }

    var valorMontoCobrar = $('#txtMontoCobrar').val();
    var txtMonto = Number(valorMontoCobrar);

    if (txtMonto > 0) {
        if (totalDesc > txtMonto) {
            swal("Advertencia", "El monto de los items elegidos, supera el monto a cobrar. revisar!", "warning");
            return;
        }
    }

    $.ajax({
        type: "POST",
        url: root + "api/app/registra-cobranza-general",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(productos),
        dataType: "json",
        beforeSend: function () {
            $.blockUI({ message: '<div><img src="../img/loading.gif" />Registrando Cobranza</div>' });
        },
        error: function (code) {
            //console.log(data);
            if (code == 400) {
                alert('400 status code! user error');
            }
            if (code == 500) {
                alert('500 status code! server error');
            }
        }
        ,
    }).done(function (d) {

        swal(d.title, d.mensaje, d.icon);
        setTimeout($.unblockUI, 2000);
        if (d.idReturn.length > 1) {
            productos = [];
            $('#txtMontoCobrar').val("0.00")
            $('#txtTipCobranzaVal').val("");
            $('#txtTipCobranza').val("");
            $('#txtReferencia').val("");
            $('#txtGlosa').val("");
            obtenerCartera();
        }
    })
}
