using AspStudio.Models;

namespace AspStudio;

public interface ITokenService
{
	string BuildToken(string key, string issuer, UsuarioAcceso user);

	bool IsTokenValid(string key, string issuer, string token);
}
