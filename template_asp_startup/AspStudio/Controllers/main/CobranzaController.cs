﻿using AspStudio.Data;
using AspStudio.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;

namespace AspStudio.Controllers.main
{
    public class CobranzaController : Controller
    {
        private readonly IConfiguration _config;

        private readonly ITokenService _tokenService;

        private string generatedToken;

        public CobranzaController(IConfiguration config, ITokenService tokenService)
        {
            _config = config;
            _tokenService = tokenService;
        }

        public IActionResult Registro()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Cobranza";

            return View();
        }

        public IActionResult TipoCobranza()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Title"] = "Tipo Cobranza";
            return View();
        }

        public IActionResult ConsultaCobranza()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Title"] = "Tipo Cobranza";
            return View();
        }

        public IActionResult TipoConceptos()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Title"] = "Concepto de Pago";
            return View();
        }
    }
}
