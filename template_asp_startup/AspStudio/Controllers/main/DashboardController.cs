﻿using AspStudio.Data;
using AspStudio.Utils;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AspStudio.Controllers.main
{
    public class DashboardController : Controller
    {
        private readonly IConfiguration _config;

        private readonly ITokenService _tokenService;

        private string generatedToken;

        public DashboardController(IConfiguration config, ITokenService tokenService)
        {
            _config = config;
            _tokenService = tokenService;
        }

        public IActionResult Index()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Centro de Peritaje - Intranet";
            return View();
        }


        public IActionResult DashPerito()
        {
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "id_Perito");
            string claim4 = utilitarios.GetClaim(@string, "usuario");

            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            //DataTable tab = GetDataSP.get_Info_Perito(Convert.ToInt32(claim3));
            //if (tab.Rows.Count > 0)
            //{
            //    DataRow dr = tab.Rows[0];

            //    ViewData["i_IdPerito"] = dr["i_IdPerito"];
            //    ViewData["v_ApellidoPaterno"] = dr["v_ApellidoPaterno"];
            //    ViewData["v_ApellidoMaterno"] = dr["v_ApellidoPaterno"];
            //    ViewData["v_Nombres"] = dr["v_Nombres"];
            //    ViewData["v_NumeroDocumento"] = dr["v_NumeroDocumento"];
            //    ViewData["i_tDoc"] = dr["i_tDoc"];
            //    ViewData["v_cp"] = dr["v_cp"];
            //    ViewData["v_ne"] = dr["v_ne"];
            //    ViewData["v_situacion"] = dr["v_situacion"];
            //    ViewData["v_estado"] = dr["v_estado"];
            //    ViewData["tipomiembro"] = dr["tipomiembro"];
            //    ViewData["finscripcion"] = dr["finscripcion"];
            //    ViewData["v_cip"] = dr["v_cip"];
            //}

            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_Perito"] = claim3;
            base.ViewData["id_usuario"] = claim4;

            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Centro de Peritaje - Intranet";
            return View();
        }
    }
}
