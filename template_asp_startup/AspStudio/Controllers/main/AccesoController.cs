﻿using AspStudio.Data;
using AspStudio.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;

namespace AspStudio.Controllers.main
{
    public class AccesoController : Controller
    {
        private readonly IConfiguration _config;

        private readonly ITokenService _tokenService;

        private string generatedToken;

        public AccesoController(IConfiguration config, ITokenService tokenService)
        {
            _config = config;
            _tokenService = tokenService;
        }

        public IActionResult autenticar()
        {
            string text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            DateTime lastWriteTime = new FileInfo(Assembly.GetExecutingAssembly().Location).LastWriteTime;
            base.ViewData["version"] = text + " Fecha de publicación : " + $"{lastWriteTime:dd/MM/yyyy}";
            base.ViewData["errorMessage"] = "";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Logout()
        {
            base.HttpContext.Session.Clear();
            return RedirectToAction("autenticar", "Acceso");
        }

        [HttpPost]
        public async Task<IActionResult> autenticar(UsuarioAcceso _usuario)
        {
            UsuarioAcceso usuarioAcceso = GetDataSP.SP_getUserLogin(_usuario.Correo, _usuario.Clave);
            if (string.IsNullOrEmpty(_usuario.Correo) || string.IsNullOrEmpty(_usuario.Clave))
            {
                return RedirectToAction("Error");
            }
            bool isNotAdmin = false;
            if (usuarioAcceso != null)
            {
                isNotAdmin = false;
                generatedToken = _tokenService.BuildToken(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), usuarioAcceso);
            }
            usuarioAcceso = GetDataSP.SP_getUserLogin2(_usuario.Correo, _usuario.Clave);
            if (usuarioAcceso != null)
            {
                isNotAdmin = true;
                generatedToken = _tokenService.BuildToken(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), usuarioAcceso);

            }
            if (generatedToken != null)
            {
                base.HttpContext.Session.SetString("Token", generatedToken);
                //if (isNotAdmin)
                //{
                //    return RedirectToAction("DashPerito", "Dashboard");
                //}
                //else
                //{
                //    return RedirectToAction("ordenes", "Pedido");
                //}

                if (isNotAdmin)
                {
                    return RedirectToAction("Registar", "NoContable");
                }
                else
                {
                    return RedirectToAction("Registar", "NoContable");
                }
            }

            string text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            DateTime lastWriteTime = new FileInfo(Assembly.GetExecutingAssembly().Location).LastWriteTime;
            base.ViewData["version"] = text + " Fecha de publicación : " + $"{lastWriteTime:dd/MM/yyyy}";
            base.ViewData["errorMessage"] = "verifique los datos de usuario y contraseña";
            return View();
        }

        public async Task<IActionResult> Salir()
        {
            await base.HttpContext.SignOutAsync("Cookies");
            return RedirectToAction("Index");
        }
    }
}
