﻿using AspStudio.Utils;
using Microsoft.AspNetCore.Mvc;

namespace AspStudio.Controllers.main
{
    public class CatalogoController : Controller
    {
        private readonly IConfiguration _config;

        private readonly ITokenService _tokenService;

        private string generatedToken;

        public CatalogoController(IConfiguration config, ITokenService tokenService)
        {
            _config = config;
            _tokenService = tokenService;
        }
		public IActionResult Paises()
		{
			#region Autenticacion
			string @string = base.HttpContext.Session.GetString("Token");
			if (@string == null)
			{
				return RedirectToAction("autenticar", "Acceso");
			}
			string claim = utilitarios.GetClaim(@string, "nombre_usuario");
			string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
			string claim3 = utilitarios.GetClaim(@string, "usuario");
			string claim4 = utilitarios.GetClaim(@string, "correo");
			if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
			{
				return RedirectToAction("autenticar", "Acceso");
			}
			base.ViewData["usuario"] = claim;
			base.ViewData["rol_usuario"] = claim2;
			base.ViewData["id_usuario"] = claim3;
			base.ViewData["correo"] = claim4;
			#endregion
			base.ViewData["Title"] = "Catalogo de Paises";
			return View();
		}
		public IActionResult Puertos()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Title"] = "Catalogo de Paises";
            return View();
        }
        public IActionResult Monedas()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Title"] = "Catalogo de Paises";
            return View();
        }
        public IActionResult TDocumentos()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Title"] = "Catalogo de Paises";
            return View();
        }
        public IActionResult ATrabajo()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Title"] = "Catalogo de Paises";
            return View();
        }
        public IActionResult Clientes()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Title"] = "Catalogo de Paises";
            return View();
        }
        public IActionResult Proveedores()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Title"] = "Catalogo de Paises";
            return View();
        }
       
        
    }
}
