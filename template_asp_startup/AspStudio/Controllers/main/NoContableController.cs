﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace AspStudio.Controllers.main
{
    public class NoContableController : Controller
    {
        private readonly IConfiguration _config;

        private readonly ITokenService _tokenService;

        private string generatedToken;

        public NoContableController(IConfiguration config, ITokenService tokenService)
        {
            _config = config;
            _tokenService = tokenService;
        }

        public IActionResult Registar()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            string claim5 = utilitarios.GetClaim(@string, "flag_caja_no_contable");
            string claim6 = utilitarios.GetClaim(@string, "flag_caja_re_apertura");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            if(claim5 == "0")
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            base.ViewData["cajanocontab"] = claim5;
            base.ViewData["reapertura"] = claim6;
            ViewBag.reapertura = claim6;
            #endregion
            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Registro Caja";

            return View();
        }

        public IActionResult RegistarMaestro()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            string claim5 = utilitarios.GetClaim(@string, "flag_caja_no_contable");
            string claim6 = utilitarios.GetClaim(@string, "flag_caja_re_apertura");

            if (claim5 == "0")
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            base.ViewData["cajanocontab"] = claim5;
            base.ViewData["reapertura"] = claim6;
            ViewBag.reapertura = claim6;
            #endregion
            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Motivos";

            return View();
        }

        public IActionResult Reportes()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            string claim5 = utilitarios.GetClaim(@string, "flag_caja_no_contable");
            string claim6 = utilitarios.GetClaim(@string, "flag_caja_re_apertura");

            if (claim5 == "0")
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            base.ViewData["cajanocontab"] = claim5;
            base.ViewData["reapertura"] = claim6;
            ViewBag.reapertura = claim6;
            #endregion
            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Reportes";

            return View();
        }

        public IActionResult ReportesResumen()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            string claim5 = utilitarios.GetClaim(@string, "flag_caja_no_contable");
            string claim6 = utilitarios.GetClaim(@string, "flag_caja_re_apertura");

            if (claim5 == "0")
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            base.ViewData["cajanocontab"] = claim5;
            base.ViewData["reapertura"] = claim6;
            ViewBag.reapertura = claim6;
            #endregion
            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Reportes";

            return View();
        }
    }
}
