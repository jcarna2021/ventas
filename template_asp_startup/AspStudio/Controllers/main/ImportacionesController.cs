﻿using Microsoft.AspNetCore.Mvc;

namespace AspStudio.Controllers.main
{
    public class ImportacionesController : Controller
    {
        private readonly IConfiguration _config;

        private readonly ITokenService _tokenService;

        private string generatedToken;

        public ImportacionesController(IConfiguration config, ITokenService tokenService)
        {
            _config = config;
            _tokenService = tokenService;
        }

        public IActionResult Index()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            string claim5 = utilitarios.GetClaim(@string, "flag_reapertura_importacion");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            base.ViewData["reapertura"] = claim5;
            ViewBag.reapertura = claim5;
            #endregion
            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Registro Caja";

            return View();
        }

        public IActionResult LineaNaviera()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            string claim5 = utilitarios.GetClaim(@string, "flag_reapertura_importacion");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            if (claim5 == "0")
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            base.ViewData["reapertura"] = claim5;
            ViewBag.reapertura = claim5;
            #endregion
            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Linea Naviera";

            return View();
        }

        public IActionResult FormaPago()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            string claim5 = utilitarios.GetClaim(@string, "flag_reapertura_importacion");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            if (claim5 == "0")
            {
                return RedirectToAction("autenticar", "Acceso");
            }

            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            base.ViewData["reapertura"] = claim5;
            ViewBag.reapertura = claim5;
            #endregion
            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Forma de pago";

            return View();
        }
    }
}
