﻿using Microsoft.AspNetCore.Mvc;

namespace AspStudio.Controllers.main
{
    public class PedidoController : Controller
    {
        private readonly IConfiguration _config;

        private readonly ITokenService _tokenService;

        private string generatedToken;

        public PedidoController(IConfiguration config, ITokenService tokenService)
        {
            _config = config;
            _tokenService = tokenService;
        }

        public IActionResult ordenes()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Titulo"] = "PANEL PRINCIPAL";
            base.ViewData["Title"] = "Centro de Peritaje - Intranet";
            return View();
        }
        public IActionResult ConsultaDocumento()
        {
            #region Autenticacion
            string @string = base.HttpContext.Session.GetString("Token");
            if (@string == null)
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            string claim = utilitarios.GetClaim(@string, "nombre_usuario");
            string claim2 = utilitarios.GetClaim(@string, "rol_usuario");
            string claim3 = utilitarios.GetClaim(@string, "usuario");
            string claim4 = utilitarios.GetClaim(@string, "correo");
            if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), @string))
            {
                return RedirectToAction("autenticar", "Acceso");
            }
            base.ViewData["usuario"] = claim;
            base.ViewData["rol_usuario"] = claim2;
            base.ViewData["id_usuario"] = claim3;
            base.ViewData["correo"] = claim4;
            #endregion
            base.ViewData["Title"] = "Catalogo de Paises";
            return View();
        }
    }
}
