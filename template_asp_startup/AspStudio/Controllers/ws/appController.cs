﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using RestSharp;
using System.Data;
using AspStudio.Data;
using AspStudio.Models;
using DinkToPdf.Contracts;
using DinkToPdf;
using Humanizer;
using NuGet.Protocol;
using System.ComponentModel.DataAnnotations;

namespace AspStudio.Controllers.ws
{
    [Route("api/[controller]")]
    [ApiController]
    public class appController : ControllerBase
    {
        private IConverter _converter;

        public appController(IConverter converter)
        {
            _converter = converter;
        }

        [HttpGet("genera-pdf-pedido/{id}")]
        public IActionResult CreatePDF(string id)
        {
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "PDF Report",
                //Out = @"D:\PDFCreator\Employee_Report.pdf"
            };

            DataTable tab = GetDataSP.getPedidosListadoByNumPed(id);
            DataRow dr = tab.Rows[0];

            string template = System.IO.File.ReadAllText(Directory.GetCurrentDirectory() + "/Template/pedido.html");
            template = template.Replace("@Pedido", "N° " + dr["CFNUMPED"]);
            template = template.Replace("@cliente", dr["CFNOMBRE"].ToString());
            template = template.Replace("@documento", dr["CFCODCLI"].ToString());
            template = template.Replace("@formapago", dr["FORMAPAGO"].ToString());
            template = template.Replace("@fecha", string.Format("{0:dd/MM/yyyy}", dr["CFFECDOC"]));
            template = template.Replace("@direccion", dr["CFDIRECC"].ToString());
            template = template.Replace("@vendedor", dr["VENDEDOR"].ToString());
            template = template.Replace("@observacion", dr["CFGLOSA"].ToString());
            template = template.Replace("@moneda", dr["SIMBOLO"].ToString());
            template = template.Replace("@mpago", dr["METPAGO"].ToString());
            template = template.Replace("@importe", String.Format("{0:N}", Convert.ToDecimal(dr["CFIMPORTE"])));

            string tabladet = " <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n  <tr>\r\n    <td width=\"15%\"><strong>CÓDIGO</strong></td>\r\n    <td width=\"50%\"><strong>DESCRIPCION</strong></td>\r\n    <td width=\"10%\"><div align=\"center\"><strong>CANT.</strong></div></td>\r\n    <td width=\"10%\"><div align=\"center\"><strong>DSCTO.</strong></div></td>\r\n    <td width=\"10%\"><div align=\"center\"><strong>PRECIO</strong></div></td>\r\n    <td width=\"10%\"><div align=\"center\"><strong>TOTAL</strong></div></td>\r\n  </tr>\r\n  ";

            DataTable tabDet = GetDataSP.getPedidosListadoDetByNumPed(id, dr["CFCODMON"].ToString());
            foreach (DataRow item in tabDet.Rows)
            {
                tabladet += "<tr>\r\n    <td style='font-size:12px;'>" + item["DFCODIGO"].ToString() + "</td>\r\n    <td  style='font-size:12px;'>" + item["DFDESCRI"].ToString() + "</td>\r\n    <td style='font-size:12px;'>" + String.Format("{0:N}", Convert.ToDecimal(item["DFCANTID"])) + "</td>\r\n    <td style='font-size:12px;' align=\"rigth\">" + String.Format("{0:N}", Convert.ToDecimal(item["DFDESCTO"])) + "</td>\r\n    <td  style='font-size:12px;' align=\"rigth\">" + String.Format("{0:N}", Convert.ToDecimal(item["DFPREC_VEN"])) + "</td>\r\n    <td  style='font-size:12px;' align=\"rigth\">" + String.Format("{0:N}", Convert.ToDecimal(item["IMPORTE"])) + "</td>\r\n  </tr>";
            }


            tabladet += "</table>";
            template = template.Replace("@tabladetalle", tabladet);
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = template, //TemplateGenerator.GetHTMLString(),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "", Line = false },
                FooterSettings = { FontName = "Arial", FontSize = 9, Line = false, Center = "" }
            };
            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };
            var file = _converter.Convert(pdf);
            return File(file, "application/pdf");
        }

        [HttpPost("obtiene-cliente-term")]
        public dynamic obtiene_cliente_term(Filtro ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getCliente(ent.term);
            return Ok(dt.ToJson());
        }

        [HttpPost("registra-pedido-general")]
        public dynamic registra_pedido_general(PEDCAB ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.CFNOMBRE.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }
            if (ent.CFCODCLI.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un número de documento para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }
            if (ent.CFDIRECC.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar una dirección para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }
            dt = GetDataSP.Procesa_Pedido_Cabecera(ent);
            return Ok(dt);
        }


        [HttpPost("obtiene-producto-term")]
        public dynamic obtiene_producto_term(FiltroProducto ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getProductos(ent);
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-pedido-term")]
        public dynamic obtiene_pedido_term(FiltroProductoLista ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getPedidosListado(ent);
            return Ok(dt.ToJson());
        }

        [HttpGet("obtiene-serie-documento/{id}")]
        public dynamic getSerieDocumento(string id)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getSerieTipoDocVta(id);
            return Ok(dt.ToJson());
        }

        [HttpGet("obtiene-motivo-ncnd/{id}")]
        public dynamic getmotivoncnd(string id)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getMotivoNcNd(id);
            return Ok(dt.ToJson());
        }

        [HttpGet("obtiene-catalogo/{id}")]

        public dynamic getCatalogo(int id)
        {
            DataTable dt = new DataTable();
            if (id == 1)
            {
                dt = GetDataSP.getvendedor();
            }
            if (id == 2)
            {
                dt = GetDataSP.getMoneda();
            }
            if (id == 3)
            {
                dt = GetDataSP.getFormaPago();
            }
            if (id == 4)
            {
                dt = GetDataSP.getListaPrecios();
            }
            if (id == 5)
            {
                dt = GetDataSP.getListaMetodoPago();
            }
            if (id == 6)
            {
                dt = GetDataSP.getTipoDocumento();
            }
            if (id == 7)
            {
                dt = GetDataSP.getTC();
            }
            if (id == 8)
            {
                dt = GetDataSP.getAlmacen();
            }
            if (id == 9)
            {
                dt = GetDataSP.getEmpresa();
            }
            if (id == 10)
            {
                dt = GetDataSP.getTipoDocVta();
            }
            return Ok(dt.ToJson());
        }

        [HttpGet("obtiene-info-dni/{nro}")]
        public async Task<APidni> obtiene_info_dni(string nro)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string token = configuration.GetValue<string>("apiDoc:token");
            string ruta = configuration.GetValue<string>("apiDoc:rutaDNI");
            ruta = ruta.Replace("@dni", nro);
            ruta = ruta.Replace("@token", token);

            var client = new RestClient(ruta);
            var request = new RestRequest(ruta, Method.Get);
            APidni content = new APidni();
            RestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {

                content = JsonConvert.DeserializeObject<APidni>(response.Content);
                return content;
            }

            return content;
        }

        [HttpGet("obtiene-info-ruc/{nro}")]
        public async Task<APiruc> obtiene_info_ruc(string nro)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string token = configuration.GetValue<string>("apiDoc:token");
            string ruta = configuration.GetValue<string>("apiDoc:rutaRUC");
            ruta = ruta.Replace("@ruc", nro);
            ruta = ruta.Replace("@token", token);

            var client = new RestClient(ruta);
            var request = new RestRequest(ruta, Method.Get);
            APiruc content = new APiruc();
            RestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {

                content = JsonConvert.DeserializeObject<APiruc>(response.Content);
                return content;
            }

            return content;
        }

        [HttpPost("obtiene-documentos-venta")]
        public dynamic obtiene_documentos_venta(FiltroDocumento ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getDocumentoRef(ent);
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-documentos-venta-ById")]
        public dynamic obtiene_documentos_venta_ById(FiltroDocumento ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getDocumentoBYID(ent);
            return Ok(dt.ToJson());
        }
        [HttpPost("obtiene-documentos-venta-detalle-ById")]
        public dynamic obtiene_documentos_venta_detalle_ById(FiltroDocumento ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getDocumentoDetalleBYID(ent);
            return Ok(dt.ToJson());
        }

        #region Cobranza

        [HttpPost("obtiene-Cartera-List")]
        public dynamic obtiene_Cartera_List(FiltroCartera ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getDatosCartera(ent);
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-MontoCobrar")]
        public IActionResult obtiene_MontoCobrar(FiltroCobrando ent)
        {
            //var jsonResult = new {};

            try
            {
                decimal resultDiff = 0;

                if (ent.MontoCobrar > 0)
                {
                    if (ent.MontoFila > ent.MontoCobrar)
                    {
                        if (ent.ckActivo)
                        {
                            resultDiff = ent.MontoCobrar;
                            ent.MontoRestante = 0;
                        }
                        else
                        {
                            ent.MontoRestante = 0;
                        }
                    }
                    else
                    {
                        if (ent.MontoFila > ent.MontoRestante)
                        {
                            if (ent.ckActivo)
                            {
                                if (ent.MontoRestante == 0)
                                {
                                    //resultDiff = ent.MontoFila;
                                    ent.MontoRestante = ent.MontoCobrar - ent.MontoFila;
                                    //ent.MontoRestante = 0;
                                }
                                else
                                {
                                    //ent.MontoRestante -= ent.MontoFila;
                                    resultDiff = ent.MontoRestante;
                                    ent.MontoRestante = 0;

                                }
                                //resultDiff = ent.MontoRestante;
                                //ent.MontoRestante = 0;
                            }
                            else
                            {
                                ent.MontoRestante += ent.MontoCobrado;
                            }
                        }
                        else
                        {
                            if (ent.ckActivo)
                            {
                                if (ent.MontoRestante == 0)
                                {
                                    ent.MontoRestante = ent.MontoCobrar - ent.MontoFila;
                                }
                                else
                                {
                                    ent.MontoRestante -= ent.MontoFila;
                                }

                            }
                            else
                            {
                                ent.MontoRestante += ent.MontoFila;
                            }
                        }
                    }
                }

                var jsonResult = new
                {
                    statuscode = (ent.ckActivo == true ? 1 : 0),
                    resultante = ent.MontoRestante,
                    resultado = resultDiff
                };
                return Ok(jsonResult);
            }
            catch (Exception ex)
            {
                var jsonResult = new
                {
                    statuscode = -1,
                    resultante = 0.00
                };
                return Ok(jsonResult);
            }
        }

        [HttpPost("obtiene-Monto-cobranzaexceso")]
        public dynamic obtiene_Monto_CobranzaExceso(FiltroCobrExc ent)
        {
            ResultJson dt = new ResultJson();
            DataTable tab = new DataTable();
            tab = GetDataSP.getCobranzaExcesoCliente(ent);

            if(tab.Rows.Count == 0)
            {
                dt = new ResultJson
                {
                    title = "0.00",
                    mensaje = "No existen información de cobranza en exceso.",
                    icon = "",
                    idReturn = "0"
                };

                return Ok(dt);
            }

            DataRow row = tab.Rows[0];

            dt = new ResultJson
            {
                title = row["Monto_Final"].ToString(),
                mensaje = "El cliente y/o aval cuenta con un monto cobrado en exceso: por la siguiente cantidad: " + row["Monto_Final"].ToString() + ", desea utilizarlo¿?",
                icon = row["Cobranza_ExcesoId"].ToString(),
                idReturn = "1"
            };

            return Ok(dt);
        }

        [HttpPost("obtiene-Combobox")]
        public dynamic getComboBox(FiltroCBR ent)
        {
            DataTable dt = new DataTable();
            if (ent.value == 1)
            {
                dt = GetDataSP.getTipCobranzas(ent.term, "0");
            }
            else if (ent.value == 2)
            {
                dt = GetDataSP.getClienteFACCAB(ent.term);
            }
            else if (ent.value == 3)
            {
                dt = GetDataSP.getAvalCartera(ent.term);
            }
            else if (ent.value == 4)
            {
                dt = GetDataSP.getDocumentosCartera(ent.term);
            }
            else if (ent.value == 5)
            {
                dt = GetDataSP.getPedidosPEDCAB(ent.term);
            }

            return Ok(dt.ToJson());
        }

        [HttpPost("registra-cobranza-general")]
        public dynamic registra_cobranza_general(object ent)
        {
            ResultJson dt = new ResultJson();
            ResultJson dt2 = new ResultJson();
            try
            {
                var dato = ent.ToString();
                List<PLAN_COB_DET> sl = JsonConvert.DeserializeObject<List<PLAN_COB_DET>>(ent.ToString());

                if (sl.Count() == 0)
                {
                    dt = new ResultJson
                    {
                        title = "Error",
                        mensaje = "No existen items para registrar, validar.",
                        icon = "error",
                        idReturn = "0"
                    };

                    return Ok(dt);
                }

                int fila = 0;
                bool pase = false;
                string nro_Planilla = "";
                decimal montoCobrado = 0;
                foreach (PLAN_COB_DET item in sl)
                {
                    item.DEPNROPLA = (nro_Planilla == "" ? "" : nro_Planilla);
                    item.DEPFECCOB = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                    item.DEPRFTIPDOC = "BD";
                    item.INTERES_DIETARIO = 0;
                    item.PORTES_DIETARIO = 0;
                    item.COMISION_DIETARIO = 0;
                    item.DEMTOCRED = 0;

                    dt = GetDataSP.Procesa_Cobranza(item);

                    montoCobrado += (decimal)item.DEPIMPORTE;

                    if (dt.idReturn == "XXXXX")
                    {
                        pase = false;
                        nro_Planilla = "";
                        break;
                    }

                    pase = true;

                    nro_Planilla = dt.idReturn;
                    fila++;
                }

                if (pase)
                {
                    Cobranza_Exceso ent2 = new Cobranza_Exceso
                    {
                        Cobranza_ExcesoId = sl[0].Cobranza_ExcesoId,
                        Ruc = sl[0].RUC,
                        Aval = sl[0].AVAL,
                        Monto_Exceso = (sl[0].Cobranza_ExcesoId == 0 ? sl[0].MONTOCOBRADO - montoCobrado : 0),
                        Monto_utilizado = (sl[0].Cobranza_ExcesoId != 0 ? montoCobrado : 0),
                    };

                    dt2 = GetDataSP.Procesa_CobranzaExceso(ent2);
                }

                return Ok(dt);
            }
            catch (Exception ex)
            {
                dt = new ResultJson
                {
                    title = "Error",
                    mensaje = ex.Message,
                    icon = "error",
                    idReturn = "0"
                };
                return Ok(dt);
            }
        }


        #endregion

        #region Consulta cobranza

        [HttpPost("obtiene-Planillas-List")]
        public dynamic obtiene_Planillas_List(FiltroFechas ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getListPlanillas(ent);
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-PlanillasDet-List")]
        public dynamic obtiene_PlanillasDet_List(Filtro ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getListPlanillasdET(ent);
            return Ok(dt.ToJson());
        }

        [HttpPost("elimina-PlanDetalle-general")]
        public dynamic elimina_PlanDetalle_general(ParamDelete ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.DEPNROPLA.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            dt = GetDataSP.EliminarItemPlanilladET(ent);
            return Ok(dt);
        }

        #endregion

        #region Tipo de Cobranzas
        [HttpPost("obtiene-TipCobranza-List")]
        public dynamic obtiene_TipCobranza_List()
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getTipCobranzasAll();
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-TipCobranza-One")]
        public dynamic obtiene_TipCobranza_One(Filtro ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getTipCobranzas(ent.term, "1");
            return Ok(dt.ToJson());
        }


        [HttpPost("registra-tipcobranza-general")]
        public dynamic registra_tipcobranza_general(Tipo_Cobranza ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.DESCRIPCION.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }
            if (ent.COD_COBRANZA.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un número de documento para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            dt = GetDataSP.Procesa_TipoCobranza(ent);
            return Ok(dt);
        }

        [HttpPost("elimina-tipcobranza-general")]
        public dynamic elimina_tipcobranza_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.term.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            dt = GetDataSP.Eliminar_TipoCobranza(ent.term);
            return Ok(dt);
        }

        #endregion

        #region CONCEPTOS DE PAGO
        [HttpPost("obtiene-Conceptospago-List")]
        public dynamic obtiene_Conceptospago_List()
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getTipConceptosAll();
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-Conceptospago-One")]
        public dynamic obtiene_Conceptospago_One(Filtro ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.getTipConceptos(ent.term, "1");
            return Ok(dt.ToJson());
        }


        [HttpPost("registra-Conceptospago-general")]
        public dynamic registra_Conceptospago_general(ConceptosPago ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.DESCRIPCION.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }
            if (ent.COD_CONCEPTO.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un número de documento para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            dt = GetDataSP.Procesa_TipoConcepto(ent);
            return Ok(dt);
        }

        [HttpPost("elimina-Conceptospago-general")]
        public dynamic elimina_Conceptospago_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.term.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            dt = GetDataSP.Eliminar_TipoConcepto(ent.term);
            return Ok(dt);
        }

        #endregion

        #region No Contable

        [HttpPost("obtiene-MotivosNoContab-List")]
        public dynamic obtiene_MotivosNoContab_List(Filtro ent)
        {
            DataTable dt = new DataTable();
            Filtro ents = new Filtro();
            if (ent.term == "I" || ent.term == "S")
                ents.term = "";
            else 
                ents = ent;
            dt = GetDataSP.GetListMotivosNoContab(ents);

            if (ent.term == "I" || ent.term == "S")
            {
                if (dt.Rows.Count == 0)
                {
                    return Ok();
                }

                DataView view = new DataView(dt);
                view.RowFilter = "flag_i_s = '" + ent.term + "'";
                view.Sort = "descripcion";
                DataTable tabs = view.ToTable();
                return Ok(tabs.ToJson());
            }

            return Ok(dt.ToJson());
        }

        [HttpPost("registra-MotivosNoContab-general")]
        public dynamic registra_MotivosNoContab_general(MotivosNoContab ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.descripcion.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar una descripción para completar el registro.",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }


            dt = GetDataSP.Procesa_MotivoNoContab(ent);
            return Ok(dt);
        }

        [HttpPost("elimina-MotivosNoContab-general")]
        public dynamic elimina_MotivosNoContab_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.term.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            dt = GetDataSP.Eliminar_MotivosNoContab(ent.term);
            return Ok(dt);
        }

        /*CAJA NO CONTABLE*/

        [HttpPost("obtiene-CajaNoContabCab-List")]
        public dynamic obtiene_CajaNoContabCab_List(FiltroFechas ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.GetListCajaNoContab_Cab(ent);
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-CajaNoContab-List")]
        public dynamic obtiene_CajaNoContab_List(FiltroCaja ent)
        {
            DataTable dt = new DataTable();
            FiltroCaja ents = new FiltroCaja();
            if (ent.term != "")
            {
                ents.term = ent.term;
                ents.FInicio = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            }
            else
            {
                ents.FInicio = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(ent.FInicio));
                ents.FTermino = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(ent.FTermino));
                ents.term = "";
            }

            dt = GetDataSP.GetListCajaNoContab(ents);
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-ResumenCajaNoContab-List")]
        public dynamic obtiene_ResumenCajaNoContab_List(Filtro ent)
        {
            DataTable dt = new DataTable();
            DateTime fechanew = Convert.ToDateTime(ent.term);

            dt = GetDataSP.GetListResumenCajaNoContab(string.Format("{0:dd/MM/yyyy}", fechanew));
            return Ok(dt.ToJson());
        }

        [HttpPost("registra-CajaNoContab-general")]
        public dynamic registra_CajaNoContab_general(CajaNoContabAlt ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.Monto == 0)
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar una Monto para completar el registro.",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            CajaNoContab cajaNoContab = new CajaNoContab
            {
                CajaId = ent.CajaId,
                Fecha = Convert.ToDateTime(ent.Fecha),
                MotivoId = ent.MotivoId,
                Monto = ent.Monto,
                Responsable = ent.Responsable,
                Comentario = ent.Comentario,
                Flag_Cierre = ent.Flag_Cierre,
                validate = ent.validate
            };

            /* VALIDANDO INFORMACIÓN PARA REGISTRO*/

            if (ent.validate == "S")
            {
                DataTable dtS = new DataTable();
                DateTime fechanew = Convert.ToDateTime(cajaNoContab.Fecha);

                dtS = GetDataSP.GetListResumenCajaNoContab(string.Format("{0:dd/MM/yyyy}", fechanew));

                if (dtS.Rows.Count > 0)
                {
                    DataRow row = dtS.Rows[0];
                    if ((Convert.ToDecimal(row["SALIDA"].ToString()) + cajaNoContab.Monto) > (Convert.ToDecimal(row["INGRESO"].ToString())))
                    {
                        dt = new ResultJson
                        {
                            title = "Advertencia",
                            mensaje = "El total de salida supera al valor de ingresos.",
                            icon = "warning",
                            idReturn = "0"
                        };
                        return Ok(dt);
                    }
                }
            }

            //ent.Fecha = Convert.ToDateTime(string.Format("{0:dd/MM/yyyy}", DateTime.Now));
            dt = GetDataSP.Procesa_CajaNoContab(cajaNoContab);
            return Ok(dt);
        }

        [HttpPost("elimina-CajaNoContab-general")]
        public dynamic elimina_CajaNoContab_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.term.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            dt = GetDataSP.Eliminar_CajaNoContab(ent.term);
            return Ok(dt);
        }

        [HttpPost("eliminaTodo-CajaNoContab-general")]
        public dynamic eliminaTodo_CajaNoContab_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.term.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            dt = GetDataSP.Eliminar_TodosDatos_CajaNoContab(ent.term);
            return Ok(dt);
        }

        [HttpPost("obtiene-AnexosNoContab-List")]
        public dynamic obtiene_AnexosNoContab_List()
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.GetListAnexosNoContab("010BDCONTABILIDAD");
            return Ok(dt.ToJson());
        }

        [HttpPost("cerrar-CajaNoContab-general")]
        public dynamic cerrar_CajaNoContab_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.term.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            dt = GetDataSP.Cerrar_CajaNoContab(ent.term);
            return Ok(dt);
        }

        [HttpPost("aperturar-CajaNoContab-general")]
        public dynamic aperturar_CajaNoContab_general(FiltroFechas ent)
        {
            ResultJson dt = new ResultJson();
            DateTime fechanew = Convert.ToDateTime(ent.FInicio);

            dt = GetDataSP.Aperturar_CajaNoContab_Dia(string.Format("{0:dd/MM/yyyy}", fechanew));
            return Ok(dt);
        }

        [HttpPost("reaperturar-CajaNoContab-general")]
        public dynamic reaperturar_CajaNoContab_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            if (ent.term.Equals(""))
            {
                dt = new ResultJson
                {
                    title = "Información",
                    mensaje = "Se debe asignar un nombre o razón social para completar el registro del pedido",
                    icon = "info",
                    idReturn = "0"
                };
                return Ok(dt);
            }

            dt = GetDataSP.ReAperturar_CajaNoContab_Dia(ent.term);
            return Ok(dt);
        }

        [HttpGet("genera-pdf-CajaNoContab/{fecha}")]
        public IActionResult CreateCajaNoContabPDF(string fecha)
        {
            string datetime = "";
            string x = fecha.Substring(6, 2);
            string y = fecha.Substring(4, 2);
            string z = fecha.Substring(0, 4);
            if (fecha != "")
            {
                datetime = x + "/" + y + "/" + z;
            }

            DateTime fechas = Convert.ToDateTime(datetime);
            string fech = string.Format("{0:dd/MM/yyyy}", fechas);

            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "PDF Report",
                //Out = @"D:\PDFCreator\Employee_Report.pdf"
            };

            DataTable tab = GetDataSP.GetReporteCajaNoContab_Dia(fech);
            DataTable tabResumen = GetDataSP.GetListResumenCajaNoContab(fech);
            DataRow dr = tab.Rows[0];
            DataRow rs = tabResumen.Rows[0];

            string template = System.IO.File.ReadAllText(Directory.GetCurrentDirectory() + "/Template/CajaNoContable.html");
            template = template.Replace("@REPORTETITLE", dr["Fecha"].ToString());
            template = template.Replace("@SALDOINICIAL", dr["MONTO_INICIAL"].ToString());
            template = template.Replace("@INGRESO", String.Format("{0:N}", Convert.ToDecimal(rs["INGRESO"])));
            template = template.Replace("@SALIDA", String.Format("{0:N}", Convert.ToDecimal(rs["SALIDA"])));
            template = template.Replace("@SALDO", String.Format("{0:N}", Convert.ToDecimal(rs["SALDO"])));
            //template = template.Replace("@direccion", dr["CFDIRECC"].ToString());
            //template = template.Replace("@vendedor", dr["VENDEDOR"].ToString());
            //template = template.Replace("@observacion", dr["CFGLOSA"].ToString());
            //template = template.Replace("@moneda", dr["SIMBOLO"].ToString()
            //template = template.Replace("@mpago", dr["METPAGO"].ToString());
            //template = template.Replace("@importe", String.Format("{0:N}", Convert.ToDecimal(dr["CFIMPORTE"])));

            string tabladet = " <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n  " +
                "<tr>\r\n    " +
                "<td width=\"5%\"><strong>I/S</strong></td>\r\n    " +
                "<td width=\"40%\"><strong>MOTIVO</strong></td>\r\n    " +
                "<td width=\"40%\"><strong>RESPONSABLE</strong></td>\r\n    " +
                "<td width=\"5%\"><div align=\"center\"><strong>MONTO</strong></div></td>\r\n    " +
                "<td width=\"10%\"><div align=\"center\"><strong>OBSERVACION</strong></div></td>" +
                "</tr>\r\n  ";

            //DataTable tabDet = GetDataSP.getPedidosListadoDetByNumPed(id, dr["CFCODMON"].ToString());
            foreach (DataRow item in tab.Rows)
            {
                tabladet += "<tr>\r\n    " +
                    "<td width=\"5%\" style='font-size:12px;'>" + item["flag_i_s"].ToString() + "</td>\r\n    " +
                    "<td width=\"40%\" style='font-size:12px;' >" + item["descripcion"].ToString() + "</td>\r\n    " +
                    "<td width=\"40%\"style='font-size:12px;'>" + item["Responsable"].ToString() + "</td>\r\n   " +
                    "<td width=\"5%\" style='font-size:12px;' align=\"rigth\">" + String.Format("{0:N}", Convert.ToDecimal(item["Monto"])) + "</td>" +
                    "<td width=\"10%\" style='font-size:12px;' align=\"rigth\">" + item["Comentario"].ToString() + "</td>" +
                    "</tr>";
            }


            tabladet += "</table>";
            template = template.Replace("@tabladetalle", tabladet);
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = template, //TemplateGenerator.GetHTMLString(),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "", Line = false },
                FooterSettings = { FontName = "Arial", FontSize = 9, Line = false, Center = "" }
            };
            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };
            var file = _converter.Convert(pdf);
            return File(file, "application/pdf");
        }

        [HttpGet("genera-pdf-CajaNoContab-Range/{fecha1}/{fecha2}")]
        public IActionResult CreateCajaNoContabRangePDF(string fecha1, string fecha2)
        {
            string datetime = "";
            string datetime2 = "";
            string x = fecha1.Substring(6, 2);
            string y = fecha1.Substring(4, 2);
            string z = fecha1.Substring(0, 4);

            string xx = fecha2.Substring(6, 2);
            string yy = fecha2.Substring(4, 2);
            string zz = fecha2.Substring(0, 4);

            //if (fecha1 != "")
            //{
            //    datetime = x + "/" + y + "/" + z;
            //}

            //if (fecha2 != "")
            //{
            //    datetime2 = xx + "/" + yy + "/" + zz;
            //}

            DateTime fechas1 = Convert.ToDateTime(fecha1);
            DateTime fechas2 = Convert.ToDateTime(fecha2);
            string fech1 = string.Format("{0:dd/MM/yyyy}", fechas1);
            string fech2 = string.Format("{0:dd/MM/yyyy}", fechas2);

            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "PDF Report",
                //Out = @"D:\PDFCreator\Employee_Report.pdf"
            };

            DataTable tab = GetDataSP.GetReporteCajaNoContab_Rango(fech1, fech2);
            DataTable tabResumen = GetDataSP.GetReporteResumenCajaNoContab_Rango(fech1, fech2);
            DataRow dr = tab.Rows[0];
            DataRow rs = tabResumen.Rows[0];

            DataTable tabGroupBy = new DataTable();
            if (tab.Rows.Count == 0)
            {
                return Ok();
            }

            tabGroupBy = tab.AsEnumerable()
                            .GroupBy(r => new { Col1 = r["Fecha"], Col2 = r["Flag_Cierre"] })
                            .Select(g =>
                            {
                                var row = tab.NewRow();
                                row["Fecha"] = g.Key.Col1;
                                row["Flag_Cierre"] = g.Key.Col2;
                                return row;
                            })
                            .CopyToDataTable();

            string template = System.IO.File.ReadAllText(Directory.GetCurrentDirectory() + "/Template/RangoCajaNoContable.html");
            template = template.Replace("@REPORTETITLE", "REPORTE DETALLADO DEL "+ fech1 + " AL "+ fech2);
            //template = template.Replace("@SALDOINICIAL", dr["MONTO_INICIAL"].ToString());
            template = template.Replace("@INGRESO", String.Format("{0:N}", Convert.ToDecimal(rs["INGRESO"])));
            template = template.Replace("@SALIDA", String.Format("{0:N}", Convert.ToDecimal(rs["SALIDA"])));
            template = template.Replace("@SALDO", String.Format("{0:N}", Convert.ToDecimal(rs["SALDO"])));

            string tabladet = " <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                "<tr>" +
                "<td colspan=\"4\"><strong>CAJA</strong></td>" +
                "</tr>" +
                "<tr>" +
                "<td width=\"15%\"><strong>MOVIMIENTO</strong></td>" +
                "<td width=\"50%\"><strong>DETALLE DE LA CAJA</strong></td>" +
                "<td width=\"10%\"><div align=\"center\"><strong>MONTO</strong></div></td>" +
                "<td width=\"10%\"><div align=\"center\"><strong>ESTADO</strong></div></td>" +
                "</tr> ";
            foreach (DataRow row in tabGroupBy.Rows)
            {
                tabladet += "<tr>" +
                            "<td colspan=\"3\"><strong>"+ row["Fecha"].ToString() + "</strong></td>" +
                            "<td ><strong>" + row["Flag_Cierre"].ToString() + "</strong></td>" +
                           "</tr>";

                DataView view = new DataView(tab);
                view.RowFilter = "Fecha = '" + row["Fecha"].ToString() + "'";
                DataTable tabs = view.ToTable();

                if (tabs.Rows.Count == 0)
                {
                    tabladet += "<tr>" +
                            "<td rowspan=\"4\" width=\"10%\" style='font-size:12px;' align=\"rigth\">No existen registros.</td>" +
                            "</tr>";
                }
                else
                {
                    foreach (DataRow item in tabs.Rows)
                    {
                        tabladet += "<tr>\r\n    " +
                            "<td width=\"15%\" style='font-size:12px;'>" + item["flag_i_s"].ToString() + "</td>" +
                            "<td width=\"50%\" style='font-size:12px;'>" + item["descripcion"].ToString() + "</td>" +
                            "<td width=\"10%\" style='font-size:12px;' align=\"rigth\">" + String.Format("{0:N}", Convert.ToDecimal(item["Monto"])) + "</td>" +
                            "<td width=\"10%\" style='font-size:12px;' align=\"rigth\">&nbsp;</td>" +
                            "</tr>";
                    }
                }
            }

            tabladet += "</table>";
            template = template.Replace("@tabladetalle", tabladet);
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = template, //TemplateGenerator.GetHTMLString(),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "", Line = false },
                FooterSettings = { FontName = "Arial", FontSize = 9, Line = false, Center = "" }
            };
            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };
            var file = _converter.Convert(pdf);
            return File(file, "application/pdf");
        }

        [HttpGet("genera-pdf-CajaNoContab-Resumen/{fecha1}/{fecha2}")]
        public IActionResult CreateCajaNoContabResumenPDF(string fecha1, string fecha2)
        {
            string datetime = "";
            string datetime2 = "";
            string x = fecha1.Substring(6, 2);
            string y = fecha1.Substring(4, 2);
            string z = fecha1.Substring(0, 4);

            string xx = fecha2.Substring(6, 2);
            string yy = fecha2.Substring(4, 2);
            string zz = fecha2.Substring(0, 4);

            //if (fecha1 != "")
            //{
            //    datetime = x + "/" + y + "/" + z;
            //}

            //if (fecha2 != "")
            //{
            //    datetime2 = xx + "/" + yy + "/" + zz;
            //}

            DateTime fechas1 = Convert.ToDateTime(fecha1);
            DateTime fechas2 = Convert.ToDateTime(fecha2);
            string fech1 = string.Format("{0:dd/MM/yyyy}", fechas1);
            string fech2 = string.Format("{0:dd/MM/yyyy}", fechas2);

            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "PDF Report",
                //Out = @"D:\PDFCreator\Employee_Report.pdf"
            };

            DataTable tab = GetDataSP.GetReporteResumenCajaNoContab(fech1, fech2);
            //DataTable tabResumen = GetDataSP.GetReporteResumenCajaNoContab_Rango(fech1, fech2);
            DataRow dr = tab.Rows[0];
            //DataRow rs = tabResumen.Rows[0];

            string template = System.IO.File.ReadAllText(Directory.GetCurrentDirectory() + "/Template/ResumenCajaNoContable.html");
            template = template.Replace("@REPORTETITLE", "RESUMEN DE CAJA DEL " + fech1 + " AL " + fech2);
            //template = template.Replace("@SALDOINICIAL", dr["MONTO_INICIAL"].ToString());
            

            string tabladet = " <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" +
                "<tr>" +
                "<td width=\"50%\"><strong>CAJA</strong></td>" +
                "<td width=\"10%\"><div align=\"center\"><strong>SALDO INICIAL</strong></div></td>" +
                "<td width=\"10%\"><div align=\"center\"><strong>INGRESOS</strong></div></td>" +
                "<td width=\"10%\"><div align=\"center\"><strong>SALIDAS</strong></div></td>" +
                "<td width=\"10%\"><div align=\"center\"><strong>SALDO FINAL</strong></div></td>" +
                "<td width=\"10%\"><div align=\"center\"><strong>ESTADO</strong></div></td>" +
                "</tr> ";

            decimal INGRESO = 0;
            decimal SALIDA = 0;
            foreach (DataRow item in tab.Rows)
            {
                tabladet += "<tr>\r\n    " +
                    "<td width=\"50%\" style='font-size:12px;'>" + item["Fecha"].ToString() + "</td>" +
                    "<td width=\"10%\" style='font-size:12px;' align=\"rigth\">" + String.Format("{0:N}", Convert.ToDecimal(item["SALDO_INICIAL"])) + "</td>" +
                    "<td width=\"10%\" style='font-size:12px;' align=\"rigth\">" + String.Format("{0:N}", Convert.ToDecimal(item["INGRESO"])) + "</td>" +
                    "<td width=\"10%\" style='font-size:12px;' align=\"rigth\">" + String.Format("{0:N}", Convert.ToDecimal(item["SALIDA"])) + "</td>" +
                    "<td width=\"10%\" style='font-size:12px;' align=\"rigth\">" + String.Format("{0:N}", Convert.ToDecimal(item["SALDO"])) + "</td>" +
                    "<td width=\"10%\" style='font-size:12px;' align=\"rigth\">" + item["CIERRE"].ToString() +"</td>" +
                    "</tr>";

                INGRESO += Convert.ToDecimal(item["INGRESO"]);
                SALIDA += Convert.ToDecimal(item["SALIDA"]);
            }
            template = template.Replace("@INGRESO", String.Format("{0:N}", Convert.ToDecimal(INGRESO)));
            template = template.Replace("@SALIDA", String.Format("{0:N}", Convert.ToDecimal(SALIDA)));
            //template = template.Replace("@SALDO", String.Format("{0:N}", Convert.ToDecimal(rs["SALDO"])));

            tabladet += "</table>";
            template = template.Replace("@tabladetalle", tabladet);
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = template, //TemplateGenerator.GetHTMLString(),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "", Line = false },
                FooterSettings = { FontName = "Arial", FontSize = 9, Line = false, Center = "" }
            };
            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };
            var file = _converter.Convert(pdf);
            return File(file, "application/pdf");
        }
        #endregion

        #region Importaciones

        [HttpPost("obtiene-ImportCab-List")]
        public dynamic obtiene_ImportCab_List(FiltroImporCab ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.GetListImporCab(ent);
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-Autocompletar_Imp")]
        public dynamic GetAutocompleteImp(FiltroCBR ent)
        {
            DataTable dt = new DataTable();
            if (ent.value == 1)
            {
                dt = GetDataSP.GetListProveedores(ent.term, "0");
            }
            else if (ent.value == 2)
            {
                dt = GetDataSP.GetListIncoterm(ent.term, "0");
            }
            else if (ent.value == 3)
            {
                dt = GetDataSP.GetListAgenciasAduanas(ent.term, "0");
            }
            else if (ent.value == 4)
            {
                dt = GetDataSP.GetListLineaNaviera(ent.term, "0");
            }
            else if (ent.value == 5)
            {
                dt = GetDataSP.GetListProductos(ent.term, "0");
            }
            else if (ent.value == 6)
            {
                dt = GetDataSP.GetListFiltroProveedores(ent.term);
            }
            else if (ent.value == 7)
            {
                dt = GetDataSP.GetListFiltroProductos(ent.term);
            }
            else if (ent.value == 8)
            {
                dt = GetDataSP.GetListFiltroMarca(ent.term);
            }
            else if (ent.value == 9)
            {
                dt = GetDataSP.GetListFormaPagoDet(ent.term, "0");
            }
            else if (ent.value == 10)
            {
                dt = GetDataSP.GetListBancos(ent.term, "0");
            }
            else if (ent.value == 11)
            {
                dt = GetDataSP.GetListProveedoresFlete(ent.term, "0");
            }

            return Ok(dt.ToJson());
        }

        [HttpPost("registra-Imporcab-general")]
        public dynamic registra_Imporcab_general(Imporc_1 ent)
        {
            ResultJson dt = new ResultJson();
            //if (ent.PROVEEDOR == "")
            //{
            //    dt = new ResultJson
            //    {
            //        title = "ADVERTENCIA",
            //        mensaje = "Se debe asignar un proveedor para completar el registro.",
            //        icon = "warning",
            //        idReturn = "0"
            //    };
            //    return Ok(dt);
            //}

            //if (ent.CAGENCIA == "")
            //{
            //    dt = new ResultJson
            //    {
            //        title = "ADVERTENCIA",
            //        mensaje = "Se debe asignar una agencia de aduanas para completar el registro.",
            //        icon = "warning",
            //        idReturn = "0"
            //    };
            //    return Ok(dt);
            //}

            //if (ent.INCOTERM == "")
            //{
            //    dt = new ResultJson
            //    {
            //        title = "ADVERTENCIA",
            //        mensaje = "Se debe completar la columna INCOTERM para completar el registro.",
            //        icon = "warning",
            //        idReturn = "0"
            //    };
            //    return Ok(dt);
            //}

            //if (ent.CCNAVIERA == "")
            //{
            //    dt = new ResultJson
            //    {
            //        title = "ADVERTENCIA",
            //        mensaje = "Se debe asignar una linea naviera para completar el registro.",
            //        icon = "warning",
            //        idReturn = "0"
            //    };
            //    return Ok(dt);
            //}

            dt = GetDataSP.Procesa_ImporCab(ent);
            return Ok(dt);
        }

        [HttpPost("Actualiza-Imporcab-general")]
        public dynamic Actualiza_Imporcab_general(ImporcAgencia_1 ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_ImporCab_Agencia(ent);
            return Ok(dt);
        }

        [HttpPost("Actualiza-ImporcabProv-general")]
        public dynamic Actualiza_ImporcabProv_general(ImporcProv_1 ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_ImporCab_Proveedor(ent);
            return Ok(dt);
        }

        [HttpPost("Actualiza-ImporcabProvFlete-general")]
        public dynamic Actualiza_ImporcabProvFlete_general(ImporcProvFlete_1 ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_ImporCab_ProveedorFlete(ent);
            return Ok(dt);
        }

        [HttpPost("eliminar-Imporcab-general")]
        public dynamic eliminar_Imporcab_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_Delete_ImporCab(ent.term);
            return Ok(dt);
        }

        [HttpPost("obtiene-ImportDet-List")]
        public dynamic obtiene_ImportDet_List(Filtro ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.GetListImporDet(ent.term);
            return Ok(dt.ToJson());
        }

        [HttpPost("registra-Impordet-general")]
        public dynamic registra_Impordet_general(Impord_1 ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_ImporDet(ent);
            return Ok(dt);
        }

        [HttpPost("eliminar-Impordet-general")]
        public dynamic eliminar_Impordet_general(FiltroCobrExc ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_Delete_ImporDet(ent.Cliente, ent.Aval);
            return Ok(dt);
        }

        [HttpPost("obtiene-PagoProveedores-List")]
        public dynamic obtiene_PagoProveedores_List(Filtro ent)
        {
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            dt = GetDataSP.GetListPagoProveedores(ent.term);
            dt2 = GetDataSP.GetListPagoAgente(ent.term);
            dt3 = GetDataSP.GetListPagoFlete(ent.term);
            dt4 = GetDataSP.GetListPagoVarios(ent.term);

            var datos = new
            {
                prov = dt,
                agent = dt2,
                flete = dt3,
                varios = dt4
            };


            return Ok(datos.ToJson());
        }

        [HttpPost("registra-PagoProveedor-general")]
        public dynamic registra_PagoProveedor_general(PagoProveedor ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_PagoProveedor(ent);
            return Ok(dt);
        }

        [HttpPost("registra-PagoAgente-general")]
        public dynamic registra_PagoAgente_general(PagoAgente ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_PagoAgente(ent);
            return Ok(dt);
        }

        [HttpPost("registra-PagoFlete-general")]
        public dynamic registra_PagoFlete_general(PagoFlete ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_PagoFlete(ent);
            return Ok(dt);
        }

        [HttpPost("registra-PagoVarios-general")]
        public dynamic registra_PagoVarios_general(PagoVarios ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_PagoVarios(ent);
            return Ok(dt);
        }

        [HttpPost("registra-cierreimp-general")]
        public dynamic registra_cierreimp_general(Imporc_Cierre ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_CierreImp(ent);
            return Ok(dt);
        }

        [HttpPost("registra-FleteSeguro-general")]
        public dynamic registra_FleteSeguro_general(Imporc_FS ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_FleteSeguro(ent);
            return Ok(dt);
        }

        [HttpPost("eliminar-PagoProveedor-general")]
        public dynamic eliminar_PagoProveedor_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_Delete_PGOPROVEEDOR(ent.term);
            return Ok(dt);
        }

        [HttpPost("eliminar-PagoAgente-general")]
        public dynamic eliminar_PagoAgente_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_Delete_PGOAGENTE(ent.term);
            return Ok(dt);
        }

        [HttpPost("eliminar-PagoFlete-general")]
        public dynamic eliminar_PagoFlete_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_Delete_PGOFLETE(ent.term);
            return Ok(dt);
        }

        [HttpPost("eliminar-PagoVarios-general")]
        public dynamic eliminar_PagoVarios_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_Delete_PGOVARIOS(ent.term);
            return Ok(dt);
        }

        #endregion

        #region Linea Naviera

        [HttpPost("obtiene-LineaNaviera-List")]
        public dynamic obtiene_LineaNaviera_List()
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.GetListLNaviera();
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-LineaNaviera-Detalle")]
        public dynamic obtiene_LineaNaviera_Detalle(Filtro ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.GetListLineaNaviera(ent.term, "1");
            return Ok(dt.ToJson());
        }

        [HttpPost("registra-LineaNaviera-general")]
        public dynamic registra_LineaNaviera_general(LineaNaviera ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_LineaNaviera(ent);
            return Ok(dt);
        }

        [HttpPost("eliminar-LineaNaviera-general")]
        public dynamic eliminar_LineaNaviera_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_Delete_LineaNaviera(ent.term);
            return Ok(dt);
        }

        #endregion

        #region FORMA DE PAGO

        [HttpPost("obtiene-FormaPago-List")]
        public dynamic obtiene_FormaPago_List()
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.GetListFormaPago();
            return Ok(dt.ToJson());
        }

        [HttpPost("obtiene-FormaPago-Detalle")]
        public dynamic obtiene_FormaPago_Detalle(Filtro ent)
        {
            DataTable dt = new DataTable();
            dt = GetDataSP.GetListFormaPagoDet(ent.term, "1");
            return Ok(dt.ToJson());
        }

        [HttpPost("registra-FormaPago-general")]
        public dynamic registra_FormaPago_general(FormaPago ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_FormaPago(ent);
            return Ok(dt);
        }

        [HttpPost("eliminar-FormaPago-general")]
        public dynamic eliminar_FormaPago_general(Filtro ent)
        {
            ResultJson dt = new ResultJson();
            dt = GetDataSP.Procesa_Delete_FormaPago(ent.term);
            return Ok(dt);
        }

        #endregion
    }
}
