using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AspStudio.Models;
using Microsoft.IdentityModel.Tokens;

namespace AspStudio;

public class TokenService : ITokenService
{
    private const double EXPIRY_DURATION_MINUTES = 30.0;

    public string BuildToken(string key, string issuer, UsuarioAcceso user)
    {
        Claim[] array = new Claim[10]
        {
            new Claim("nombre_usuario", user.Nombre),
            new Claim("rol_usuario", user.RolUsuario),
            new Claim("soloPerito", user.soloPerito.ToString()),
            new Claim("id_Perito", user.Codigo.ToString()),
            new Claim("usuario", user.v_UsuarioNombre),
            new Claim("correo", user.Correo),
            new Claim("flag_caja_no_contable", user.flag_caja_no_contable),
            new Claim("flag_caja_re_apertura", user.flag_caja_re_apertura),
            new Claim("flag_reapertura_importacion", user.flag_reapertura_importacion),
            new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", Guid.NewGuid().ToString())
        };
        SymmetricSecurityKey val = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
        SigningCredentials val2 = new SigningCredentials((SecurityKey)(object)val, "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256");
        DateTime? dateTime = DateTime.Now.AddMinutes(30.0);
        SigningCredentials val3 = val2;
        JwtSecurityToken val4 = new JwtSecurityToken(issuer, issuer, (IEnumerable<Claim>)array, (DateTime?)null, dateTime, val3);
        return ((SecurityTokenHandler)new JwtSecurityTokenHandler()).WriteToken((SecurityToken)(object)val4);
    }

    public bool IsTokenValid(string key, string issuer, string token)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(key);
        SymmetricSecurityKey issuerSigningKey = new SymmetricSecurityKey(bytes);
        JwtSecurityTokenHandler val = new JwtSecurityTokenHandler();
        try
        {
            SecurityToken val2 = default(SecurityToken);
            ((SecurityTokenHandler)val).ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidIssuer = issuer,
                ValidAudience = issuer,
                IssuerSigningKey = (SecurityKey)(object)issuerSigningKey
            }, out val2);
        }
        catch
        {
            return false;
        }
        return true;
    }
}
