﻿using AspStudio.Models;
using AspStudio.Utils;
using Microsoft.CodeAnalysis.Elfie.Serialization;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Data.Common;

namespace AspStudio.Data
{
    public static class GetDataSP
    {
        public static UsuarioAcceso SP_getUserLogin(string v_usuario, string v_password)
        {

            var config = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json").Build();
            string passDefault = EncDec.Encrypt(v_password, "SergiusMaximus");
            string cn = config.GetConnectionString("SqlConnectionString");
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(cn))
            {
                using (SqlCommand cmd = new SqlCommand("BDWENCO..GETLOGIN", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@v_usuario", v_usuario);
                    cmd.Parameters.AddWithValue("@v_password", passDefault);
                    
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {

                        sda.Fill(dt);

                    }
#pragma warning disable CS8603 // Posible tipo de valor devuelto de referencia nulo
                    return dt.ToList<UsuarioAcceso>().FirstOrDefault();
#pragma warning restore CS8603 // Posible tipo de valor devuelto de referencia nulo
                }
            }
        }

        public static UsuarioAcceso SP_getUserLogin2(string v_usuario, string v_password)
        {

            var config = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json").Build();
            //string passDefault = EncDec.Encrypt(v_password, "SergiusMaximus");
            string cn = config.GetConnectionString("SqlConnectionString");
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(cn))
            {
                using (SqlCommand cmd = new SqlCommand("BDWENCO..GETLOGIN2", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@v_usuario", v_usuario);
                    cmd.Parameters.AddWithValue("@v_password", v_password);
                  
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {

                        sda.Fill(dt);

                    }
#pragma warning disable CS8603 // Posible tipo de valor devuelto de referencia nulo
                    return dt.ToList<UsuarioAcceso>().FirstOrDefault();
#pragma warning restore CS8603 // Posible tipo de valor devuelto de referencia nulo
                }
            }
        }

        public static string myConnect = string.Empty;

        public static DataTable getEmpresa()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETEMP", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    // val2.Parameters.AddWithValue("@tipo_legajo_id", (object)tipoDoc);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getTipoDocVta()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETTIPDOCVNT", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    // val2.Parameters.AddWithValue("@tipo_legajo_id", (object)tipoDoc);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getSerieTipoDocVta(string tipoDoc)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETSERTIPDOCVNT", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@TIPO_DOCU", (object)tipoDoc);


                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getMotivoNcNd(string tipoDoc)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETMOTNCND", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@COD_CATALOGO", (object)tipoDoc);


                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getvendedor()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETVENDEDOR", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    // val2.Parameters.AddWithValue("@tipo_legajo_id", (object)tipoDoc);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getMoneda()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETMONEDA", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    // val2.Parameters.AddWithValue("@tipo_legajo_id", (object)tipoDoc);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getFormaPago()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETFORMAPAGO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    // val2.Parameters.AddWithValue("@tipo_legajo_id", (object)tipoDoc);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getListaPrecios()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETLISTAPRECIOS", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    // val2.Parameters.AddWithValue("@tipo_legajo_id", (object)tipoDoc);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getListaMetodoPago()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETMETODOPAGO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    // val2.Parameters.AddWithValue("@tipo_legajo_id", (object)tipoDoc);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getTipoDocumento()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETTIPODOC", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    // val2.Parameters.AddWithValue("@tipo_legajo_id", (object)tipoDoc);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getAlmacen()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETALMACEN", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    // val2.Parameters.AddWithValue("@tipo_legajo_id", (object)tipoDoc);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getTC()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETTC", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    // val2.Parameters.AddWithValue("@tipo_legajo_id", (object)tipoDoc);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getCliente(string term)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETCLIENTE", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getProductos(FiltroProducto ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETPRODUCTO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@COD_LISPRE", (object)(ent.listap == null ? "" : ent.listap));
                    val2.Parameters.AddWithValue("@STALMA", (object)(ent.almacen == null ? "" : ent.almacen));
                    val2.Parameters.AddWithValue("@term", (object)(ent.term == null ? "" : ent.term));
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getPedidosListado(FiltroProductoLista ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETPEDIDOS", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@TIPDDOCVTA", (object)ent.TIPDOCVTA);
                    val2.Parameters.AddWithValue("@CLIENTE", (object)ent.CLIENTE);
                    val2.Parameters.AddWithValue("@NRODOCU", (object)ent.NRODOCU);
                    val2.Parameters.AddWithValue("@FECHAINI", (object)ent.FECHAINI);
                    val2.Parameters.AddWithValue("@FECHAFIN", (object)ent.FECHAFIN);
                    val2.Parameters.AddWithValue("@VENDEDOR", (object)ent.VENDEDOR);
                    val2.Parameters.AddWithValue("@FORMA", (object)ent.FORMA);
                    val2.Parameters.AddWithValue("@METODO", (object)ent.METODO);
                    val2.Parameters.AddWithValue("@MONEDA", (object)ent.MONEDA);

                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getPedidosListadoByNumPed(string numero)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETPEDIDOS_BYCFNUMPED", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@CFNUMPED", (object)numero);


                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getPedidosListadoDetByNumPed(string numero, string moneda)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_GETPEDIDOSDET_BYCFNUMPED", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@CFNUMPED", (object)numero);
                    val2.Parameters.AddWithValue("@CFCODMON", (object)moneda);

                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static ResultJson Procesa_Pedido_Cabecera(PEDCAB ent)
        {
            //IL_0032: Unknown result type (might be due to invalid IL or missing references)
            //IL_0038: Expected O, but got Unknown
            //IL_003e: Unknown result type (might be due to invalid IL or missing references)
            //IL_0045: Expected O, but got Unknown
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("SP_INSERTACABPEDIDO", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
						val2.Parameters.AddWithValue("@TIPDDOCVTA", (object)ent.TIPDOCVTA);
						val2.Parameters.AddWithValue("@SERDDOCVTA", (object)ent.SERDOCVTA);
						val2.Parameters.AddWithValue("@CFFECDOC", (object)ent.CFFECDOC);
                        val2.Parameters.AddWithValue("@CFFECVEN", (object)ent.CFFECVEN);
                        val2.Parameters.AddWithValue("@CFVENDE", (object)ent.CFVENDE);
                        val2.Parameters.AddWithValue("@CFPUNVEN", (object)ent.CFPUNVEN);
                        val2.Parameters.AddWithValue("@CFCODCLI", (object)ent.CFCODCLI);
                        val2.Parameters.AddWithValue("@CFNOMBRE", (object)ent.CFNOMBRE);
                        val2.Parameters.AddWithValue("@CFDIRECC", (object)ent.CFDIRECC);
                        val2.Parameters.AddWithValue("@CFRUC", (object)ent.CFRUC);
                        val2.Parameters.AddWithValue("@CFIMPORTE", (object)ent.CFIMPORTE);
                        val2.Parameters.AddWithValue("@CFPORDESCL", (object)ent.CFPORDESCL);
                        val2.Parameters.AddWithValue("@CFPORDESES", (object)ent.CFPORDESES);
                        val2.Parameters.AddWithValue("@CFFORVEN", (object)ent.CFFORVEN);
                        val2.Parameters.AddWithValue("@CFTIPCAM", (object)ent.CFTIPCAM);
                        val2.Parameters.AddWithValue("@CFCODMON", (object)ent.CFCODMON);
                        val2.Parameters.AddWithValue("@CFRFTD", (object)ent.CFRFTD);
                        val2.Parameters.AddWithValue("@CFRFNUMSER", (object)ent.CFRFNUMSER);
                        val2.Parameters.AddWithValue("@CFRFNUMDOC", (object)ent.CFRFNUMDOC);
                        val2.Parameters.AddWithValue("@CFESTADO", (object)ent.CFESTADO);
                        val2.Parameters.AddWithValue("@CFUSER", (object)ent.CFUSER);
                        val2.Parameters.AddWithValue("@CFGLOSA", (object)ent.CFGLOSA);
                        val2.Parameters.AddWithValue("@CFNUMGUI", (object)ent.CFNUMGUI);
                        val2.Parameters.AddWithValue("@CFNUMFAC", (object)ent.CFNUMFAC);
                        val2.Parameters.AddWithValue("@CFORDCOM", (object)ent.CFORDCOM);
                        val2.Parameters.AddWithValue("@CFGLOSA1", (object)ent.CFGLOSA1);
                        val2.Parameters.AddWithValue("@CFIGV", (object)ent.CFIGV);
                        val2.Parameters.AddWithValue("@CFDESCTO", (object)ent.CFDESCTO);
                        val2.Parameters.AddWithValue("@CFDESIMP", (object)ent.CFDESIMP);
                        val2.Parameters.AddWithValue("@CFTIPFAC", (object)ent.CFTIPFAC);
                        val2.Parameters.AddWithValue("@CFDESVAL", (object)ent.CFDESVAL);
                        val2.Parameters.AddWithValue("@CFCOTIZA", (object)ent.CFCOTIZA);
                        val2.Parameters.AddWithValue("@CFLINEA", (object)ent.CFLINEA);
                        val2.Parameters.AddWithValue("@CFORDENFAB", (object)ent.CFORDENFAB);
                        val2.Parameters.AddWithValue("@CFDIRECCA", (object)ent.CFDIRECCA);
                        val2.Parameters.AddWithValue("@CFESTADO_PED", (object)ent.CFESTADO_PED);
                        val2.Parameters.AddWithValue("@CFEXISTECOTIZA", (object)ent.CFEXISTECOTIZA);
                        val2.Parameters.AddWithValue("@COD_DIRECCION", (object)ent.COD_DIRECCION);
                        val2.Parameters.AddWithValue("@COD_AUDITORIA", (object)ent.COD_AUDITORIA);
                        val2.Parameters.AddWithValue("@RESPUESTA", (object)ent.RESPUESTA);
                        val2.Parameters.AddWithValue("@TIPO", (object)ent.TIPO);
                        val2.Parameters.AddWithValue("@ETIQUETA1", (object)ent.ETIQUETA1);
                        val2.Parameters.AddWithValue("@ETIQUETA2", (object)ent.ETIQUETA2);
                        val2.Parameters.AddWithValue("@ETIQUETA3", (object)ent.ETIQUETA3);
                        val2.Parameters.AddWithValue("@ETIQUETA4", (object)ent.ETIQUETA4);
                        val2.Parameters.AddWithValue("@ETIQUETA5", (object)ent.ETIQUETA5);
                        val2.Parameters.AddWithValue("@ETIQUETA6", (object)ent.ETIQUETA6);
                        val2.Parameters.AddWithValue("@ETIQUETA7", (object)ent.ETIQUETA7);
                        val2.Parameters.AddWithValue("@ETIQUETA8", (object)ent.ETIQUETA8);
                        val2.Parameters.AddWithValue("@CFMPAG", (object)ent.CFMPAG);
						val2.Parameters.AddWithValue("@CFALMA", (object)ent.CFALMA);
						val2.Parameters.AddWithValue("@CFSALDO", (object)ent.CFSALDO);
						val2.Parameters.AddWithValue("@CFNROPED", (object)ent.CFNROPED);
						val2.Parameters.AddWithValue("@MOTIVO_NC_ND", (object)ent.MOTIVO_NC_ND);
						val2.Parameters.AddWithValue("@CFRFFECDOC", (object)ent.CFRFFECDOC);
						string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');
                        int total = ent.DETALLE.Count;
                        int fila = 0;
                        foreach (PEDDET item in ent.DETALLE)
                        {

                            item.DFNUMPED = mensaje[3];
                            item.CFCODMON = ent.CFCODMON;
                            item.CFTIPCAM = ent.CFTIPCAM;
                            int resp = Procesa_Pedido_Detalle(item);
                            fila += resp;
                        }
                        //if (fila == total) {
                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static int Procesa_Pedido_Detalle(PEDDET ent)
        {
            ResultJson resultJson = new ResultJson();
            int idReturn = 0;
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("SP_INSERTADETPEDIDO", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;

						val2.Parameters.AddWithValue("@TIPDDOCVTA", (object)ent.TIPDOCVTA);
						val2.Parameters.AddWithValue("@SERDDOCVTA", (object)ent.SERDOCVTA);
						val2.Parameters.AddWithValue("@DFNUMPED", (object)ent.DFNUMPED);
                        val2.Parameters.AddWithValue("@DFSECUEN", (object)ent.DFSECUEN);
                        val2.Parameters.AddWithValue("@DFCODIGO", (object)ent.DFCODIGO);
                        val2.Parameters.AddWithValue("@DFDESCRI", (object)ent.DFDESCRI);
                        val2.Parameters.AddWithValue("@DFCANTID", (object)ent.DFCANTID);
                        val2.Parameters.AddWithValue("@DFPREC_VEN", (object)ent.DFPREC_VEN);
                        val2.Parameters.AddWithValue("@DFPREC_ORI", (object)ent.DFPREC_ORI);
                        val2.Parameters.AddWithValue("@DFDESCTO", (object)ent.DFDESCTO);
                        val2.Parameters.AddWithValue("@DFIGV", (object)ent.DFIGV);
                        val2.Parameters.AddWithValue("@DFDESCLI", (object)ent.DFDESCLI);
                        val2.Parameters.AddWithValue("@DFDESESP", (object)ent.DFDESESP);
                        val2.Parameters.AddWithValue("@DFIGVPOR", (object)ent.DFIGVPOR);
                        val2.Parameters.AddWithValue("@DFPORDES", (object)ent.DFPORDES);
                        val2.Parameters.AddWithValue("@DFIMPUS", (object)ent.DFIMPUS);
                        val2.Parameters.AddWithValue("@DFIMPMN", (object)ent.DFIMPMN);
                        val2.Parameters.AddWithValue("@DFESTADO", (object)ent.DFESTADO);
                        val2.Parameters.AddWithValue("@DFSERIE", (object)ent.DFSERIE);
                        val2.Parameters.AddWithValue("@DFALMA", (object)ent.DFALMA);
                        val2.Parameters.AddWithValue("@DFTEXTO", (object)ent.DFTEXTO);
                        val2.Parameters.AddWithValue("@DFCANREF", (object)ent.DFCANREF);
                        val2.Parameters.AddWithValue("@DFLOTE", (object)ent.DFLOTE);
                        val2.Parameters.AddWithValue("@DFSALDO", (object)ent.DFSALDO);
                        val2.Parameters.AddWithValue("@DFARTIGV", (object)ent.DFARTIGV);
                        val2.Parameters.AddWithValue("@DFCODLIS", (object)ent.DFCODLIS);
                        val2.Parameters.AddWithValue("@DFUNIDAD", (object)ent.DFUNIDAD);
                        val2.Parameters.AddWithValue("@DFPRECOM", (object)ent.DFPRECOM);
                        val2.Parameters.AddWithValue("@DFCOMPROMETIDO", (object)ent.DFCOMPROMETIDO);
                        val2.Parameters.AddWithValue("@DFCOMPRA", (object)ent.DFCOMPRA);
                        val2.Parameters.AddWithValue("@DFORIGEN", (object)ent.DFORIGEN);
                        val2.Parameters.AddWithValue("@REFERENCIA_GLOSA", (object)ent.REFERENCIA_GLOSA);
                        val2.Parameters.AddWithValue("@DFSALDOBULTOS", (object)ent.DFSALDOBULTOS);
                        val2.Parameters.AddWithValue("@PESO_BRUTO", (object)ent.PESO_BRUTO);
                        val2.Parameters.AddWithValue("@ETIQUETADET1", (object)ent.ETIQUETADET1);
                        val2.Parameters.AddWithValue("@CFCODMON", (object)ent.CFCODMON);
                        val2.Parameters.AddWithValue("@CFTIPCAM", (object)ent.CFTIPCAM);
                        idReturn = ((DbCommand)(object)val2).ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        idReturn = 0;
                    }
                    ((DbConnection)(object)val).Close();
                    return idReturn;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable getDocumentoRef(FiltroDocumento ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_BUSCARDOC", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@TIPDDOCVTA", (object)ent.TIPDDOCVTA);
                    val2.Parameters.AddWithValue("@SERDDOCVTA", (object)ent.SERDDOCVTA);
                    val2.Parameters.AddWithValue("@NUMDDOCVTA", (object)ent.NUMDDOCVTA);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable getDocumentoBYID(FiltroDocumento ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_OBTENERCABECERADOCUMENTO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@RUC", (object)ent.RUC);
                    val2.Parameters.AddWithValue("@TIPDDOCVTA", (object)ent.TIPDDOCVTA);
                    val2.Parameters.AddWithValue("@SERDDOCVTA", (object)ent.SERDDOCVTA);
                    val2.Parameters.AddWithValue("@NUMDDOCVTA", (object)ent.NUMDDOCVTA);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        public static DataTable getDocumentoDetalleBYID(FiltroDocumento ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("SP_OBTENERDETALLEDOCUMENTO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@RUC", (object)ent.RUC);
                    val2.Parameters.AddWithValue("@TIPDDOCVTA", (object)ent.TIPDDOCVTA);
                    val2.Parameters.AddWithValue("@SERDDOCVTA", (object)ent.SERDDOCVTA);
                    val2.Parameters.AddWithValue("@NUMDDOCVTA", (object)ent.NUMDDOCVTA);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        #region cobranzas

        public static DataTable getDatosCartera(FiltroCartera ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("Usp_RelacionCartera_Cobranzas", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@cliente", (object)ent.Cliente);
                    val2.Parameters.AddWithValue("@aval", (object)ent.Aval);
                    val2.Parameters.AddWithValue("@finicio", (object)string.Format("{0:yyyy-MM-dd}",ent.FInicio));
                    val2.Parameters.AddWithValue("@ftermino", (object)string.Format("{0:yyyy-MM-dd}", ent.FTermino));
                    val2.Parameters.AddWithValue("@pedido", (object)ent.Pedido);
                    val2.Parameters.AddWithValue("@documento", (object)ent.Documento);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable getTipCobranzas(string term, string filtro)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_LISTAR_TIPOSCOBRANZA", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@filtro", (object)filtro);
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable getCobranzaExcesoCliente(FiltroCobrExc ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GET_COBRANZAEXCESO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@CLIENTE", (object)ent.Cliente);
                    val2.Parameters.AddWithValue("@AVAL", (object)ent.Aval);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable getClienteFACCAB(string term)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_LISTAR_CLIENTES", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable getAvalCartera(string term)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_LISTAR_AVAL", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable getDocumentosCartera(string term)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_LISTAR_DOCUMENTOS", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable getPedidosPEDCAB(string term)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_LISTAR_PEDIDOS", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_Cobranza(PLAN_COB_DET ent)
        {
            //IL_0032: Unknown result type (might be due to invalid IL or missing references)
            //IL_0038: Expected O, but got Unknown
            //IL_003e: Unknown result type (might be due to invalid IL or missing references)
            //IL_0045: Expected O, but got Unknown
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_PLANCOBRANZA", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@DEPNROPLA", (object)(ent.DEPNROPLA == null ? "" : ent.DEPNROPLA));
                        val2.Parameters.AddWithValue("@DEPTIPDOC", (object)ent.DEPTIPDOC);
                        val2.Parameters.AddWithValue("@DEPNRODOC", (object)ent.DEPNRODOC);
                        val2.Parameters.AddWithValue("@DEPCONCEP", (object)ent.DEPCONCEP);
                        val2.Parameters.AddWithValue("@DEPFECCOB", (object)ent.DEPFECCOB);
                        val2.Parameters.AddWithValue("@DEPIMPORTE", (object)ent.DEPIMPORTE);
                        val2.Parameters.AddWithValue("@DEPGLOSA", (object)ent.DEPGLOSA);
                        val2.Parameters.AddWithValue("@DEPRFNUMDOC", (object)ent.DEPRFNUMDOC);
                        val2.Parameters.AddWithValue("@DEPIMPORTEPERC", (object)ent.DEPIMPORTEPERC);
                        val2.Parameters.AddWithValue("@F_CJABCO", (object)ent.F_CJABCO);
                        val2.Parameters.AddWithValue("@DEPBCOGIR", (object)ent.DEPBCOGIR);
                        val2.Parameters.AddWithValue("@DEPCTABANCH", (object)ent.DEPCTABANCH);
                        val2.Parameters.AddWithValue("@DEPPERAUTO", (object)ent.DEPPERAUTO);
                        val2.Parameters.AddWithValue("@DEPCOBRDIFCLIE", (object)ent.DEPCOBRDIFCLIE);
                        val2.Parameters.AddWithValue("@CTA_BANCARIA", (object)ent.CTA_BANCARIA);
                        val2.Parameters.AddWithValue("@IDBCODET", (object)ent.IDBCODET);
                        val2.Parameters.AddWithValue("@DEPNUMRECIBO", (object)ent.DEPNUMRECIBO);
                        val2.Parameters.AddWithValue("@IDCAJADET", (object)ent.IDCAJADET);
                        val2.Parameters.AddWithValue("@AUTODETRACCION", (object)ent.AUTODETRACCION);
                        val2.Parameters.AddWithValue("@AUTODETRACCION_GENERADO", (object)ent.AUTODETRACCION_GENERADO);
                        val2.Parameters.AddWithValue("@AUTODETRACCION_MANUAL", (object)ent.AUTODETRACCION_MANUAL);
                        val2.Parameters.AddWithValue("@COD_AUDITORIA", (object)ent.COD_AUDITORIA);
                        val2.Parameters.AddWithValue("@FLG_DIETARIO", (object)ent.FLG_DIETARIO);
                        val2.Parameters.AddWithValue("@INTERES_DIETARIO", (object)ent.INTERES_DIETARIO);
                        val2.Parameters.AddWithValue("@PORTES_DIETARIO", (object)ent.PORTES_DIETARIO);
                        val2.Parameters.AddWithValue("@COMISION_DIETARIO", (object)ent.COMISION_DIETARIO);
                        val2.Parameters.AddWithValue("@FEC_RES", (object)ent.FEC_RES);
                        //val2.Parameters.AddWithValue("@DEMTOCRED", (object)ent.DEMTOCRED);
                        //val2.Parameters.AddWithValue("@DENROCOMP", (object)ent.DENROCOMP);
                        //val2.Parameters.AddWithValue("@DEFECHCOMP", (object)ent.DEFECHCOMP);
                        //val2.Parameters.AddWithValue("@DEPFECCONTAB", (object)ent.DEPFECCONTAB);
                        //val2.Parameters.AddWithValue("@DEPAJUSTE", (object)ent.DEPAJUSTE);
                        //val2.Parameters.AddWithValue("@DEPCOB_BANCO", (object)ent.DEPCOB_BANCO);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');
                        //if (fila == total) {
                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_CobranzaExceso(Cobranza_Exceso ent)
        {
            //IL_0032: Unknown result type (might be due to invalid IL or missing references)
            //IL_0038: Expected O, but got Unknown
            //IL_003e: Unknown result type (might be due to invalid IL or missing references)
            //IL_0045: Expected O, but got Unknown
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_COBRANZA_EXCESO", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@Cobranza_ExcesoId", (object)ent.Cobranza_ExcesoId);
                        val2.Parameters.AddWithValue("@Ruc", (object)ent.Ruc);
                        val2.Parameters.AddWithValue("@Aval", (object)ent.Aval);
                        val2.Parameters.AddWithValue("@Monto_Exceso", (object)ent.Monto_Exceso);
                        val2.Parameters.AddWithValue("@Monto_utilizado", (object)ent.Monto_utilizado);

                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');
                        //if (fila == total) {
                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        #endregion

        #region Consultar cobranza
        public static DataTable getListPlanillas(FiltroFechas ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_PLANCOBCAB", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@finicio", (object)ent.FInicio);
                    val2.Parameters.AddWithValue("@ftermino", (object)ent.FTermino);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable getListPlanillasdET(Filtro ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_PLANCOBDET", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@term", (object)ent.term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson EliminarItemPlanilladET(ParamDelete ent)
        {
            ResultJson resultJson = new ResultJson();
            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_ELIMIAR_PLANCOBDET", val);
                try
                {
                    try
                    {
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@DEPNROPLA", (object)ent.DEPNROPLA);
                        val2.Parameters.AddWithValue("@DEPSECUENC", (object)ent.DEPSECUENC);
                        val2.Parameters.AddWithValue("@DEPNRODOC", (object)ent.DEPNRODOC);
                        val2.Parameters.AddWithValue("@DEPFECCOB", (object)ent.DEPFECCOB);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        #endregion

        #region Tipo de Cobranzas
        public static DataTable getTipCobranzasAll()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_TIPCOBRANZA", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    //val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_TipoCobranza(Tipo_Cobranza ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_GRABAR_TIPCOBRANZA", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@TIPO", (object)ent.TIPO);
                        val2.Parameters.AddWithValue("@COD_COBRANZA", (object)ent.COD_COBRANZA);
                        val2.Parameters.AddWithValue("@DESCRIPCION", (object)ent.DESCRIPCION);
                        val2.Parameters.AddWithValue("@MONEDA", (object)ent.MONEDA);
                        val2.Parameters.AddWithValue("@CUENTA", (object)ent.CUENTA);
                        val2.Parameters.AddWithValue("@ANEX_PROV", (object)ent.ANEX_PROV);
                        val2.Parameters.AddWithValue("@BANCOS", (object)ent.BANCOS);
                        val2.Parameters.AddWithValue("@USUARIO", (object)ent.USUARIO);
                        val2.Parameters.AddWithValue("@CHEQ_DIFER", (object)ent.CHEQ_DIFER);
                        val2.Parameters.AddWithValue("@APLIC_DOC", (object)ent.APLIC_DOC);
                        val2.Parameters.AddWithValue("@TIP", (object)ent.TIP);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Eliminar_TipoCobranza(string codigo)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_TIPCOBRANZA", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@COD_COBRANZA", (object)codigo);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        #endregion

        #region Conceptos de Pago
        public static DataTable getTipConceptosAll()
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_TIPCONCEPTOSPAGO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    //val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_TipoConcepto(ConceptosPago ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_GRABAR_TIPCONCEPTOSPAGO", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@TIPO", (object)ent.TIPO);
                        val2.Parameters.AddWithValue("@COD_CONCEPTO", (object)ent.COD_CONCEPTO);
                        val2.Parameters.AddWithValue("@DESCRIPCION", (object)ent.DESCRIPCION);
                        val2.Parameters.AddWithValue("@MONEDA", (object)ent.MONEDA);
                        val2.Parameters.AddWithValue("@CUENTA", (object)ent.CUENTA);
                        val2.Parameters.AddWithValue("@ANEX_PROV", (object)ent.ANEX_PROV);
                        val2.Parameters.AddWithValue("@BANCOS", (object)ent.BANCOS);
                        val2.Parameters.AddWithValue("@USUARIO", (object)ent.USUARIO);
                        val2.Parameters.AddWithValue("@CHEQ_DIFER", (object)ent.CHEQ_DIFER);
                        val2.Parameters.AddWithValue("@APLIC_DOC", (object)ent.APLIC_DOC);
                        val2.Parameters.AddWithValue("@TIP", (object)ent.TIP);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Eliminar_TipoConcepto(string codigo)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_TIPCONCEPTOSPAGO", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@COD_CONCEPTO", (object)codigo);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable getTipConceptos(string term, string filtro)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_LISTAR_TIPCONCEPTOSPAGO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@filtro", (object)filtro);
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        #endregion

        #region No Contable

        public static DataTable GetListMotivosNoContab(Filtro ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_MOTIVOSNOCONTABLES", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@term", (object)ent.term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_MotivoNoContab(MotivosNoContab ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_MOTIVOSNOCONTAB", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@MOTIVOID", (object)ent.MotivoId);
                        val2.Parameters.AddWithValue("@DESCRIPCION", (object)ent.descripcion);
                        val2.Parameters.AddWithValue("@FLAG", (object)ent.flag_i_s);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Eliminar_MotivosNoContab(string codigo)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_MOTIVOSNOCONTAB", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@term", (object)codigo);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        /*CAJA NO CONTABLE*/

        public static DataTable GetListCajaNoContab_Cab(FiltroFechas ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_CAJANOCONTABLE_CAB", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@finicio", (object)ent.FInicio);
                    val2.Parameters.AddWithValue("@ftermino", (object)ent.FTermino);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListCajaNoContab(FiltroCaja ent)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_CAJANOCONTAB", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@finicio", (object)ent.FInicio);
                    //val2.Parameters.AddWithValue("@ftermino", (object)ent.FTermino);
                    val2.Parameters.AddWithValue("@term", (object)ent.term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_CajaNoContab(CajaNoContab ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_CAJANOCONCATB", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@cajaId", (object)ent.CajaId);
                        val2.Parameters.AddWithValue("@fecha", (object)ent.Fecha);
                        val2.Parameters.AddWithValue("@motivoId", (object)ent.MotivoId);
                        val2.Parameters.AddWithValue("@monto", (object)ent.Monto);
                        val2.Parameters.AddWithValue("@responsable", (object)ent.Responsable);
                        val2.Parameters.AddWithValue("@comentario", (object)ent.Comentario);
                        val2.Parameters.AddWithValue("@flag", (object)ent.Flag_Cierre);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Eliminar_CajaNoContab(string codigo)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_CAJANOCONTAB", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@term", (object)codigo);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Eliminar_TodosDatos_CajaNoContab(string codigo)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_TODODETALLE_CAJANOCONTAB", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@term", (object)codigo);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListAnexosNoContab(string baseDatos)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_ANEXOS", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@BASE", (object)baseDatos);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListResumenCajaNoContab(string fecha)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_RESUMEN_CAJANOCONTAB_DIA", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@FECHA", (object)fecha);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Cerrar_CajaNoContab(string fecha)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_CIERRE_CAJANOCONTAB_DIA", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@FECHA", (object)fecha);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Aperturar_CajaNoContab_Dia(string fecha)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_APERTURAR_CAJANOCONTAB_DIA", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@FECHA", (object)fecha);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson ReAperturar_CajaNoContab_Dia(string fecha)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_RE_APERTURAR_CAJANOCONTAB_DIA", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@FECHA", (object)fecha);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }


        /*REPORTES CAJA NO CONTABLE*/

        public static DataTable GetReporteCajaNoContab_Dia(string fecha)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_REPORTE_CAJANOCONTAB_DIA", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@finicio", (object)fecha);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetReporteCajaNoContab_Rango(string fecha1, string fecha2)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_REPORTE_CAJANOCONTAB_RANGO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@finicio", (object)fecha1);
                    val2.Parameters.AddWithValue("@ftermino", (object)fecha2);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetReporteResumenCajaNoContab_Rango(string fecha1, string fecha2)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_RESUMEN_CAJANOCONTAB_RANGO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@finicio", (object)fecha1);
                    val2.Parameters.AddWithValue("@ftermino", (object)fecha2);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetReporteResumenCajaNoContab(string fecha1, string fecha2)
        {

            //IL_0076: Expected O, but got Unknown
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_REPORTE_RESUMEN_CAJANOCONTABLE_CAB", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@finicio", (object)fecha1);
                    val2.Parameters.AddWithValue("@ftermino", (object)fecha2);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        #endregion

        #region Importaciones

        // FILTROS
        public static DataTable GetListFiltroProveedores(string term)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_FILTRO_PROVEEDOR", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListFiltroProductos(string term)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_FILTRO_PRODUCTO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListFiltroMarca(string term)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_FILTRO_MARCAS", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        // FIN FILTROS
        public static DataTable GetListImporCab(FiltroImporCab ent)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_IMPORTACIONES_CAB", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@finicio", (object)ent.FInicio);
                    val2.Parameters.AddWithValue("@ftermino", (object)ent.FTermino);
                    val2.Parameters.AddWithValue("@proveedor", (object)ent.Proveedor);
                    val2.Parameters.AddWithValue("@marca", (object)ent.Marca);
                    val2.Parameters.AddWithValue("@producto", (object)ent.Producto);
                    val2.Parameters.AddWithValue("@eta", (object)ent.Eta);
                    val2.Parameters.AddWithValue("@N_BL", (object)ent.nro_bl);
                    val2.Parameters.AddWithValue("@N_CONTENEDOR", (object)ent.nro_contenedor);
                    val2.Parameters.AddWithValue("@N_ORDEN_CHEMO", (object)ent.nro_orden_chemo);
                    val2.Parameters.AddWithValue("@N_PROF_PROVEEDOR", (object)ent.nro_prof_proveedor);
                    val2.Parameters.AddWithValue("@N_DAM", (object)ent.nro_dam);
                    val2.Parameters.AddWithValue("@N_PZI", (object)ent.nro_pzi);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListProveedores(string term, string filtro)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_PROVEEDORES", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@filtro", (object)filtro);
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListIncoterm(string term, string filtro)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_INCOTERM", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@filtro", (object)filtro);
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListAgenciasAduanas(string term, string filtro)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_AGENCIAADUANAS", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@filtro", (object)filtro);
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListLineaNaviera(string term, string filtro)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_LINEANAVIERA", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@filtro", (object)filtro);
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListProveedoresFlete(string term, string filtro)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_PROVEEDORFLETE", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@filtro", (object)filtro);
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_ImporCab(Imporc_1 ent)
        {   
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_IMPORC_WEB", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@CAGENCIA", (object)ent.CAGENCIA);
                        val2.Parameters.AddWithValue("@CVESSEL", (object)ent.CVESSEL);
                        val2.Parameters.AddWithValue("@CCNAVIERA", (object)ent.CCNAVIERA);
                        val2.Parameters.AddWithValue("@CNUMEROBL", (object)ent.CNUMEROBL);
                        val2.Parameters.AddWithValue("@CNROCONTENEDOR", (object)ent.CNROCONTENEDOR);
                        val2.Parameters.AddWithValue("@FEMISION", (object)ent.FEMISION);
                        val2.Parameters.AddWithValue("@CETD", (object)ent.CETD);
                        val2.Parameters.AddWithValue("@CETA", (object)ent.CETA);
                        val2.Parameters.AddWithValue("@CPAGODERECHOS", (object)ent.CPAGODERECHOS);
                        val2.Parameters.AddWithValue("@CLLEGAALMACEN", (object)ent.CLLEGAALMACEN);
                        val2.Parameters.AddWithValue("@CLLEGALIQUIDACION", (object)ent.CLLEGALIQUIDACION);
                        val2.Parameters.AddWithValue("@INCOTERM", (object)ent.INCOTERM);
                        //val2.Parameters.AddWithValue("@CMARCA", (object)ent.CMARCA);
                        val2.Parameters.AddWithValue("@PROVEEDOR", (object)ent.PROVEEDOR);
                        val2.Parameters.AddWithValue("@PROVEEDOR_DESC", (object)ent.PROVEEDOR_DESC);
                        val2.Parameters.AddWithValue("@CPAGOPROVEEDOR", (object)ent.CPAGOPROVEEDOR);
                        val2.Parameters.AddWithValue("@DOCUMENTOS", (object)ent.DOCUMENTOS);
                        val2.Parameters.AddWithValue("@ORDEN_DE_COMPRA", (object)ent.ORDEN_DE_COMPRA);
                        val2.Parameters.AddWithValue("@CNROFACTURA", (object)ent.CNROFACTURA);
                        val2.Parameters.AddWithValue("@CNRODAM", (object)ent.CNRODAM);
                        val2.Parameters.AddWithValue("@CDOCSIWTFSUYON", (object)ent.CDOCSIWTFSUYON);
                        val2.Parameters.AddWithValue("@CEXTRAINFO", (object)ent.CEXTRAINFO);
                        val2.Parameters.AddWithValue("@CPZI", (object)ent.CPZI);
                        val2.Parameters.AddWithValue("@COC_CHEMO", (object)ent.COC_CHEMO);
                        val2.Parameters.AddWithValue("@CFECHA_EFACTURA", (object)ent.CFECHA_EFACTURA);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_ImporCab_Agencia(ImporcAgencia_1 ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ACTUALIZAR_DATOS_AGENCIA", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@CAGENCIA", (object)ent.CAGENCIA);
                        val2.Parameters.AddWithValue("@CLLEGALIQUIDACION", (object)ent.CLLEGALIQUIDACION);
                        val2.Parameters.AddWithValue("@CDOCSIWTFSUYON", (object)ent.CDOCSIWTFSUYON);
                        val2.Parameters.AddWithValue("@CEXTRAINFO", (object)ent.CEXTRAINFO);
                        val2.Parameters.AddWithValue("@CNRO_LIQUIDACION", (object)ent.CNRO_LIQUIDACION);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_ImporCab_Proveedor(ImporcProv_1 ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ACTUALIZAR_DATOS_PROVEEDOR", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@CPROVEEDOR", (object)ent.CCODPROVE);
                        val2.Parameters.AddWithValue("@CPROVEEDOR_DESCRI", (object)ent.CDESPROVE);
                        val2.Parameters.AddWithValue("@CEXTRAINFO", (object)ent.CEXTRAINFO);
                        val2.Parameters.AddWithValue("@CFECHAEMISIONFACT", (object)ent.CFECHA_EFACTURA);
                        val2.Parameters.AddWithValue("@CNRO_FACTURA", (object)ent.CNROFACTURA);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_CierreImp(Imporc_Cierre ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_CIERRE_IMPORTACIONES_USUARIO", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@FLAG", (object)ent.CFLAG);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_ImporCab_ProveedorFlete(ImporcProvFlete_1 ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ACTUALIZAR_DATOS_PROVEEDORFLETE", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@CPROVEEDOR", (object)ent.CCODPROVE_FLETE);
                        val2.Parameters.AddWithValue("@CPROVEEDOR_DESCRI", (object)ent.CDESPROVE_FLETE);
                        val2.Parameters.AddWithValue("@CFECHAEMISIONFACT", (object)ent.CFECHA_EFACTURA_FLETE);
                        val2.Parameters.AddWithValue("@CNRO_FACTURA", (object)ent.CNROFACTURA_FLETE);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_Delete_ImporCab(string cnumero)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_REGISTROS_IMPOR_WEB", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@CNUMERO", (object)cnumero);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListImporDet(string cnumero)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_IMPORD_WEB", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@CNUMERO", (object)cnumero);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListProductos(string term, string filtro)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_PRODUCTOS", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@filtro", (object)filtro);
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_ImporDet(Impord_1 ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_IMPORD_WEB", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@CITEM", (object)ent.CITEM);
                        val2.Parameters.AddWithValue("@CCODARTIC", (object)ent.CCODARTIC);
                        val2.Parameters.AddWithValue("@CDESARTIC", (object)ent.CDESARTIC);
                        val2.Parameters.AddWithValue("@NCANTIDAD", (object)ent.NCANTIDAD);
                        val2.Parameters.AddWithValue("@NPREUNITA", (object)ent.NPREUNITA);
                        val2.Parameters.AddWithValue("@DBULTOS", (object)ent.DBULTOS);
                        val2.Parameters.AddWithValue("@CCOMENT1", (object)ent.CCOMENT1);
                        val2.Parameters.AddWithValue("@CMAR_CODIGO", (object)ent.CMAR_CODIGO);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_Delete_ImporDet(string cnumero, string citem)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_REGISTROS_IMPORD_WEB", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@CNUMERO", (object)cnumero);
                        val2.Parameters.AddWithValue("@CITEM", (object)citem);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListPagoProveedores(string cnumero)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_PAGOPROVVEDORES", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@CNUMERO", (object)cnumero);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListPagoAgente(string cnumero)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_PAGOAGENTES", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@CNUMERO", (object)cnumero);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListPagoFlete(string cnumero)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_PAGOFLETE", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@CNUMERO", (object)cnumero);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListPagoVarios(string cnumero)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_PAGOVARIOS", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@CNUMERO", (object)cnumero);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_PagoProveedor(PagoProveedor ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_PAGOPROVEEDOR", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@PAGOPROVEEDOR_ID", (object)ent.PAGOPROVEEDOR_ID);
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@CCODPROVE", (object)ent.CCODPROVE);
                        val2.Parameters.AddWithValue("@FORMA_PAGO", (object)ent.FORMA_PAGO);
                        val2.Parameters.AddWithValue("@IMPORTE", (object)ent.IMPORTE);
                        val2.Parameters.AddWithValue("@FECHA_PAGO", (object)ent.FECHA_PAGO);
                        val2.Parameters.AddWithValue("@BANCO", (object)ent.BANCO);
                        val2.Parameters.AddWithValue("@NUMERO_OPERACION", (object)ent.NUMERO_OPERACION);
                        val2.Parameters.AddWithValue("@OBSERVACION", (object)ent.OBSERVACION);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_PagoAgente(PagoAgente ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_PAGOAGENTE", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@PAGOAGENTE_ID", (object)ent.PAGOAGENTE_ID);
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@CCODPROVE", (object)ent.CCODPROVE);
                        val2.Parameters.AddWithValue("@FORMA_PAGO", (object)ent.FORMA_PAGO);
                        val2.Parameters.AddWithValue("@IMPORTE", (object)ent.IMPORTE);
                        val2.Parameters.AddWithValue("@FECHA_PAGO", (object)ent.FECHA_PAGO);
                        val2.Parameters.AddWithValue("@BANCO", (object)ent.BANCO);
                        val2.Parameters.AddWithValue("@NUMERO_OPERACION", (object)ent.NUMERO_OPERACION);
                        val2.Parameters.AddWithValue("@OBSERVACION", (object)ent.OBSERVACION);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_PagoFlete(PagoFlete ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_PAGOFLETE", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@PAGOFLETE_ID", (object)ent.PAGOFLETE_ID);
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@CCODPROVE", (object)ent.CCODPROVE);
                        val2.Parameters.AddWithValue("@FORMA_PAGO", (object)ent.FORMA_PAGO);
                        val2.Parameters.AddWithValue("@IMPORTE", (object)ent.IMPORTE);
                        val2.Parameters.AddWithValue("@FECHA_PAGO", (object)ent.FECHA_PAGO);
                        val2.Parameters.AddWithValue("@BANCO", (object)ent.BANCO);
                        val2.Parameters.AddWithValue("@NUMERO_OPERACION", (object)ent.NUMERO_OPERACION);
                        val2.Parameters.AddWithValue("@OBSERVACION", (object)ent.OBSERVACION);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_PagoVarios(PagoVarios ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_PAGOVARIOS", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@PAGOVARIOS_ID", (object)ent.PAGOVARIOS_ID);
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@CCODPROVE", (object)ent.CCODPROVE);
                        val2.Parameters.AddWithValue("@FORMA_PAGO", (object)ent.FORMA_PAGO);
                        val2.Parameters.AddWithValue("@IMPORTE", (object)ent.IMPORTE);
                        val2.Parameters.AddWithValue("@FECHA_PAGO", (object)ent.FECHA_PAGO);
                        val2.Parameters.AddWithValue("@BANCO", (object)ent.BANCO);
                        val2.Parameters.AddWithValue("@NUMERO_OPERACION", (object)ent.NUMERO_OPERACION);
                        val2.Parameters.AddWithValue("@OBSERVACION", (object)ent.OBSERVACION);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_Delete_PGOPROVEEDOR(string id)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_PAGOPROVEEDOR", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@term", (object)id);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_Delete_PGOAGENTE(string id)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_PAGOAGENTE", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@term", (object)id);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_Delete_PGOFLETE(string id)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_PAGOFLETE", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@term", (object)id);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_Delete_PGOVARIOS(string id)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_PAGOVARIOS", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@term", (object)id);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_FleteSeguro(Imporc_FS ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_GUARDAR_FLETESEGURO", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@CNUMERO", (object)ent.CNUMERO);
                        val2.Parameters.AddWithValue("@CFLETE", (object)ent.CFLETE);
                        val2.Parameters.AddWithValue("@CSEGURO", (object)ent.CSEGURO);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListBancos(string term, string filtro)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_TIPOCOBRANZA_BANCOS", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@filtro", (object)filtro);
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }
        #endregion

        #region Linea Naviera
        public static DataTable GetListLNaviera()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_LNAVIERA", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_LineaNaviera(LineaNaviera ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_LINEANAVIERA", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@LINEA_NAVIERAID", (object)ent.LINEA_NAVIERAID);
                        val2.Parameters.AddWithValue("@DESCRIPCION", (object)ent.DESCRIPCION);
                        val2.Parameters.AddWithValue("@COMENTARIO", (object)ent.COMENTARIO);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_Delete_LineaNaviera(string id)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_LINEANAVIERA", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@term", (object)id);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        #endregion

        #region Forma Pago
        public static DataTable GetListFormaPago()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_FORMAPAGO", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static DataTable GetListFormaPagoDet(string term, string filtro)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                ((DbConnection)(object)val).Open();
                SqlCommand val2 = new SqlCommand("USP_GETLIST_FORMAPAGO_DET", val);
                try
                {
                    ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                    val2.Parameters.AddWithValue("@filtro", (object)filtro);
                    val2.Parameters.AddWithValue("@term", (object)term);
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter val3 = new SqlDataAdapter(val2);
                    try
                    {
                        ((DbDataAdapter)(object)val3).Fill(dataTable);
                    }
                    finally
                    {
                        ((IDisposable)val3)?.Dispose();
                    }
                    return dataTable;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_FormaPago(FormaPago ent)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_REGISTRAR_FORMAPAGO", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@FORMAPAGO_ID", (object)ent.FORMAPAGO_ID);
                        val2.Parameters.AddWithValue("@DESCRIPCION", (object)ent.DESCRIPCION);
                        val2.Parameters.AddWithValue("@OBSERVACION", (object)ent.OBSERVACION);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        public static ResultJson Procesa_Delete_FormaPago(string id)
        {
            ResultJson resultJson = new ResultJson();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionString = configuration.GetConnectionString("SqlConnectionString");
            SqlConnection val = new SqlConnection(connectionString);
            try
            {
                SqlCommand val2 = new SqlCommand("USP_ELIMINAR_FORMAPAGO", val);
                try
                {
                    try
                    {
                        ((DbConnection)(object)val).Open();
                        ((DbCommand)(object)val2).CommandType = CommandType.StoredProcedure;
                        val2.Parameters.AddWithValue("@term", (object)id);
                        string idReturn = ((DbCommand)(object)val2).ExecuteScalar().ToString();
                        string[] mensaje = idReturn.Split('|');

                        resultJson = new ResultJson
                        {
                            title = mensaje[0],
                            mensaje = mensaje[1],
                            icon = mensaje[2],
                            idReturn = mensaje[3]
                        };
                        //}
                    }
                    catch (Exception ex)
                    {
                        resultJson = new ResultJson
                        {
                            title = "Error",
                            mensaje = "Operacion con errores, no se pudo completar la operación " + ex.Message,
                            icon = "error",
                            idReturn = "0"
                        };
                    }
                    ((DbConnection)(object)val).Close();
                    return resultJson;
                }
                finally
                {
                    ((IDisposable)val2)?.Dispose();
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        #endregion
    }
}
